# -*- coding: utf-8 -*-
import json
import time
import os
import shutil
import settings
import requests
from common import api
from datetime import date

# gov
from scrawl.gov.yuncai import ScrapeYunCai

from scrawl.gov.pbcnews_gouttongjiaoliu import PbcGovNews
from scrawl.gov.csrc import CsrcGovNews
from scrawl.gov.cbrc import CbrcGovNews
from scrawl.gov.miit import MiitGovNews
from scrawl.gov.amac import AmacGovNews
from scrawl.gov.safe import SafeGovNews
from scrawl.gov.cac import CacGovNews
import shutil


outside_api_caller = api.IntellifeedAPICaller()


def check_response_to_slack(status_code, web_name):
    if status_code in [200, 201]:
        outside_api_caller.send_slack_notification('[ %s Scrwal Process Successfully ]' % web_name)
    else:
        outside_api_caller.send_slack_notification('[ %s Scrwal Process Fail ], the status code is %s' % (web_name, e))


def delete_all_file_under_tmp():
    print('*' * 6 + 'start to delete the tmp folder ' + '*' * 6)
    folder = settings.DEFAULT_CHROME_DOWNLOAD_PATH
    for the_file in os.listdir(folder):
        file_path = os.path.join(folder, the_file)
        try:
            if os.path.isfile(file_path):
                os.remove(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print(e)

    print('*' * 6 + 'successfully delete all files in tmp' + 6* '*')


def main_gov():

    while True:

        # # Yun Cai Jing
        try:
            outside_api_caller.send_slack_notification('[Start Yun Cai Jing Scrawl]')
            yuncai = ScrapeYunCai('https://www.yuncaijing.com/tag/id_5.html', 'Yun Cai Jing', 'cn')
            data = yuncai.response_data('2019-01-01')  # start day is  2019-01-01
            response = outside_api_caller.send_latest_news_data_to_mvp(data, 'gov')
            check_response_to_slack(response.status_code, 'Yun Cai Jing ')
        except Exception as error:
            outside_api_caller.send_slack_notification('[ Yun Cai Jing Scrawl Fail ], the err msg is %s' % str(error))

        # # PBC News goutongjiaoliu
        try:
            outside_api_caller.send_slack_notification('[Start PBC News Gou Tong Jiao Liu Scrawl]')
            first_page_url = 'http://www.pbc.gov.cn/goutongjiaoliu/113456/113469/11040/index1.html'
            pbcgovnews = PbcGovNews(first_page_url, 'PBoC', 'cn')
            response = pbcgovnews.get_response()
            if response:
                check_response_to_slack(response.status_code, 'PBC News Gou Tong Jiao Liu ')
            else:
                outside_api_caller.send_slack_notification('[ PBC  Gou Tong Jiao Liu PBC  Fail ]')
            time.sleep(60)
        except Exception as en:
            outside_api_caller.send_slack_notification('[ PBC Gou Tong Jiao Liu Scrawl Fail ], the err msg is %s'
                                                       % str(en))

        # PBC News diaochatonsgjisi
        try:
            outside_api_caller.send_slack_notification('[Start PBC News Diao Cha Tong ji Scrawl]')
            first_page_url = 'http://www.pbc.gov.cn/diaochatongjisi/116219/116225/index.html'
            # pbcgovnews = PbcGovNews(first_page_url, 'PBoC', 'cn')
            response = pbcgovnews.get_response_from_diaochatongji(first_page_url)
            if response:
                check_response_to_slack(response.status_code, 'PBC News Diao Cha Tong Ji  ')
            else:
                outside_api_caller.send_slack_notification('[ PBC  Diao Cha Tong Ji   Fail ]')
            time.sleep(60)

        except Exception as en:
            outside_api_caller.send_slack_notification('[ PBC  Diao Cha Tong ji Scrawl Fail ], the err msg is %s'
                                                       % str(en))

        # Cbrc Gov News
        try:
            outside_api_caller.send_slack_notification('[Start Cbrc News Scawl ]')
            cbrc_gov_news = CbrcGovNews('http://www.cbrc.gov.cn/chinese/newListDoc/111002/1.html', 'CBRC', 'cn')
            result = cbrc_gov_news.response_data()
            response = outside_api_caller.send_latest_news_data_to_mvp(result, 'gov')
            check_response_to_slack(response.status_code, 'CBRC News')
        except Exception as err:
            outside_api_caller.send_slack_notification('[ Cbrc Scrawl Fail ], the err msg is %s' % str(err))

        #  Miit Gov News
        try:
            outside_api_caller.send_slack_notification('[Start Mitt News Scawl ]')

            all_first_page = ['http://www.miit.gov.cn/n1146290/n4388791/index.html',
                              'http://www.miit.gov.cn/n1146290/n1146392/index.html']
            for item in all_first_page:
                cbrc_gov_news = MiitGovNews(item, 'MIIT', 'cn')
                _ = cbrc_gov_news.response_data()
                time.sleep(20)
            check_response_to_slack(200, 'Miit Scrawl Success ')
        except Exception as err:
            outside_api_caller.send_slack_notification('[ Miit Scrawl Fail ], the err msg is %s' % str(err))
        #
        # #  SAFE Gov News
        try:
            outside_api_caller.send_slack_notification('[Start SAFE News Scawl ]')
            first_page_url = 'http://www.safe.gov.cn/safe/zcfg/index.html'
            safe_gov_news = SafeGovNews(first_page_url, 'SAFE', 'cn')
            _ = safe_gov_news.response_data()
            time.sleep(20)
            check_response_to_slack(200, 'SAFE Scrawl Success ')
        except Exception as err:
            outside_api_caller.send_slack_notification('[ SAFE Scrawl Fail ], the err msg is %s' % str(err))

        # AMAC Gov News
        try:
            outside_api_caller.send_slack_notification('[Start AMAC News Scawl ]')
            first_page = 'http://www.amac.org.cn/xhgg/tzgg/'
            amac_gov_news = AmacGovNews(first_page, 'AMAC', 'cn')
            _ = amac_gov_news.response_data()
            check_response_to_slack(200, 'AMAC Scrawl Success ')
        except Exception as err:
            outside_api_caller.send_slack_notification('[ AMAC Scrawl Fail ], the err msg is %s' % str(err))

        #  CAC Gov News
        try:
            outside_api_caller.send_slack_notification('[Start CAC News Scawl ]')

            first_page_authoritative_publication_sector = \
                'http://qc.wa.news.cn/nodeart/list?nid=1192675&pgnum=1&cnt=40&attr=63&tp=1&orderby=1&callback=sam_cac&_=1554102097997'
            first_page_Window_of_work_sector = 'http://qc.wa.news.cn/nodeart/list?nid=1192675&pgnum=1' \
                                          '&cnt=40&attr=63&tp=1&orderby=1&callback=sam_cac&_=1554102178986'
            first_network_security_sector = 'http://qc.wa.news.cn/nodeart/list?nid=1182959&pgnum=1&cnt=' \
                                            '36&attr=63&tp=1&orderby=1&callback=sam_cac&_=1554271004658'

            first_page_informationization_sector = 'http://qc.wa.news.cn/nodeart/list?nid=1182960&pgnum=1' \
                                              '&cnt=36&attr=63&tp=1&orderby=1&callback=sam_cac&_=1554271058529'

            first_page_cooperation_and_communication_sector = 'http://qc.wa.news.cn/nodeart/list?nid=11102840&pgnum=1' \
                                                              '&cnt=36&attr=63&tp=1&orderby=1&callback=' \
                                                              'sam_cac&_=1554102377430'

            all_cac_list = [first_page_authoritative_publication_sector, first_network_security_sector,
                            first_page_informationization_sector, first_page_cooperation_and_communication_sector,
                            first_page_Window_of_work_sector]

            for per_item in all_cac_list:
                cac_gov_news = CacGovNews(per_item, 'CAC', 'cn')
                _ = cac_gov_news.response_data()
                time.sleep(30)
            check_response_to_slack(200, 'CAC Scrawl Success ')
        except Exception as err:
            outside_api_caller.send_slack_notification('[ CAC Scrawl Fail ], the err msg is %s' % str(err))

        delete_all_file_under_tmp()
        time.sleep(86400)  # run every day


main_gov()


# -------------------  the following code is to fix the missing pdf on S3 ----------------------------------
# -------------------  the following code is to fix the missing pdf on S3 ----------------------------------

#
# cninfo_obj = CninfoScrape()
# with open('missing_pdf_uuid_list.txt.txt') as f:
#     data = json.loads(f.read())
#     base_pdf_url = 'https://s3-us-west-1.amazonaws.com/research-static-prod/intellifeeds/cninfo/pdf/'
#     static_url_cninfo = 'http://static.cninfo.com.cn/finalpage/'
#     outside_api_caller = api.IntellifeedAPICaller()
#     index = 1
#     failed_list = []
#     for item in data:
#
#         pdf_id = str(item['identifier'])
#         date_str = str(item['date'])
#         pdf_path_in_s3 = 'intellifeeds/cninfo/pdf/' + pdf_id + '.pdf'
#         pdf_url = base_pdf_url + pdf_id + '.pdf'
#         pdf_static_url_cninfo = static_url_cninfo + date_str + '/' + pdf_id + '.PDF'
#         now_cwd_path = cninfo_obj.created_folder_by_date_in_tmp(date_str)
#         os.path.abspath(os.path.join(os.path.dirname(now_cwd_path + pdf_id + '.pdf'), os.path.pardir))
#         os.chdir(now_cwd_path + '/' + date_str + '/')
#         try:
#             # first attempt
#             response = requests.get(pdf_static_url_cninfo, timeout=30)
#         except Exception as err:
#             print('download pdf failed ,the identifier id is %s' % item['identifier'])
#             failed_list.append({'date': item['date'], 'identifier': item['identifier']})
#             continue
#         with open(str(item['identifier']) + '.pdf', 'wb') as file:
#             file.write(response.content)
#         try:
#             # first attempt
#             result_upload_status = outside_api_caller.upload_file_to_s3(pdf_id + '.pdf', pdf_path_in_s3,
#                                                                         content_type='application/pdf')
#         except Exception as err:
#             try:
#                 print('retry upload 2 ')
#                 result_upload_status = outside_api_caller.upload_file_to_s3(pdf_id + '.pdf', pdf_path_in_s3,
#                                                                             content_type='application/pdf')
#             except Exception as e:
#                 try:
#                     print('retry upload 3')
#                     result_upload_status = outside_api_caller.upload_file_to_s3(pdf_id + '.pdf', pdf_path_in_s3,
#                                                                                 content_type='application/pdf')
#                 except Exception as err:
#                     print('time out ! pls upload  manually, the the identifier is %s' % item['identifier'])
#                     result_upload_status = None
#         if result_upload_status:
#             print('PDF has been upload to S3 Successfully. the identifier is %s' % item['identifier'])
#             index += 1
#         else:
#             print('PDF failed to upload . the identifier is %s' % item['identifier'])
#             failed_list.append({'date': item['date'], 'identifier': item['identifier']})
#         os.chdir('/Users/sam/intellifeed-kol/')
#         # if the amount of PDF in tmp folder is larger than 20, delete it
#         if index > 30:
#             folder = 'tmp/'
#             for the_file in os.listdir(folder):
#                 file_path = os.path.join(folder, the_file)
#                 print(file_path)
#                 try:
#                     if os.path.isfile(file_path):
#                         os.remove(file_path)
#                     elif os.path.isdir(file_path):
#                         shutil.rmtree(file_path)
#                     index = 0
#                 except Exception as e:
#                     print(e)
#     print(failed_list)



# for itm in missing_pdf_uuid_list:
#     if itm not in test_list:
#         print(itm)

# test = ScrapeYunCai('https://www.yuncaijing.com/tag/id_5.html', 'gov Yun Cai', 'cn')
# test_a = test.response_data()
# print(test_a)

#
#
# print(len(missing_pdf_uuid_list))
# still_missing_uuid_list = []
#
# cninfo_obj = CninfoScrape()

# def delete_folder_from_tmp():
#     folder = 'tmp/'
#     for the_file in os.listdir(folder):
#         print(the_file)
#         file_path = os.path.join(folder, the_file)
#         try:
#             if os.path.isfile(file_path):
#                 os.remove(file_path)
#             elif os.path.isdir(file_path):
#                 shutil.rmtree(file_path)
#         except Exception as e:
#             print(e)

# -------------------  [ From Qian ] the following code is to fix the missing pdf from Qin on S3 ------
# -------------------  [ From Qian ] the following code is to fix the missing pdf on S3         -------

# this function is for qian's missing pdf list
# def find_missing_pdf_from_qian_list(missing_list):
#     intell_api = api.IntellifeedAPICaller()
#     cninfo_obj = CninfoScrape()
#     while len(missing_list) > 0:
#         print(len(missing_list))
#         for item in missing_list:
#             response = intell_api.fetch_intellifeed_data_by_id(item)
#             if not isinstance(response, str):
#                 if response.status_code not in [200, 201]:
#                     continue
#                 else:  # success
#                     test = json.loads(response.content)
#                     tmp_path = cninfo_obj.created_folder_by_date_in_tmp('missing_pdf')
#                     today_tmp_path = tmp_path + '/' + 'missing_pdf' + '/'
#                     try:
#                         _ = cninfo_obj.download_pdf_locality(test['source_full_route_url'] +
#                                                                    '?switchLocale=y&siteEntryPassthrough=true',
#                                                                    today_tmp_path, test["source_identifier"])
#                         os.chdir(cninfo_obj.main_path)
#                         missing_list.remove(item)
#                     except Exception as errr:
#                         continue
#             else:
#                 continue
    # delete_folder_from_tmp()


# import xlrd
#
# def change_chinese_name_by_excel():
#     workbook = xlrd.open_workbook('/Users/sam/intellifeed-kol/cn_op.xlsx')
#     sheet = workbook.sheet_by_index(0)
#     Flg = True
#     dict_result = dict()
#     while Flg:
#         index = 1
#         # all_row_values = sheet.row_values(index)
#         for rown in range(sheet.nrows):
#             value_D_col = sheet.cell_value(rown, 3)
#             if value_D_col:
#                 value_A_col = sheet.cell_value(rown, 0)
#                 dict_result[value_A_col] = value_D_col
#                 print("Update research_intellifeedcontent Set source_website_name = '" + value_D_col + "' where source_website_name = '" + str(value_A_col) + "'")
#             else:
#                 continue
#         Flg = False
#     print(dict_result)
#
# import xlrd
# import xlwt




# def get_companies_data_in_db():
#     intell_api = api.IntellifeedAPICaller()
#     url = 'http://api.redpulse.com/api/v1/intellifeedCompanies/'
#     headers = {'Authorization': 'Token ' + settings.MVP_ACCOUNT_TOKEN, 'Content-Type': 'application/json'}
#     response = requests.get(url, headers=headers)
#     content_list = json.loads(response.text)
#     index = 1
#     wb = xlwt.Workbook()
#     ws = wb.add_sheet('test_sheet')
#     for per_company in content_list:
#         ws.write(index, 1, per_company['full_name'])
#         index += 1
#     print('cninfo done')
#     index = 1
#     for it in xueqiu:
#         ws.write(index, 4, it['nick_name'])
#         ws.write(index, 5, it['original_url'])
#         index += 1
#     print('Xueqiu done')
#     index = 1
#     for weib in weibo:
#         ws.write(index, 8, weib['nick_name'])
#         ws.write(index, 9, weib['original_url'])
#         index += 1
#     print('weibo done')
#     index = 1
#     for zhih in zhihu:
#         ws.write(index, 12, zhih['nick_name'])
#         ws.write(index, 13, zhih['original_url'])
#         index += 1
#     print('zhihu done')
#     index = 1
#     for weic in weichat:
#         ws.write(index, 16, weic['nick_name'])
#         ws.write(index, 17, weic['original_url'])
#         index += 1
#
#     print('weichat done')
#     index = 1
#     for abiji in baijiahao:
#         ws.write(index, 20, abiji['nick_name'])
#         ws.write(index, 21, abiji['original_url'])
#         index += 1
#     wb.save('company_intellifeed.xls')


# get_companies_data_in_db()

# here is the missing pdf list from Qian
# missing_pdf_list = ['e9a9ec14-53ae-41e5-8602-66a6509420f0', 'e49f177c-fb49-4133-ba7f-8d61ffe0834d', 'ad43eec7-d56b-4248-850b-402634d1f57b', 'd09663d6-a83f-4e64-87df-12223dd06a9e', 'fe7f43e9-8075-4a96-a48a-2b6555cc3b72', 'a7831e45-6a2c-43d3-8790-6a0b85befd4e', '00af2753-bd86-40cc-a2b4-abfc2f717a5e', '5c33528b-8c17-4e97-b969-9cb2c312d5a5', '3675e35c-0f34-40b7-8dd4-1234458f1fe2', 'fa508a85-9705-4bba-b869-a7ec56a2fbc0', '8b6aecb2-e911-457d-b9ce-4ea91099ae3b', '9a38155f-1d74-4b57-8683-bdd88ece691a', '5718fb3e-8520-432a-84d3-d7c6af916cf0', '6c65e03f-e397-4b09-a855-424e307a5358', 'f69c05b3-20ba-413f-ad4f-ee29ad18cb1f', '7a9e6793-a642-431d-b97f-25b17ac353f3', '519e20b9-4cf7-4605-b6b3-dfa71a54f3a1', 'fa5c85e8-3eb9-44de-a535-f5257976d037', 'b92f8ca6-6be7-46b1-a9d4-f848b095d29c', '9ebe1a76-b3c4-4b05-8d5d-e5314088ebe0', '8fe9911b-2d2b-4260-9680-dbc3f57c2d54', '84d15483-bd28-4f83-bf9b-9abc5f28b67d', '4497f668-4772-4920-bef7-04c6db7ac49b']

# find_missing_pdf_from_qian_list(missing_pdf_list)

# for item in missing_pdf_uuid_list:
#     a = api.IntellifeedAPICaller()
#     response = a.fetch_intellifeed_data_by_id(item)
#     print(type(response))
#     if not isinstance(response, str):
#         print('op')
#         if response.status_code not in [200, 201]:
#             still_missing_uuid_list.append(item)
#             continue
#     else:
#         still_missing_uuid_list.append(item)
#         continue
#     if response.status_code in [200, 201]:
#         test = json.loads(response.content)
#     else:
#         still_missing_uuid_list.append(item)
#         continue
#     tmp_path = cninfo_obj.created_folder_by_date_in_tmp('2019-02-14')
#     today_tmp_path = tmp_path + '/' + '2019-02-14' + '/'
#     print(test['source_full_destination_url'])
#     # pdf_url = 'http://www.cninfo.com.cn/finalpage/' + test['date'] + '/' + test['identifier'] + '.PDF'
#     result_json = cninfo_obj.download_pdf_locality(test['source_full_route_url']+'?switchLocale=y&siteEntryPassthrough=true', today_tmp_path,
#                                                    test["source_identifier"])
#     os.chdir(cninfo_obj.main_path)
#     if isinstance(result_json, str):
#         still_missing_uuid_list.append(item)
#         print(item)
#         continue
# print(still_missing_uuid_list)
# #
#
# cninfo_obj.upload_pdf_to_s3()
# def tmp_folder_operat(index):
#     index = 10
#     folder = 'tmp/'
#     print(folder)
#     for the_file in os.listdir(folder):
#         file_path = os.path.join(folder, the_file)
#         print(file_path)
#         try:
#             if os.path.isfile(file_path):
#                 os.remove(file_path)
#             elif os.path.isdir(file_path):
#                 shutil.rmtree(file_path)
#             index = 0
#             print(index)
#         except Exception as e:
#             print(e)
#
# tmp_folder_operat(10)


