from scrawl.news.base import BaseScraper
import settings
import requests
import time
import uuid
from selenium import webdriver
from newspaper import Article
from bs4 import BeautifulSoup as BS
from selenium.common import exceptions
from selenium.webdriver.common.keys import Keys
from common import api
from urllib.parse import quote, unquote
import urllib.request
import os
from translate import Translator


def web_driver(url):
    _tmp_folder = '/tmp/{}'.format(uuid.uuid4())
    if not os.path.exists(_tmp_folder):
        os.makedirs(_tmp_folder)

    if not os.path.exists(_tmp_folder + '/user-data'):
        os.makedirs(_tmp_folder + '/user-data')

    if not os.path.exists(_tmp_folder + '/data-path'):
        os.makedirs(_tmp_folder + '/data-path')

    if not os.path.exists(_tmp_folder + '/cache-dir'):
        os.makedirs(_tmp_folder + '/cache-dir')

    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-gpu')
    chrome_options.add_argument('--window-size=1280x1696')
    chrome_options.add_argument('--user-data-dir={}'.format(_tmp_folder + '/user-data'))
    chrome_options.add_argument('--hide-scrollbars')
    chrome_options.add_argument('--enable-logging')
    chrome_options.add_argument('--log-level=0')
    chrome_options.add_argument('--v=99')
    chrome_options.add_argument('--single-process')
    chrome_options.add_argument('--data-path={}'.format(_tmp_folder + '/data-path'))
    chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument('--homedir={}'.format(_tmp_folder))
    chrome_options.add_argument('--disk-cache-dir={}'.format(_tmp_folder + '/cache-dir'))
    chrome_options.add_argument(
        'user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
        'Chrome/61.0.3163.100 Safari/537.36')
    prefs = {"download.default_directory": "/var/codes/intellifeed-kol"}
    # chrome_options.add_experimental_option("prefs", prefs)
    # driver = webdriver.Chrome(chrome_options=chrome_options)
    chrome_options.binary_location = settings.DRIVER_PATH_CHROME
    browser = webdriver.Chrome(chrome_options=chrome_options)

    browser.set_window_position(200, 200)
    browser.get(url)
    return browser


a = web_driver('http://www.baidu.com')
print(a.page_source)


# this is to  translate website title from zh to en
def translated_by_google(text_to_translate=None):

    translator = Translator(from_lang="zh_CN", to_lang="en")
    # 1. receive three params : translation type -default EN, text, is_html -default True
    # 2. to be upgrade: the type param can be a list and the result can be a list for various translation
    try:
        result = translator.translate(text_to_translate)
    except Exception as err:
        print(str(err))
        return ''
    if result:
        return result
    else:
        return ''

# --------------  [ From Luke ] the following code is to change the Name in DB to correct One     ------
# --------------  [ From Luke ] the following code is to change the Name in DB to correct One     -------
# import xlrd
#
# def change_chinese_name_by_excel():
#     workbook = xlrd.open_workbook('/Users/sam/intellifeed-kol/cn_op.xlsx')
#     sheet = workbook.sheet_by_index(0)
#     Flg = True
#     dict_result = dict()
#     while Flg:
#         index = 1
#         # all_row_values = sheet.row_values(index)
#         for rown in range(sheet.nrows):
#             value_D_col = sheet.cell_value(rown, 3)
#             if value_D_col:
#                 value_A_col = sheet.cell_value(rown, 0)
#                 dict_result[value_A_col] = value_D_col
#                 print("Update research_intellifeedcontent Set source_website_name = '" + value_D_col + "' where source_website_name = '" + str(value_A_col) + "'")
#             else:
#                 continue
#         Flg = False
#     print(dict_result)
#
# change_chinese_name_by_excel()


