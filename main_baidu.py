# -*- coding: utf-8 -*-
from common import api

from scrawl.news.web_scrapy_36kr import Scrape36kr
from scrawl.news.web_scrapy_caixin_all import ScrapeCaiXin
from scrawl.news.web_scrapy_chaindd import ScrapeChainDd
from scrawl.news.web_scrapy_china_daily import ScrapeChinaDaily
from scrawl.news.web_scrapy_dealstreetasia import ScrapeDealStreetAsia
#
from scrawl.news.web_scrapy_jinse import ScrapeJinSe
from scrawl.news.web_scrapy_router import ScrapeRouter
from scrawl.news.web_scrapy_scmp import ScrapeScmp

from scrawl.news.web_scrapy_yicai_all import ScrapeYiCai

from scrawl.news.web_scrapy_xinhua import ScrapeXinHua

from scrawl.cninfo.cninfo import CninfoScrape
from scrawl.news.web_scrapy_baidu_news import LandingHome
from selenium_framework.utils import BaseAction, start_browser, close_browser

# gov
from scrawl.gov.yuncai import ScrapeYunCai
import settings
from selenium import webdriver
import time
import os
import uuid

outside_api_caller = api.IntellifeedAPICaller()


def main_bai_du():
      while True:
            #  change the url of keyword search in bai du

            url_en_macquarie = 'http://news.baidu.com/ns?rn=20&ie=utf-8&cl=2&ct=1&bs=ASX%3AMQG&rsv_bp=' \
                               '1&sr=0&f=3&prevct=no&tn=news&word=Macquarie&rsv_sug3=2' \
                               '&rsv_sug4=69&rsv_sug1=2&rsp=0&inputT=631&rsv_sug=1'
            url_cn_macquarie = 'http://news.baidu.com/ns?rn=20&ie=utf-8&cl=2&ct=1&bs=Macquarie&rsv_bp=1&' \
                               'sr=0&f=8&prevct=no&tn=news&word=%E9%BA%A6%E6%A0%BC%E7%90%86&rsv_sug3=1&rsv_sug4=' \
                               '31&rsv_sug1=1&inputT=912&rsv_sug=1'
            url_other_keyword = 'http://news.baidu.com/ns?word=ASX:MQG&tn=news&from=news&cl=2&rn=20&ct=1'  # keyword : ASX:MQG

            url_for_Ping_An = 'http://news.baidu.com/ns?word=%E4%BA%BA%E5%AF%BF%E4%BF%9D%E9%99%A9&tn=' \
                              'news&from=news&cl=2&rn=20&ct=1' # keyword for Ping An Bao Xian (Chinese )

            url_for_Jian_Kang = 'http://news.baidu.com/ns?word=%E5%81%A5%E5%BA%B7%E4%BF%9D%E9%99%A9&tn=news' \
                                '&from=news&cl=2&rn=20&ct=1'

            url_for_Bao_Xian_fei = 'http://news.baidu.com/ns?word=%E4%BF%9D%E9%99%A9%E8%B4%B9&tn=news&' \
                                   'from=news&cl=2&rn=20&ct=1' # keyword for Bao Xian Fei(Chinese )

            url_for_Zai_Bao_Xian = 'http://news.baidu.com/ns?word=%E5%86%8D%E4%BF%9D%E9%99%A9&tn=news&f' \
                                   'rom=news&cl=2&rn=20&ct=1'  # keyword for Zai Bao Xian (Chinese )

            web_site_list = [url_en_macquarie, url_cn_macquarie, url_other_keyword, url_for_Ping_An,
                             url_for_Jian_Kang, url_for_Bao_Xian_fei, url_for_Zai_Bao_Xian]
            chrome_options = webdriver.ChromeOptions()

            _tmp_folder = '/tmp/{}'.format(uuid.uuid4())

            chrome_options.add_argument('--headless')
            chrome_options.add_argument('--disable-gpu')
            chrome_options.add_argument('--no-sandbox')
            chrome_options.add_argument('--window-size=1280,1000')
            chrome_options.add_argument('--allow-running-insecure-content')
            chrome_options.add_argument('--user-data-dir={}'.format(_tmp_folder + '/user-data'))
            chrome_options.add_argument('--hide-scrollbars')
            chrome_options.add_argument('--enable-logging')
            chrome_options.add_argument('--log-level=0')
            chrome_options.add_argument('--v=99')
            chrome_options.add_argument('--single-process')
            chrome_options.add_argument('--data-path={}'.format(_tmp_folder + '/data-path'))
            chrome_options.add_argument('--ignore-certificate-errors')
            chrome_options.add_argument('--homedir={}'.format(_tmp_folder))
            chrome_options.add_argument('--disk-cache-dir={}'.format(_tmp_folder + '/cache-dir'))
            chrome_options.add_argument('user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 '
                                        '(KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36')
            chrome_options.binary_location = settings.DRIVER_PATH_CHROME

            for item in web_site_list:
                browser = webdriver.Chrome(chrome_options=chrome_options)
                browser.get(item)
                landing = LandingHome()
                outside_api_caller.send_slack_notification('[Start Baidu for Macquarie or BaoXian ]')
                try:
                    flg = landing.get_all_data(browser)
                    if not flg:
                        close_browser(browser)
                        continue
                except Exception as err:
                    outside_api_caller.send_slack_notification('[ Fail to Baidu / BaoXian Scrawl], '
                                                               'the error msg is %s' % str(err))
            time.sleep(86400*2)  # run every 2 day


main_bai_du()







# # -------------------  PBC News    ----------------------------------
# # -------------------  PBC News     ----------------------------------
#
# pbcgovnews = PbcGovNews()
# result = pbcgovnews.response_data()
# outside_api_caller = api.IntellifeedAPICaller()
# response = outside_api_caller.send_latest_news_data_to_mvp(result)
# print(response)




# -------------------  the following code is to fix the missing pdf on S3 ----------------------------------
# -------------------  the following code is to fix the missing pdf on S3 ----------------------------------

#
# cninfo_obj = CninfoScrape()
# with open('missing_pdf_uuid_list.txt.txt') as f:
#     data = json.loads(f.read())
#     base_pdf_url = 'https://s3-us-west-1.amazonaws.com/research-static-prod/intellifeeds/cninfo/pdf/'
#     static_url_cninfo = 'http://static.cninfo.com.cn/finalpage/'
#     outside_api_caller = api.IntellifeedAPICaller()
#     index = 1
#     failed_list = []
#     for item in data:
#
#         pdf_id = str(item['identifier'])
#         date_str = str(item['date'])
#         pdf_path_in_s3 = 'intellifeeds/cninfo/pdf/' + pdf_id + '.pdf'
#         pdf_url = base_pdf_url + pdf_id + '.pdf'
#         pdf_static_url_cninfo = static_url_cninfo + date_str + '/' + pdf_id + '.PDF'
#         now_cwd_path = cninfo_obj.created_folder_by_date_in_tmp(date_str)
#         os.path.abspath(os.path.join(os.path.dirname(now_cwd_path + pdf_id + '.pdf'), os.path.pardir))
#         os.chdir(now_cwd_path + '/' + date_str + '/')
#         try:
#             # first attempt
#             response = requests.get(pdf_static_url_cninfo, timeout=30)
#         except Exception as err:
#             print('download pdf failed ,the identifier id is %s' % item['identifier'])
#             failed_list.append({'date': item['date'], 'identifier': item['identifier']})
#             continue
#         with open(str(item['identifier']) + '.pdf', 'wb') as file:
#             file.write(response.content)
#         try:
#             # first attempt
#             result_upload_status = outside_api_caller.upload_file_to_s3(pdf_id + '.pdf', pdf_path_in_s3,
#                                                                         content_type='application/pdf')
#         except Exception as err:
#             try:
#                 print('retry upload 2 ')
#                 result_upload_status = outside_api_caller.upload_file_to_s3(pdf_id + '.pdf', pdf_path_in_s3,
#                                                                             content_type='application/pdf')
#             except Exception as e:
#                 try:
#                     print('retry upload 3')
#                     result_upload_status = outside_api_caller.upload_file_to_s3(pdf_id + '.pdf', pdf_path_in_s3,
#                                                                                 content_type='application/pdf')
#                 except Exception as err:
#                     print('time out ! pls upload  manually, the the identifier is %s' % item['identifier'])
#                     result_upload_status = None
#         if result_upload_status:
#             print('PDF has been upload to S3 Successfully. the identifier is %s' % item['identifier'])
#             index += 1
#         else:
#             print('PDF failed to upload . the identifier is %s' % item['identifier'])
#             failed_list.append({'date': item['date'], 'identifier': item['identifier']})
#         os.chdir('/Users/sam/intellifeed-kol/')
#         # if the amount of PDF in tmp folder is larger than 20, delete it
#         if index > 30:
#             folder = 'tmp/'
#             for the_file in os.listdir(folder):
#                 file_path = os.path.join(folder, the_file)
#                 print(file_path)
#                 try:
#                     if os.path.isfile(file_path):
#                         os.remove(file_path)
#                     elif os.path.isdir(file_path):
#                         shutil.rmtree(file_path)
#                     index = 0
#                 except Exception as e:
#                     print(e)
#     print(failed_list)


#
# for itm in missing_pdf_uuid_list:
#     if itm not in test_list:
#         print(itm)

# test = ScrapeYunCai('https://www.yuncaijing.com/tag/id_5.html', 'gov Yun Cai', 'cn')
# test_a = test.response_data()
# print(test_a)

#
#
# print(len(test_list))
# still_missing_uuid_list = []
# a = api.IntellifeedAPICaller()
#
# cninfo_obj = CninfoScrape()
# for item in test_list:
#     response = a.fetch_intellifeed_data_by_id(item)
#     if not isinstance(response, str):
#         print('op')
#         if response.status_code not in [200, 201]:
#             still_missing_uuid_list.append(item)
#             continue
#     else:
#         still_missing_uuid_list.append(item)
#         continue
#     if response.status_code in [200, 201]:
#         test = json.loads(response.content)
#     else:
#         still_missing_uuid_list.append(item)
#         continue
#     tmp_path = cninfo_obj.created_folder_by_date_in_tmp('2019-02-16')
#     today_tmp_path = tmp_path + '/' + '2019-02-16' + '/'
#     print(test['source_full_destination_url'])
#     # pdf_url = 'http://www.cninfo.com.cn/finalpage/' + test['date'] + '/' + test['identifier'] + '.PDF'
#     result_json = cninfo_obj.download_pdf_locality(test['source_full_route_url']+'?switchLocale=y&siteEntryPassthrough=true', today_tmp_path,
#                                                    test["source_identifier"])
#     os.chdir(cninfo_obj.main_path)
#     if isinstance(result_json, str):
#         still_missing_uuid_list.append(item)
#         print(item)
#         continue
# print(still_missing_uuid_list)
# #
#
# cninfo_obj.upload_pdf_to_s3()
# def tmp_folder_operat(index):
#     index = 10
#     folder = 'tmp/'
#     print(folder)
#     for the_file in os.listdir(folder):
#         file_path = os.path.join(folder, the_file)
#         print(file_path)
#         try:
#             if os.path.isfile(file_path):
#                 os.remove(file_path)
#             elif os.path.isdir(file_path):
#                 shutil.rmtree(file_path)
#             index = 0
#             print(index)
#         except Exception as e:
#             print(e)
#
# tmp_folder_operat(10)

#
# with open('missing_pdf_list.txt', 'r') as f:
#     data = json.loads(f.read())
#     print(data[0]['identifier'])



# tmp_list = [
#     {'date': '2019-01-18', 'identifier': '1205778687'}, {'date': '2019-01-18', 'identifier': '1205778730'}, {'date': '2019-01-18', 'identifier': '1205778726'}, {'date': '2019-01-18', 'identifier': '1205778688'}, {'date': '2019-01-17', 'identifier': '1205778578'}, {'date': '2019-01-17', 'identifier': '1205775853'}, {'date': '2019-01-15', 'identifier': '1205768595'}, {'date': '2019-01-15', 'identifier': '1205768673'}, {'date': '2019-01-14', 'identifier': '1205768760'}, {'date': '2019-01-14', 'identifier': '1205768732'}, {'date': '2019-01-12', 'identifier': '1205762194'}, {'date': '2019-01-12', 'identifier': '1205764820'}, {'date': '2019-01-12', 'identifier': '1205763581'}]
#
# for item in tmp_list:
#     # pdf_url, pdf_path, rp_id
#     test = CninfoScrape()
#     today_tmp_path = 'tmp' + '/' + item['date'] + '/'
#     pdf_url = 'http://www.cninfo.com.cn/finalpage/' + item['date'] + '/' + item['identifier'] + '.PDF'
#     result_json = test.download_pdf_locality(pdf_url, 'tmp/2018-01-20/', item['identifier'])





# -------------  the following code is to fix the missing pdf on S3 by Intellifeed Company  --------------------
# -------------  the following code is to fix the missing pdf on S3 by Intellifeed Company  ---------------------