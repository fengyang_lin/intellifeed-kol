.PHONY: clean clean-test build start loaddata test lint docs coverage coverage-open bash celery-logs version
.DEFAULT_GOAL := help

define BROWSER_PYSCRIPT
import os, webbrowser, sys
try:
	from urllib import pathname2url
except:
	from urllib.request import pathname2url

webbrowser.open("file://" + pathname2url(os.path.abspath(sys.argv[1])))
endef
export BROWSER_PYSCRIPT

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT
BROWSER := python3 -c "$$BROWSER_PYSCRIPT"

help:
	@python3 -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

clean: ## Remove all build artifacts and unnecessary Python files
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -f {} +
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

lint: ## Check pylint
	pylint common scraper utility

codestyle: ## Run pycodestyle
	pycodestyle common scraper settings utility

check-packages: ## Check Python packages for outdated versions
	docker-compose -f local.yml run django pip-review

test: ## Run the tests
	docker-compose -f local.yml run -e DJANGO_SETTINGS_MODULE=config.settings.test django python manage.py test --verbosity=2 --exclude-tag=open_calais_call

test-parallel: ## Run the tests
	docker-compose -f local.yml run -e DJANGO_SETTINGS_MODULE=config.settings.test django python manage.py test --parallel --exclude-tag=open_calais_call

test-opencalais: ## Run only the OpenCalais tests, will call OpenCalais API, add "@tag('open_calais_call')" in test to make it be included
	docker-compose -f local.yml run -e DJANGO_SETTINGS_MODULE=config.settings.local django python manage.py test --verbosity=2 --tag=open_calais_call

coverage: ## Run tests and create coverage report
	docker-compose -f local.yml run -e DJANGO_SETTINGS_MODULE=config.settings.test django coverage run manage.py test
	docker-compose -f local.yml run django coverage html
	docker-compose -f local.yml run django coverage report

coverage-open: coverage ## Run coverage tests and open in browser
	$(BROWSER) htmlcov/index.html

docs: ## generate Sphinx HTML documentation, including API docs
	$(MAKE) -C docs html
	$(BROWSER) docs/_build/html/index.html

bash: ## open a bash in the Django container
	docker-compose -f local.yml run django /bin/bash

celery-logs: ## follow logs of the celery worker
	docker-compose -f local.yml logs -f celeryworker

celery-restart: ## Restart celery Docker container
	docker-compose -f local.yml restart celeryworker celerybeat

version: ## generate a version
	@./utility/build_version.py dev
