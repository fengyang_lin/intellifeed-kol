# -*- coding: utf-8 -*-
from common import api
from scrawl.news.web_scrapy_36kr import Scrape36kr
from scrawl.news.web_scrapy_caixin_all import ScrapeCaiXin
from scrawl.news.web_scrapy_chaindd import ScrapeChainDd
from scrawl.news.web_scrapy_china_daily import ScrapeChinaDaily
from scrawl.news.web_scrapy_dealstreetasia import ScrapeDealStreetAsia

from scrawl.news.web_scrapy_jinse import ScrapeJinSe
# from scrawl.news.web_scrapy_router import ScrapeRouter
from scrawl.news.web_scrapy_scmp import ScrapeScmp

from scrawl.news.web_scrapy_yicai_all import ScrapeYiCai
# from scrawl.news.web_scrapy_xinhua import ScrapeXinHua

from scrawl.cninfo.cninfo import CninfoScrape
from scrawl.news.web_scrapy_baidu_news import LandingHome
from scrawl.gov.yuncai import ScrapeYunCai

from scrawl.news.base import BaseScraper
import settings
# from scrawl.gov.pbcnews import PbcGovNews
import time
import requests
import json


outside_api_caller = api.IntellifeedAPICaller()


def send_data_to_mvp_backend_and_slack(json_data, web_name):
    try:
        response = outside_api_caller.send_latest_news_data_to_mvp(json_data, 'news')
        status_code = response.status_code
    except Exception as e:
        status_code = 500
        outside_api_caller.send_slack_notification('[ %s Scrwal Process Fail ], the status code is %s' % (web_name, e))
    if status_code in [200, 201]:
        outside_api_caller.send_slack_notification('[ %s Scrwal Process Successfully ]' % web_name)
    else:
        outside_api_caller.send_slack_notification('[ %s Scrwal Process Fail ], the status code is %s' % (web_name, response.status_code))


def main_news():
    while True:
        # China Daily News
        try:
            outside_api_caller.send_slack_notification('[Start China Daily Scrawl]')
            kr_obj = ScrapeChinaDaily('http://www.chinadaily.com.cn/business/money', 'China-daily-money', 'en')
            result_json = kr_obj.extract_content_by_xpath_data()
            send_data_to_mvp_backend_and_slack(result_json, 'China Daily')
        except Exception as e:
            outside_api_caller.send_slack_notification('[ China Daily Scrawl Fail ], the err msg is %s' % str(e))

        # Xin Hua News
        # try:
        #     outside_api_caller.send_slack_notification('[Start Xin Hua Scrawl]')
        #     kr_obj = ScrapeXinHua('http://qc.wa.news.cn/nodeart/list?nid=11143393&'
        #                           'pgnum=1&cnt=50&tp=1&orderby=1?callback=sam&_=1545873337323', 'XinHua', 'en')
        #
        #     result_json = kr_obj.response_data()
        #     send_data_to_mvp_backend_and_slack(result_json, 'Xin Hua')
        # except Exception as e:
        #     outside_api_caller.send_slack_notification('[ Xin Hua Scrawl Fail ], the err msg is %s' % e)

        try:
            outside_api_caller.send_slack_notification('[Start 36kr Scrawl]')
            kr_obj = Scrape36kr('https://36kr.com/api/newsflash', '36kr', 'cn')
            result_json = kr_obj.response_data()
            send_data_to_mvp_backend_and_slack(result_json, '36kr')
        except Exception as e:
            outside_api_caller.send_slack_notification('[ 36kr Scrawl Fail ], the err msg is %s' % str(e))

        # Caixin  Economy
        try:
            outside_api_caller.send_slack_notification('[Start Caixin Economy ]')
            kr_obj = ScrapeCaiXin('https://www.caixinglobal.com/economy/', 'Caixin Economy', 'en')
            result_json = kr_obj.extract_content_by_xpath()
            send_data_to_mvp_backend_and_slack(result_json, 'Caixin')
        except Exception as e:
            outside_api_caller.send_slack_notification('[ Cai Xin Economy Scrawl Fail ], the err msg is %s' % e)

        # Caixin  Business Tech
        try:
            outside_api_caller.send_slack_notification('[Start Caixin Business Tech ]')
            kr_obj = ScrapeCaiXin('https://www.caixinglobal.com/business-and-tech/', 'caixin-business-and-tech', 'en')
            result_json = kr_obj.extract_content_by_xpath()
            send_data_to_mvp_backend_and_slack(result_json, 'Caixin Business Tech')
        except Exception as e:
            outside_api_caller.send_slack_notification('[ Cai Xin Business Tech Scrawl Fail ], the err msg is %s' % e)

        # Chain add
        # try:
        #     outside_api_caller.send_slack_notification('[Start ChainDD ]')
        #     kr_obj = ScrapeChainDd('https://www.chaindd.com/nictation/', 'ChainDD', 'cn')
        #     result_json = kr_obj.extract_content_by_bs()
        #     send_data_to_mvp_backend_and_slack(result_json, 'Caixin ChainDD')
        # except Exception as e:
        #     outside_api_caller.send_slack_notification('[ ChainDD Scrawl Fail ], the err msg is %s' % e)

        # Router (can't be access In China ,need Vpn)
        # try:
        #     outside_api_caller.send_slack_notification('[Start Router ]')
        #     kr_obj = ScrapeRouter('https://wireapi.reuters.com/v3/feed/url/www.reuters.com/places/china', 'Router China', 'en')
        #     result_json = kr_obj.extract_content()
        #     send_data_to_mvp_backend_and_slack(result_json, 'Router')
        # except Exception as e:
        #     outside_api_caller.send_slack_notification('[ Router Scrawl Fail ], the err msg is %s' % e)

        # dealstreetasia
        try:
            outside_api_caller.send_slack_notification('[Start dealstreetasia ]')
            kr_obj = ScrapeDealStreetAsia('https://www.dealstreetasia.com/countries/china-hk/page/',
                                          'Dealstreetasia China-HK', 'en')
            result_json = kr_obj.extract_content_by_bs_data()
            send_data_to_mvp_backend_and_slack(result_json, 'Dealstreetasia China-HK')
        except Exception as e:
            outside_api_caller.send_slack_notification('[ dealstreetasia Scrawl Fail ], the err msg is %s' % e)

        # JinSe Cai Jing (not Use any more)
        # try:
        #     outside_api_caller.send_slack_notification('[Start JinSe Cai Jing ]')
        #     kr_obj = ScrapeJinSe('https://api.jinse.com/v4/live/list?limit=200&reading=false&source=web', 'Jinse Finance', 'cn')
        #     result_json = kr_obj.extract_content()
        #     send_data_to_mvp_backend_and_slack(result_json, 'JinSe Cai Jing')
        # except Exception as e:
        #     outside_api_caller.send_slack_notification('[ JinSe Cai Jing Scrawl Fail ], the err msg is %s' % e)

        # YiCai Global
        try:
            outside_api_caller.send_slack_notification('[Start Yi Cai  ]')

            kr_obj = ScrapeYiCai('_', 'Yicai Global', 'en')
            result_json = kr_obj.response_data()
            send_data_to_mvp_backend_and_slack(result_json, 'news')
        except Exception as e:
            outside_api_caller.send_slack_notification('[ YiCai Scrawl Fail ], the err msg is %s' % e)

        # Scmp  (can't be access In China , need Vpn)
        # try:
        #     outside_api_caller.send_slack_notification('[Start Scmp ]')
        #     kr_obj = ScrapeScmp('https://www.scmp.com/topics/banking-finance', 'scmp', 'en')
        #     result_json = kr_obj.response_data()
        #     send_data_to_mvp_backend_and_slack(result_json, 'SCMP')
        # except Exception as e:
        #     outside_api_caller.send_slack_notification('[ Scmp Scrawl Fail ], the err msg is %s' % e)

        time.sleep(43200)  # run every half day


main_news()


# -------------------  YiCai Historical Data     -----------------------------------
# -------------------  YiCai Historical Data    ------------------------------------

# Now I run following code in the locality, not in the service

# def get_historical_yicai_data():
#
#     api_url_yicai_buiness = 'https://api.yicaiglobal.com/documents?categoryIds=5bf12c9569aa96784b7f0bd7&publishTime=%24lt%402019-03-21T09%3A02%3A39.871Z&status=normal&fields=title,authors,name,publishTime,image&sort=-publishTime&limit=9999'
#
#     api_url_yicai_tech = 'https://api.yicaiglobal.com/documents?categoryIds=5bf12c9569aa96784b7f0bdd&publishTime=%24lt%402019-03-20T11%3A30%3A38.928Z&status=normal&fields=title,authors,name,publishTime,image&sort=-publishTime&limit=8999'
#
#     api_url_yicai_startup = 'https://api.yicaiglobal.com/documents?categoryIds=5bf12c9569aa96784b7f0be0&publishTime=%24lt%402019-03-12T08%3A23%3A05.055Z&status=normal&fields=title,authors,name,publishTime&sort=-publishTime&limit=8999'
#
#     # response = requests.get(api_url_yicai_buiness)
#     # json_dict = json.loads(response.content)
#     # for item in json_dict["data"]:
#     #     if item['title'] == "Multinationals Remain Rosy Over China's Economy":
#     #         print(json_dict["data"].index(item))
#     #     else:
#     #         continue
#     url_list = [api_url_yicai_buiness, api_url_yicai_tech, api_url_yicai_startup]
#     for per_url in url_list:
#         if url_list.index(per_url) == 0:  # business
#             last_name = 'business'
#         elif url_list.index(per_url) == 1:  # tech
#             last_name = 'tech'
#         elif url_list.index(per_url) == 2:   # start up
#             last_name = 'startup'
#         else:
#             print("What the hell ?")
#         response = requests.get(per_url)
#         base_scraper_obj = BaseScraper('domain_url', 'media', 'cn')
#         total_result_list = []
#         index = 1
#         json_dict = json.loads(response.content)
#         if last_name == 'business':
#             json_dict["data"] = json_dict["data"][7348:]
#         for item in json_dict["data"]:
#             index += 1
#             item_data = dict()
#             item_data['title'] = item['title']
#             detail_page_url = 'https://www.yicaiglobal.com/news/' + item['name']
#             print(detail_page_url)
#             try:
#                 content = base_scraper_obj.extract_content_by_newspaper3k(detail_page_url, 'en')
#             except Exception as err:
#                 content = ''
#                 time.sleep(10)
#                 print(err)
#                 return True
#             if content:
#                 item_data['content'] = content
#             else:
#                 print('Extract Data From Web Page Down ')
#                 continue
#                 # send_data_to_mvp_backend_and_slack(total_result_list, 'news')
#             date_list_value = item['publishTime'].split("T")
#             combined_date_format = date_list_value[0] + ' ' + date_list_value[1].split('.')[0]
#             item_data['published_at'] = combined_date_format
#             item_data['language'] = 'en'
#             item_data['url'] = detail_page_url
#             item_data['title'] = item['title']
#             item_data['is_added_to_source'] = False
#             item_data['media'] = 'YiCai' + '-' + last_name
#             print(item_data['url'], index)
#             total_result_list.append(item_data)
#             if index > 150:
#                 send_data_to_mvp_backend_and_slack(total_result_list, 'news')
#                 index = 1
#                 total_result_list = []
#         send_data_to_mvp_backend_and_slack(total_result_list, 'news')
#
# get_historical_yicai_data()

# -------------------  China Daily Historical Data     -----------------------------------
# -------------------  China Daily Historical Data     ------------------------------------


def get_historical_china_daily_money_data():
    try:
        outside_api_caller.send_slack_notification('[Start China Daily Scrawl]')
        'http://www.chinadaily.com.cn/business/money/page_153.html'
        for index in range(1, 154):
            print(index)
            url_per_page = 'http://www.chinadaily.com.cn/business/money/page_' + str(index) + '.html'
            kr_obj = ScrapeChinaDaily(url_per_page, 'China-daily-money', 'en')
            result_json = kr_obj.extract_content_by_xpath_data()
            send_data_to_mvp_backend_and_slack(result_json, 'China Daily')
    except Exception as e:
        outside_api_caller.send_slack_notification('[ China Daily Scrawl Fail ], the err msg is %s' % e)







# -------------------  chinadaily    ----------------------------------
# -------------------  chinadaily    ----------------------------------

# kr_obj = ScrapeChinaDaily('http://www.chinadaily.com.cn/business/money', 'China-daily-money', 'en')
# result_json = kr_obj.extract_content_by_xpath_data()
# outside_api_caller = api.IntellifeedAPICaller()
# response = outside_api_caller.send_latest_news_data_to_mvp(result_json, 'news')
# print(result_json)


#
#
#
# # -------------------  XinHua    ----------------------------------
# # -------------------  XinHua    ----------------------------------
#
# kr_obj = ScrapeXinHua('http://qc.wa.news.cn/nodeart/list?nid=11143393&'
#                       'pgnum=1&cnt=50&tp=1&orderby=1?callback=sam&_=1545873337323', 'XinHua', 'en')
#
# result_json = kr_obj.response_data()
# outside_api_caller = api.IntellifeedAPICaller()
# response = outside_api_caller.send_latest_news_data_to_mvp(result_json, 'news')
# # print(response)


#
# # -------------------  36kr    ----------------------------------
# # -------------------  36kr    ----------------------------------
#
# kr_obj = Scrape36kr('https://36kr.com/api/newsflash', '36kr', 'cn')
# result_json = kr_obj.response_data()
# outside_api_caller = api.IntellifeedAPICaller()
# response = outside_api_caller.send_latest_news_data_to_mvp(result_json, 'news')
# print(response)
# #
# # -------------------  Caixin   Economy ----------------------------------
# # -------------------  Caixin  Economy  ----------------------------------
#
# kr_obj = ScrapeCaiXin('https://www.caixinglobal.com/economy/', 'Caixin Economy', 'en')
# result_json = kr_obj.extract_content_by_xpath()
# outside_api_caller = api.IntellifeedAPICaller()
# response = outside_api_caller.send_latest_news_data_to_mvp(result_json, 'news')
# print(response)
#
#
# # -------------------  Caixin   Business  Tech ----------------------------------
# # -------------------  Caixin   Business  Tech ----------------------------------
#
# kr_obj = ScrapeCaiXin('https://www.caixinglobal.com/business-and-tech/', 'caixin-business-and-tech', 'en')
# result_json = kr_obj.extract_content_by_xpath()
# outside_api_caller =  api.IntellifeedAPICaller()
# response = outside_api_caller.send_latest_news_data_to_mvp(result_json, 'news')
# print(response)
#
#
# # -------------------  Chaindd ----------------------------------
# # -------------------  Chaindd ----------------------------------
#
#
# kr_obj = ScrapeChainDd('https://www.chaindd.com/nictation/', 'ChainDD', 'cn')
# result_json = kr_obj.extract_content_by_bs()
# outside_api_caller = api.IntellifeedAPICaller()
# response = outside_api_caller.send_latest_news_data_to_mvp(result_json, 'news')
# print(response)
#
#
# # -------------------  Router ----------------------------------
# # -------------------  Router ----------------------------------
#
#
# kr_obj = ScrapeRouter('https://wireapi.reuters.com/v3/feed/url/www.reuters.com/places/china', 'Router China', 'en')
# result_json = kr_obj.extract_content()
# outside_api_caller = api.IntellifeedAPICaller()
# response = outside_api_caller.send_latest_news_data_to_mvp(result_json, 'news')
# print(response)
#
#
# # -------------------  dealstreetasia ----------------------------------
# # -------------------  dealstreetasia ----------------------------------
#
# kr_obj = ScrapeDealStreetAsia('https://www.dealstreetasia.com/countries/china-hk/page/', 'Dealstreetasia China-HK', 'en')
# result_json = kr_obj.extract_content_by_bs_data()
# outside_api_caller = api.IntellifeedAPICaller()
# response = outside_api_caller.send_latest_news_data_to_mvp(result_json, 'news')
# print(response)

#
# # -------------------  Jinse Finance  ----------------------------------
# # -------------------  Jinse Finance  ----------------------------------
#
# kr_obj = ScrapeJinSe('https://api.jinse.com/v4/live/list?limit=200&reading=false&source=web', 'Jinse Finance', 'cn')
# result_json = kr_obj.extract_content()
# outside_api_caller = api.IntellifeedAPICaller()
# response = outside_api_caller.send_latest_news_data_to_mvp(result_json, 'news')
# print(response)
#
#
# # -------------------  YiCai Global ( 4 ) ----------------------------------
# # -------------------  YiCai Global ( 4 ) ----------------------------------
#
# kr_obj = ScrapeYiCai('_', 'Yicai Global', 'en')
#
# result_json = kr_obj.response_data()
# outside_api_caller = api.IntellifeedAPICaller()
# response = outside_api_caller.send_latest_news_data_to_mvp(result_json, 'news')
# print(response)
#
#
# # -------------------  Scmp ----------------------------------
# # -------------------  Scmp ----------------------------------
# kr_obj = ScrapeScmp('https://www.scmp.com/topics/banking-finance', 'scmp', 'en')
#
# result_json = kr_obj.response_data()
# print(result_json)
# outside_api_caller = api.IntellifeedAPICaller()
# response = outside_api_caller.send_latest_news_data_to_mvp(result_json, 'news')
# print(response)



# # -------------------  YunCai    ----------------------------------
# # -------------------  YunCai    ----------------------------------
#
# yuncai = ScrapeYunCai('https://www.yuncaijing.com/tag/id_5.html', 'Y')
# result = yuncai.response_data()
# outside_api_caller = api.IntellifeedAPICaller()
# response = outside_api_caller.send_latest_news_data_to_mvp(result, 'gov')
# print(response)


# # -------------------  Baidu News  ----------------------------------
# # -------------------  Baidu News  ----------------------------------


# from selenium import webdriver
# url = 'http://news.baidu.com/ns?rn=20&ie=utf-8&cl=2&ct=1&bs=' \
#       'Macquarie&rsv_bp=1&sr=0&f=8&prevct=no&tn=news&word=ASX%3AMQG&rsv_sug3=2&rsv_sug4=766&inputT=1031'
# browser = webdriver.Chrome(settings.DRIVER_PATH_CHROME)
# browser.get(url)
# ladning = LandingHome()
# result = ladning.get_all_data(browser)


# outside_api_caller = api.IntellifeedAPICaller()
# response = outside_api_caller.send_latest_news_data_to_mvp(result, 'gov')






# # -------------------  PBC News    ----------------------------------
# # -------------------  PBC News     ----------------------------------
#
# pbcgovnews = PbcGovNews()
# result = pbcgovnews.response_data()
# outside_api_caller = api.IntellifeedAPICaller()
# response = outside_api_caller.send_latest_news_data_to_mvp(result)
# print(response)




# -------------------  the following code is to fix the missing pdf on S3 ----------------------------------
# -------------------  the following code is to fix the missing pdf on S3 ----------------------------------

#
# cninfo_obj = CninfoScrape()
# with open('missing_pdf_uuid_list.txt.txt') as f:
#     data = json.loads(f.read())
#     base_pdf_url = 'https://s3-us-west-1.amazonaws.com/research-static-prod/intellifeeds/cninfo/pdf/'
#     static_url_cninfo = 'http://static.cninfo.com.cn/finalpage/'
#     outside_api_caller = api.IntellifeedAPICaller()
#     index = 1
#     failed_list = []
#     for item in data:
#
#         pdf_id = str(item['identifier'])
#         date_str = str(item['date'])
#         pdf_path_in_s3 = 'intellifeeds/cninfo/pdf/' + pdf_id + '.pdf'
#         pdf_url = base_pdf_url + pdf_id + '.pdf'
#         pdf_static_url_cninfo = static_url_cninfo + date_str + '/' + pdf_id + '.PDF'
#         now_cwd_path = cninfo_obj.created_folder_by_date_in_tmp(date_str)
#         os.path.abspath(os.path.join(os.path.dirname(now_cwd_path + pdf_id + '.pdf'), os.path.pardir))
#         os.chdir(now_cwd_path + '/' + date_str + '/')
#         try:
#             # first attempt
#             response = requests.get(pdf_static_url_cninfo, timeout=30)
#         except Exception as err:
#             print('download pdf failed ,the identifier id is %s' % item['identifier'])
#             failed_list.append({'date': item['date'], 'identifier': item['identifier']})
#             continue
#         with open(str(item['identifier']) + '.pdf', 'wb') as file:
#             file.write(response.content)
#         try:
#             # first attempt
#             result_upload_status = outside_api_caller.upload_file_to_s3(pdf_id + '.pdf', pdf_path_in_s3,
#                                                                         content_type='application/pdf')
#         except Exception as err:
#             try:
#                 print('retry upload 2 ')
#                 result_upload_status = outside_api_caller.upload_file_to_s3(pdf_id + '.pdf', pdf_path_in_s3,
#                                                                             content_type='application/pdf')
#             except Exception as e:
#                 try:
#                     print('retry upload 3')
#                     result_upload_status = outside_api_caller.upload_file_to_s3(pdf_id + '.pdf', pdf_path_in_s3,
#                                                                                 content_type='application/pdf')
#                 except Exception as err:
#                     print('time out ! pls upload  manually, the the identifier is %s' % item['identifier'])
#                     result_upload_status = None
#         if result_upload_status:
#             print('PDF has been upload to S3 Successfully. the identifier is %s' % item['identifier'])
#             index += 1
#         else:
#             print('PDF failed to upload . the identifier is %s' % item['identifier'])
#             failed_list.append({'date': item['date'], 'identifier': item['identifier']})
#         os.chdir('/Users/sam/intellifeed-kol/')
#         # if the amount of PDF in tmp folder is larger than 20, delete it
#         if index > 30:
#             folder = 'tmp/'
#             for the_file in os.listdir(folder):
#                 file_path = os.path.join(folder, the_file)
#                 print(file_path)
#                 try:
#                     if os.path.isfile(file_path):
#                         os.remove(file_path)
#                     elif os.path.isdir(file_path):
#                         shutil.rmtree(file_path)
#                     index = 0
#                 except Exception as e:
#                     print(e)
#     print(failed_list)


#
# for itm in missing_pdf_uuid_list:
#     if itm not in test_list:
#         print(itm)

# test = ScrapeYunCai('https://www.yuncaijing.com/tag/id_5.html', 'gov Yun Cai', 'cn')
# test_a = test.response_data()
# print(test_a)

#
#
# print(len(test_list))
# still_missing_uuid_list = []
# a = api.IntellifeedAPICaller()
#
# cninfo_obj = CninfoScrape()
# for item in test_list:
#     response = a.fetch_intellifeed_data_by_id(item)
#     if not isinstance(response, str):
#         print('op')
#         if response.status_code not in [200, 201]:
#             still_missing_uuid_list.append(item)
#             continue
#     else:
#         still_missing_uuid_list.append(item)
#         continue
#     if response.status_code in [200, 201]:
#         test = json.loads(response.content)
#     else:
#         still_missing_uuid_list.append(item)
#         continue
#     tmp_path = cninfo_obj.created_folder_by_date_in_tmp('2019-02-16')
#     today_tmp_path = tmp_path + '/' + '2019-02-16' + '/'
#     print(test['source_full_destination_url'])
#     # pdf_url = 'http://www.cninfo.com.cn/finalpage/' + test['date'] + '/' + test['identifier'] + '.PDF'
#     result_json = cninfo_obj.download_pdf_locality(test['source_full_route_url']+'?switchLocale=y&siteEntryPassthrough=true', today_tmp_path,
#                                                    test["source_identifier"])
#     os.chdir(cninfo_obj.main_path)
#     if isinstance(result_json, str):
#         still_missing_uuid_list.append(item)
#         print(item)
#         continue
# print(still_missing_uuid_list)
# #
#
# cninfo_obj.upload_pdf_to_s3()
# def tmp_folder_operat(index):
#     index = 10
#     folder = 'tmp/'
#     print(folder)
#     for the_file in os.listdir(folder):
#         file_path = os.path.join(folder, the_file)
#         print(file_path)
#         try:
#             if os.path.isfile(file_path):
#                 os.remove(file_path)
#             elif os.path.isdir(file_path):
#                 shutil.rmtree(file_path)
#             index = 0
#             print(index)
#         except Exception as e:
#             print(e)
#
# tmp_folder_operat(10)

#
# with open('missing_pdf_list.txt', 'r') as f:
#     data = json.loads(f.read())
#     print(data[0]['identifier'])



# tmp_list = [
#     {'date': '2019-01-18', 'identifier': '1205778687'}, {'date': '2019-01-18', 'identifier': '1205778730'}, {'date': '2019-01-18', 'identifier': '1205778726'}, {'date': '2019-01-18', 'identifier': '1205778688'}, {'date': '2019-01-17', 'identifier': '1205778578'}, {'date': '2019-01-17', 'identifier': '1205775853'}, {'date': '2019-01-15', 'identifier': '1205768595'}, {'date': '2019-01-15', 'identifier': '1205768673'}, {'date': '2019-01-14', 'identifier': '1205768760'}, {'date': '2019-01-14', 'identifier': '1205768732'}, {'date': '2019-01-12', 'identifier': '1205762194'}, {'date': '2019-01-12', 'identifier': '1205764820'}, {'date': '2019-01-12', 'identifier': '1205763581'}]
#
# for item in tmp_list:
#     # pdf_url, pdf_path, rp_id
#     test = CninfoScrape()
#     today_tmp_path = 'tmp' + '/' + item['date'] + '/'
#     pdf_url = 'http://www.cninfo.com.cn/finalpage/' + item['date'] + '/' + item['identifier'] + '.PDF'
#     result_json = test.download_pdf_locality(pdf_url, 'tmp/2018-01-20/', item['identifier'])





# -------------  the following code is to fix the missing pdf on S3 by Intellifeed Company  --------------------
# -------------  the following code is to fix the missing pdf on S3 by Intellifeed Company  ---------------------

