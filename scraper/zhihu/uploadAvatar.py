import time
from bs4 import BeautifulSoup as BS
from selenium import webdriver
import common.common


def get_avatar(urls):
    s3_urls = []
    for url in urls:
        browser = webdriver.Chrome(r"../../driver_binary_files/chromedriver_mac_xu")
        browser.get(url)
        time.sleep(0.2)

        source = browser.page_source
        time.sleep(0.2)
        browser.quit()

        html = BS(source, 'lxml')
        img_url = html.find('img', class_='Avatar Avatar--large UserAvatar-inner').attrs['src']
        title = 'zhihu_' + url.split('/')[-2] + '.' + img_url.split('.')[-1]
        s3_url = common.common.get_avatar_and_upload_to_s3(img_url, title)
        s3_urls.append(s3_url)
        print(s3_url)
    return s3_urls
