# import codecs
import requests
import re
import time
from lxml import etree
from urllib import request
from html.parser import HTMLParser
from common.api import IntellifeedAPICaller
from common.common import get_avatar_and_upload_to_s3, get_article_img_and_upload_to_s3

profile_obj = IntellifeedAPICaller()


def get_weibo_list(url):
    headers = {
        'User-Agent': 'User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
        'Cookie': '_T_WM=3fc89d2d4672526b80e5a99b66c959a0; SUB=_2A25xRU9UDeRhGeBG4lYW9CfOwzWIHXVSxlEcrDV6PUJbktAKLUX_kW1NQfbLFkOdihIYsWhyFvn6FDNWTXJ1_v-G; SUBP=0033WrSXqPxfM725Ws9jqgMF55529P9D9W5LN.aZxSmcI4ERO8q6lGDR5JpX5KzhUgL.FoqR1KBNSh.E1h.2dJLoIEXLxK-L1hnL1h5LxKqL1--LB-2LxKBLBonL12BLxK-L1h-LBKH0IGHXUspL1hnt; SUHB=0A0JFenbkjmrmU; SSOLoginState=1547779844; MLOGIN=1; XSRF-TOKEN=f041cf; M_WEIBOCN_PARAMS=luicode%3D20000174'
    }
    req = request.Request(url, headers=headers)
    reponse = request.urlopen(req)
    page_source = reponse.read()
    selector = etree.HTML(page_source)
    return selector


def get_pagecount(selector):
    page_count = selector.xpath('//*[@id="pagelist"]/form/div/input[1]/@value')
    page_count = int(page_count[0])
    return page_count


def change_Element_to_Html(content):
    from lxml import html
    content_user_1 = html.tostring(content)
    content_user_2 = HTMLParser().unescape(content_user_1.decode())
    html = content_user_2.replace('<span class="ctt">', '<span>')
    repl = re.findall(r'<a href="/n[\d\D]*>@[\d\D]{1,15}</a>', html)
    for i in repl:
        html = html.replace(i, '')
    html = html.replace('[', '')
    return html


def get_user_info(url):
    lst_new = {"nick_name": "", "real_name": "", "gender": "", "email": "", "avatar": "", "living_location": "",
               "industry": "", "career": "", "education": "", "self_intro": "", "original_url": "",
               "followers": {"weibo": {"follower": "", "fans": ""}}, "achievement": ""}
    selector_for_userinfo = get_weibo_list(url)
    userimage = selector_for_userinfo.xpath('/html/body/div[3]/img/@src')
    username = selector_for_userinfo.xpath('/html/body/div[6]/text()')
    if userimage == []:
        userimage = selector_for_userinfo.xpath('/html/body/div[4]/img/@src')
    if username == ['基本信息']:
        username = selector_for_userinfo.xpath('/html/body/div[7]/text()')
    for i in username:
        if i[:2] == '昵称':
            lst_new["nick_name"] = i[3:]
        if i[:2] == '性别':
            if i[3:] == '男':
                lst_new["gender"] = 'male'
            elif i[3:] == '女':
                lst_new["gender"] = 'female'
        if i[:2] == '地区':
            lst_new["living_location"] = i[3:]
        if i[:2] == '简介':
            lst_new["self_intro"] = i[3:]
    lst_new["original_url"] = url
    uid = re.findall(r'\d+', url)
    # r = requests.get(userimage[0])
    # with open('image/' + lst_new['nick_name'] + '.png', 'wb') as f:
    #     f.write(r.content)
    path = get_avatar_and_upload_to_s3(userimage[0], 'weibo_' + uid[0])
    lst_new["avatar"] = path
    return lst_new


def get_user_follower():
    selector_for_follower = get_weibo_list(url)
    follows = selector_for_follower.xpath('//*[@class="tip2"]/a[1]/text()')
    fans = selector_for_follower.xpath('//*[@class="tip2"]/a[2]/text()')
    follows_num = re.findall(r'-?[1-9]\d*', follows[0])
    fans_num = re.findall(r'-?[1-9]\d*', fans[0])
    return follows_num, fans_num


def get_user_href(url):
    selector_for_userhref = get_weibo_list(url)
    userinfo_href = selector_for_userhref.xpath('//*[@class="ut"]/a[2]/@href')
    lst_new = get_user_info('https://weibo.cn/' + userinfo_href[0])
    return lst_new


deleted = ['<span>抱歉，此微博已被作者删除。查看帮助：<a href=',
           '<span>抱歉，由于作者设置，你暂时没有这条微博的查看权限哦。查看帮助：<a href=',
           '<span>该微博因被多人投诉，根据《微博社区公约》，已被删除。查看帮助：<a href=']


def main(url, uid):
    selector_for_page = get_weibo_list(url)
    page_count = get_pagecount(selector_for_page)
    for i in range(209, page_count):
        # lst_weibo = []
        time.sleep(1)
        url_1 = url + '&page=' + str(i)
        selector_1 = get_weibo_list(url_1)
        List_1 = selector_1.xpath('//*[@class="c"]')
        weibo_number_in_page = 1
        for item in List_1:
            user_name = item.xpath('./div/span[@class="cmt"]/a[1]/text()')
            content_user_1 = item.xpath('./div/span[@class="ctt"]')
            # href = item.xpath('./div/span[@class="ctt"]/a/@href')
            sendtime = item.xpath('./div/span[@class="ct"]/text()')
            image = item.xpath('./div[2]/a[2]/text()')
            image_label = ""
            if image == ['原图']:
                imghref = item.xpath('./div[2]/a[2]/@href')
                image_uid = re.findall(r'https://weibo.cn/mblog/oripic\?id=(.*)=(.*)', imghref[0])
                image_href = 'http://ww1.sinaimg.cn/large/' + image_uid[0][1] + '.jpg'
                path = get_article_img_and_upload_to_s3(image_href, 'weibo_' + image_uid[0][1])
                print(path)
                image_label = '<img src=' + "\"" + path + "\""'>'
            content = {
                "content_info": {
                    "source_website_name": "weibo",
                    "source_domain_url": "https://weibo.cn/",
                    "source_full_destination_url": url_1 + '#' + str(weibo_number_in_page),
                    "publish_time": "",
                    "content_cn": "",
                    "title_cn": " ",
                    "kol_id": uid,
                    "types": "social_media"
                }
            }
            # if the weibo is transmitted
            weibo_content = ""
            if user_name:
                if user_name[0] != profile_data["nick_name"]:
                    fulltext = item.xpath('./div/span[@class="ctt"]/a/text()')
                    number = 0
                    if '全文' not in fulltext:
                        weibo_content = change_Element_to_Html(content_user_1[0]) + image_label
                    for text in fulltext:
                        number += 1
                        if text == '全文':
                            fulltext_href = item.xpath('./div/span[@class="ctt"]/a[' + str(number) + ']/@href')
                            fulltext_href = 'https://weibo.cn' + fulltext_href[0]
                            time.sleep(1)
                            selector_2 = get_weibo_list(fulltext_href)
                            List_2 = selector_2.xpath('//*[@class="c"]')
                            for item in List_2:
                                content_user_2 = item.xpath('./div/span[@class="ctt"]')
                                if content_user_2:
                                    forward_content = change_Element_to_Html(content_user_2[0]) + image_label
                                    weibo_content = forward_content
            # if the weibo is original
            else:
                if content_user_1:
                    original_content = change_Element_to_Html(content_user_1[0]) + image_label
                    hreflst = re.findall(r'<span>[\d\D]*<a href=', original_content)
                    hreflst.append('deleted')
                    if hreflst[0] not in deleted:
                        weibo_content = original_content
            if weibo_content:
                send_time = sendtime[0].split('\xa0')
                send_time = send_time[0].replace('月', '-').replace('日', '')
                if len(send_time) < 16:
                    send_time = '2019-' + send_time
                content['content_info']["content_cn"] = weibo_content
                content['content_info']["publish_time"] = send_time
                print(url)
                print(profile_obj.new_content(content))
                print(weibo_content)
                print(content)
            weibo_number_in_page += 1


if __name__ == '__main__':
    # f = open("url.txt")
    # lines = f.readlines()
    lines = ['https://weibo.cn/u/1265915191?filter=0']
    for line in lines:
        url = line.strip('\n')
        profile_data = get_user_href(url)
        time.sleep(1)
        follows_num, fans_num = get_user_follower()
        time.sleep(1)
        profile_data["followers"]["weibo"]["follower"] = follows_num[0]
        profile_data["followers"]["weibo"]["fans"] = fans_num[0]
        uid = profile_obj.new_kol_profile(profile_data).json()['id']
        print(uid)
        main(url, uid)
        time.sleep(1)
    # f.close()

    # url = "http://2018.ip138.com/ic.asp"
    # rsp = request.urlopen(url)
    # html = rsp.read().decode('GBK')
    # print(html)
