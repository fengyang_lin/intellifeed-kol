import json
import time
import os
import shutil
import settings
import requests
from common import api
from datetime import date
from scrawl.cninfo.cninfo import CninfoScrape
import os
import shutil

# --------------  [ From Wally ] the following code is to fix the missing pdf and data in the past      ------
# --------------  [ From Wally ] the following code is to fix the missing pdf and data in the past      -------


def delete_folder_from_tmp():
    folder = 'tmp/'
    for the_file in os.listdir(folder):
        print(the_file)
        file_path = os.path.join(folder, the_file)
        try:
            if os.path.isfile(file_path):
                os.remove(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print(e)


def get_companies_from_mvp():
    outside_api_caller = api.IntellifeedAPICaller()
    response = outside_api_caller.fetch_intellifeed_company_list()  # get intel companies from mvp
    if isinstance(response, str):
        response = outside_api_caller.fetch_intellifeed_company_list()  # try again
    if isinstance(response, str):
        return 'Connect Error'

    intellifeed_list_data = json.loads(response.content.decode(encoding='utf-8'))  # the list of companies to be scrawl
    cninfo = CninfoScrape()
    result_list = []
    print('[the total number of intellifeed comapanies is %s]' % len(intellifeed_list_data))
    for item in intellifeed_list_data:
        print('[ Now! the company id is %s begin to download ,the index is %s]'
              % (item['id'], str(intellifeed_list_data.index(item))))
        page = 1
        iter_ = True
        while iter_:
            time.sleep(5)
            url = 'http://www.cninfo.com.cn/new/fulltextSearch/full?searchkey=' + str(item['stock_code']) + \
                  '&sdate&edate&isfulltext=false&sortName=nothing&sortType=desc&pageNum=' + str(page)
            print('[ Now! the page %s is downloading ...]' % page)
            response = requests.post(url, timeout=30)
            json_data = json.loads(response.text)
            pdf_result_dict = {}
            json_list = json_data['announcements']  # get the data from cninfo search api
            total_page = int(json_data['totalpages'])

            for sub_item in json_list:
                dict_value = dict()
                dt = date.fromtimestamp(sub_item['announcementTime'] / 1000)
                dict_value["source_website_name"] = 'cninfo'
                dict_value["publish_time"] = dt.strftime("%Y-%m-%d %H:%M:%S")  # dt.strftime('YYYY-MM-DD HH:MM')
                dict_value["title_cn"] = sub_item['announcementTitle']
                dict_value["source_full_route_url"] = cninfo.domain_static_url + sub_item['adjunctUrl']  # s3
                dict_value["source_full_destination_url"] = \
                    cninfo.domain_url_in_s3 + sub_item['announcementId'] + '.pdf'
                dict_value["announcement_type_name"] = sub_item['announcementTypeName']
                dict_value["source_identifier"] = sub_item['announcementId']
                dict_value["announcement_type"] = sub_item['announcementType']
                dict_value['types'] = 'cn_info'
                dict_value['source_domain_url'] = settings.CNINFO_STATIC_URL_BASE
                pdf_result_dict["company_info"] = item
                print(dict_value["publish_time"])
                # # # create the date folder in the tmp and download the pdf
                try:
                    os.chdir(cninfo.main_path)
                    tmp_path = cninfo.created_folder_by_date_in_tmp(str(cninfo.now_date))
                    today_tmp_path = tmp_path + '/' + str(cninfo.now_date) + '/'
                    pdf_tmp_path = cninfo.download_pdf_locality(
                            cninfo.domain_static_url + sub_item['adjunctUrl'], today_tmp_path,
                            sub_item['announcementId'])
                except Exception as err:
                    print('OOOP some error happened when download PDF in locality, the error message is %s ' % str(err))
                    continue
                if settings.AWS_S3_CONNECTION:
                    print('[start to send pdf to s3]')
                    try:
                        s3_upload_file = cninfo.upload_pdf_to_s3(pdf_tmp_path, sub_item['announcementId'])
                        print('result S3 upload !' + str(s3_upload_file))
                        time.sleep(2)
                    except Exception as err:
                        try:
                            print('[Upload PDF 2 ....]')
                            s3_upload_file = cninfo.upload_pdf_to_s3(pdf_tmp_path, sub_item['announcementId'])
                            print('result S3 upload 2!' + str(s3_upload_file))
                            time.sleep(3)
                        except Exception as e:
                            try:
                                print('[Upload PDF 3 ....]')
                                s3_upload_file = cninfo.upload_pdf_to_s3(pdf_tmp_path, sub_item['announcementId'])
                                print('result S3 upload 3 !' + str(s3_upload_file))
                                time.sleep(3)
                            except Exception as erro:
                                s3_upload_file = False
                                print('[OOp! Some Error to S3], '
                                      'the id is %s and the error is %s' % (sub_item['announcementId'], str(erro)))
                                continue
                else:
                    s3_upload_file = False
                    print('[No Data to S3], the id is %s' % str(sub_item['announcementId']))
                # send data to mvp backend
                if settings.MVP_BACKEND_CONNECTION and s3_upload_file:
                    try:
                        # extract data from pdf
                        dict_value["content_cn"] = cninfo.extract_data_from_pdf(
                            pdf_tmp_path + '/' + sub_item['announcementId'] + '.pdf')
                        if not dict_value["content_cn"].strip() > "":
                            continue
                        pdf_result_dict["content_info"] = dict_value
                        response = cninfo.outside_api_caller.new_content(pdf_result_dict)
                        if isinstance(response, str):
                            print('some error to mvp backend 2...,the err is %s' % str)
                            response = cninfo.outside_api_caller.new_content(pdf_result_dict)
                            if isinstance(response, str):
                                print('some error to mvp backend 3,the err is %s' % str)
                                response = cninfo.outside_api_caller.new_content(pdf_result_dict)
                                if isinstance(response, str):
                                    print('some error to mvp backend 2,the err is %s' % str)
                                    continue  # give up  the cninfo item
                        response_status = response.status_code
                        if response_status in [200, 201]:
                            result_list.append(pdf_result_dict)
                            print('[Mvp Data Process Status ]: status code is: ' + str(response_status))
                        else:
                            print('[OOp! Some Error to Mvp Backend ]:the error status code is %s ' % str(response_status))
                    except Exception as err:
                        print('[OOp! Some Error to Mvp Backend 2]:the error message is %s ' % err)
                        continue
                    finally:
                        print('*' * 20 + 'Ending ! ' + '*' * 20)
                else:
                    print('[No Data to Mvp]')
            # every 10 will clean the tmp folder
            os.chdir(cninfo.main_path)
            delete_folder_from_tmp()
            page += 1
            if page == total_page:
                iter_ = False


get_companies_from_mvp()
