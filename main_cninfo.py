# -*- coding: utf-8 -*-
from scrawl.cninfo.cninfo import CninfoScrape
import settings
from common import api
import time
import os

outside_api_caller = api.IntellifeedAPICaller()


# def delete_all_file_under_tmp():
#     folder = 'tmp/'
#     for the_file in os.listdir(folder):
#         file_path = os.path.join(folder, the_file)
#         try:
#             if os.path.isfile(file_path):
#                 os.remove(file_path)
#             elif os.path.isdir(file_path):
#                 shutil.rmtree(file_path)
#         except Exception as e:
#             print(e)


def main_gov():
    while True:
        try:
            outside_api_caller.send_slack_notification('[Start Cninfo Scrawl]')
            cninfo = CninfoScrape()
            _ = cninfo.get_content()
            outside_api_caller.send_slack_notification('[ End Cninfo Scrawl]')
        except Exception as err:
            outside_api_caller.send_slack_notification('[ Fail to Cninfo Scrawl], the error msg is ' % str(err))
        time.sleep(86400)  # run every day


main_gov()