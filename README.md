Intellifeed-kols
==================================

 - supply API to insert data to Redshift (DB House)

 - - Spider for below Data
   * 雪球 - Silvia (https://xueqiu.com/)
   * 微博 - Ansel (https://weibo.com/ )
   * 知乎 - Sara (https://zhihu.com/ )

## Useful Commands

**Check Codestyle**

##### `pycodestyle mvp_api scraper settings utility`

**Check Lint**

##### `pylint mvp_api scraper settings utility` 
