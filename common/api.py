#  -*-coding:utf-8 -*-
import requests
import boto3
import os
from botocore.client import Config
import settings
import platform
import json
from retrying import retry


class IntellifeedAPICaller(object):
    headers = {'Authorization': 'Token ' + settings.MVP_ACCOUNT_TOKEN, 'Content-Type': 'application/json'}

    # post data to mvp
    def new_content(self, content):
        """
        #### "content" sample json:
        {
            "content_info": {
                "source_website_name": "xueqiu",
                "source_domain_url": "https://xueqiu.com/",
                "source_full_destination_url": "https://xueqiu.com/2489529751/118946095",
                "publish_time": "2018-12-27 14:36",
                "content_cn": "</p>6月17日，东阿阿胶公司表示，复方阿胶浆退出医保目录的工作正在推进中，具体结果视工作进度而确定。
                <p>两年之后，东阿阿胶终于打算放弃复方阿胶浆的医院渠道。</p><p>6月16日，东阿阿胶董事长秦玉峰在公司股东大会上表示：
                “按现在的零售价，复方阿胶浆生产一支就亏一支。考虑先退出医保，等药品价格改革后再进去。”</p><p>
                这是东阿阿胶官方首次公开表示有退出医保的想法，2011年时，东阿阿胶也曾面对医保药品降价的压力，但仍坚持“不谋求退出医保，
                不会大幅提价”。</p><p>多位医药界资深人士表示：“退出基药的案例有不少，但退出医保目录的例子非常罕见，毕竟医保目录进入有难度。
                ”</p><p>6月17日，东阿阿胶方面回应称：“长期以来，复方阿胶浆一直面临着巨大的成本压力，其价值并没有得到充分体现。
                为了保护这一产品，因此公司的确正在考虑复方阿胶浆退出医保目录的可能性。”</p><p><b>纳入医保意味着销量扩张</b></p>",
                "title_cn": "关于复方阿胶浆退出医保的事",
                "announcement_type_name": "<cn_info_only>",
                "announcement_type": "<cn_info_only>",
                "types": "<social_media>/<cn_info>/<news>",
                "kol_id": "<uuid>"
            },
            "company_info": {
                "full_name": "北京元隆雅图文化传播股份有限公司",
                "en_name": "Beijing Yuanlong Yato Culture Dissemination Co., Ltd.",
                "cn_name": "北京元隆雅图文化传播股份有限公司",
                "nation": "china",
                "address": "北京市西城区广安门内大街12层1218",
                "established_time": "",
                "industry": "商务服务业",
                "parent_company": "",
                "subsidiaries": "",
                "offical_website": "www.ylyato.com",
                "phone": "010-83528822",
                "fax": "010-83528011",
                "Twitter": "",
                "president": "孙震",
                "secretary": "相文燕",
                "reg_capital": "13,040.6410",
                "ipo_time": "2017-06-06",
                "prospectus_time": "2017-05-19",
                "ipo_vol": "1,884",
                "ipo_price": "14.48",
                "ipo_pe_ratio": "22.75",
                "shareholders": "",
                "stock_code": "002878",
                "short_name": "元隆雅图"
            }
        }
        """

        url = settings.MVP_BACKEND_URL_BASE + settings.MVP_BACKEND_URL_INTELLIFEEDS
        try:
            content_json = json.dumps(content)
            response = requests.post(url, data=content_json, headers=self.headers)
            return response
        except requests.exceptions.HTTPError as errh:
            print("Http Error:", errh)
        except requests.exceptions.ConnectionError as errc:
            print("Error Connecting:", errc)
        except requests.exceptions.Timeout as errt:
            print("Timeout Error:", errt)
        except requests.exceptions.RequestException as err:
            print("OOps: Something Else", err)
        return ''

    #  send user profile to mvp and return user id
    def new_kol_profile(self, profile_data):
        """

        #### "profile_data" sample json:
        {
        "nick_name": "wally", "real_name": "wally yu", "gender": "male",
        "email": "wally.yu@redpulse.com",
         "avatar": "https://static.ws.126.net/f2e/www/index20170701/images/sprite_img_20181029.svg",
         "living_location": "shanghai", "industry": "engineering", "career": "something json format",
         "education": "something json format", "self_intro": "some introduction",
         "original_url": "http://www.xxxxx.xxx",
         "is_weixin": "False",
         "weixin_info": "<JSONField>",
         }

        More info: https://gitlab.com/redpulse/dev/intellifeed-kol/snippets/1795823

        :return: Success or Failed
        """
        url = settings.MVP_BACKEND_URL_BASE + settings.MVP_BACKEND_URL_USER_PROFILES
        try:
            response = requests.post(url, json=profile_data, headers=self.headers)
            return response
        except requests.exceptions.HTTPError as errh:
            print("Http Error:", errh)
        except requests.exceptions.ConnectionError as errc:
            print("Error Connecting:", errc)
        except requests.exceptions.Timeout as errt:
            print("Timeout Error:", errt)
        except requests.exceptions.RequestException as err:
            print("OOps: Something Else", err)
        return ''

    def check_kol_profile(self, profile_data):
        """
        The API detects whether a profile exists in the database by passing in the nick_name

        #### "profile_data" sample json:
        {
        "nick_name": "wally"
         }

        :return: request success or Failed
        """
        url = settings.MVP_BACKEND_URL_BASE + settings.MVP_BACKEND_URL_CHECK_USER_PROFILES
        try:
            response = requests.get(url, json=profile_data, headers=self.headers)
            return response
        except requests.exceptions.HTTPError as errh:
            print("Http Error:", errh)
        except requests.exceptions.ConnectionError as errc:
            print("Error Connecting:", errc)
        except requests.exceptions.Timeout as errt:
            print("Timeout Error:", errt)
        except requests.exceptions.RequestException as err:
            print("OOps: Something Else", err)
        return ''

    def check_IntelliFeedContent(self, profile_data):
        """
        The API detects whether a article exists in the database by passing in the title_cn

        #### "profile_data" sample json:

        {"title_cn": "简式权益变动报告书（一）"}

        :return: request success or Failed
        """
        url = settings.MVP_BACKEND_URL_BASE + settings.MVP_BACKEND_URL_CHECK_INTELLIFEEDS
        try:
            response = requests.get(url, json=profile_data, headers=self.headers)
            return response
        except requests.exceptions.HTTPError as errh:
            print("Http Error:", errh)
        except requests.exceptions.ConnectionError as errc:
            print("Error Connecting:", errc)
        except requests.exceptions.Timeout as errt:
            print("Timeout Error:", errt)
        except requests.exceptions.RequestException as err:
            print("OOps: Something Else", err)
        return ''

    def query_kol_profile(self, profile_data):
        """

        #### "profile_data" sample json:
        {"user_id": "affae5cb-0e3e-479b-b965-875d7e65a77e"}

        :return: request success or Failed
        """
        url = settings.MVP_BACKEND_URL_BASE + settings.MVP_BACKEND_URL_QUERY_USER_PROFILES
        try:
            response = requests.get(url, json=profile_data, headers=self.headers)
            return response
        except requests.exceptions.HTTPError as errh:
            print("Http Error:", errh)
        except requests.exceptions.ConnectionError as errc:
            print("Error Connecting:", errc)
        except requests.exceptions.Timeout as errt:
            print("Timeout Error:", errt)
        except requests.exceptions.RequestException as err:
            print("OOps: Something Else", err)
        return ''

    def update_kol_profile(self, profile_data):
        """
        The API update user follower through user_id
        #### "profile_data" sample json:
        {
        "user_id": "affae5cb-0e3e-479b-b965-875d7e65a77e",
        "followers": "{"xueqiu": {"fans": "89", "follower": "140286", "likes": "11"}}",
        }
        :return: request success or Failed
        """
        url = settings.MVP_BACKEND_URL_BASE + settings.MVP_BACKEND_URL_UPDATE_USER_PROFILES
        try:
            response = requests.get(url, json=profile_data, headers=self.headers)
            return response
        except requests.exceptions.HTTPError as errh:
            print("Http Error:", errh)
        except requests.exceptions.ConnectionError as errc:
            print("Error Connecting:", errc)
        except requests.exceptions.Timeout as errt:
            print("Timeout Error:", errt)
        except requests.exceptions.RequestException as err:
            print("OOps: Something Else", err)
        return ''

    #  send user profile to mvp redpulse  and return user id
    @staticmethod
    def upload_file_to_s3(file_name, key_name, content_type='application/json', bucket_name=None):
        """
        :param file_name: file abs PATH to be uploaded.
        :param bucket_name: The name of the bucket to upload to.
        :param key_name: The name of the key to upload to. (Manually to set the same to file name by default)
        key name is the path of S3
        :param content_type: upload file type
        :return: True or False for success or not
        """
        config = Config(connect_timeout=40, retries={'max_attempts': 4})
        if not bucket_name:
            bucket_name = settings.AWS_BUCKET_NAME
        try:
            s3 = boto3.client(
                's3',
                aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
                aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
            )
            s3.config = config
            _ = s3.upload_file(file_name, bucket_name, key_name, ExtraArgs={'ContentType': content_type})
            return True
        except TypeError as err:  # pylint: disable=unused-variable
            print('error message is ' + str(err))
            return False
        except requests.exceptions.ConnectionError:
            return False
        except Exception as err:  # pylint: disable=unused-variable
            print('finally error message is ' + str(err))
            return False

    # get the latest news data from postgres
    def send_latest_news_data_to_mvp(self, news_list, data_type):
        url = settings.MVP_BACKEND_URL_BASE + settings.MVP_BACKEND_URL_NEWS_DATA
        # url = 'http://localhost:8000/api/v1/receive-news-data/'
        if data_type == 'gov':
            json_obj = json.dumps({'gov_data': news_list})
        elif data_type == 'news':
            json_obj = json.dumps({'news_data': news_list})
        else:
            return None
        try:
            response = requests.post(url, json=json_obj, headers=self.headers)
            return response
        except requests.exceptions.HTTPError as errh:
            print("Http Error:", errh)
            return False
        except requests.exceptions.ConnectionError as errc:
            print("Error Connecting:", errc)
            return False
        except requests.exceptions.Timeout as errt:
            print("Timeout Error:", errt)
            return False
        except requests.exceptions.RequestException as err:
            print("OOps: Something Else", err)
            return False
        except Exception as errm:
            print("OOps: Something Else", errm)
            return False

    # get the intellifeed data by uuid, the function is to check missing pdf in the mvp
    @retry(stop_max_attempt_number=3)
    def fetch_intellifeed_data_by_id(self, intel_id):
        if intel_id:
            url = 'http://api.redpulse.com/api/v1/intellifeeds/' + str(intel_id)
        else:
            url = 'http://api.redpulse.com/api/v1/intellifeeds/'
        try:
            response = requests.get(url, headers=self.headers)
            return response
        except requests.exceptions.HTTPError as errh:
            print("Http Error:", errh)
        except requests.exceptions.ConnectionError as errc:
            print("Error Connecting:", errc)
        except requests.exceptions.Timeout as errt:
            print("Timeout Error:", errt)
        except requests.exceptions.RequestException as err:
            print("OOps: Something Else", err)
        return ''

    # get the intellifeed data by uuid, the function is to check missing pdf in the mvp
    def fetch_intellifeed_company_list(self):
        url = 'http://api.redpulse.com/api/v1/intellifeedCompanies/'
        try:
            response = requests.get(url, headers=self.headers)
            return response
        except requests.exceptions.HTTPError as errh:
            print("Http Error:", errh)
        except requests.exceptions.ConnectionError as errc:
            print("Error Connecting:", errc)
        except requests.exceptions.Timeout as errt:
            print("Timeout Error:", errt)
        except requests.exceptions.RequestException as err:
            print("OOps: Something Else", err)
        return ''

    def get_gov_file_and_upload_to_s3(self, file_url, file_name, suffix_name, file_type):
        """
        Download Gov Image / PDF / Xls and Upload to S3
        :param url:
        :param title:
        :return:  S3 file url
        """
        repsonse = requests.get(file_url)

        tmp_folder = settings.DEFAULT_CHROME_DOWNLOAD_PATH  # '/Users/sam/intellifeed-kol/tmp/'
        # temp_foler = tempfile.gettempdir() + '\\' if "Windows" in platform.platform() else '/tmp/'
        file_abs_path = tmp_folder + file_name
        print('file_abs_path', file_abs_path)
        print(settings.DEFAULT_CHROME_DOWNLOAD_PATH)
        os.chdir(settings.DEFAULT_CHROME_DOWNLOAD_PATH)
        with open(file_name, 'wb') as f:
            f.write(repsonse.content)
        result = self.upload_file_to_s3(file_abs_path, 'intellifeeds/gov/%s' % file_name, file_type)
        print('upload to s3 result :' + str(result))
        static_image_in_s3_path = settings.AWS_S3_Intellifeed_HEAD + 'gov/' + file_name

        if static_image_in_s3_path:
            return static_image_in_s3_path
        else:
            return False
        # try:
        #     os.remove(file_abs_path)
        #     return static_image_in_s3_path
        # except Exception as e:  # pylint: disable=unused-variable
        #     print('Fail to delete the tmp folder')
        #     return False

    def intellifeed_regular_job_record(self, result_data):
        url = settings.MVP_BACKEND_URL_BASE + settings.MVP_BACKEND_URL_INTELLIFEEDS_STATUS
        url = settings.LOCAL_MVP_BACKEND_URL_BASE + settings.MVP_BACKEND_URL_INTELLIFEEDS_STATUS
        result_data = json.dumps(result_data)
        return requests.post(url, data={'data': result_data})

    @staticmethod
    def send_slack_notification(message, icon_emoji=":alien:", channel="#engineering_debug"):
        try:
            # Sending billing information to slack
            slack_api = settings.SLACK_HOOK_API
            if settings.IS_PUBLIC_PROD_ENV:  # Production Environment, send to "channel"
                request_body = {"channel": channel, "text": message,
                                "username": "Platform Message", "icon_emoji": icon_emoji}
                response = requests.post(url=slack_api, data=json.dumps(request_body),
                                         headers={'Content-Type': 'application/json'})
                return True if response.status_code == 200 else False
            elif settings.IS_PRODUCTION and not settings.IS_PUBLIC_PROD_ENV:  # dev env
                channel = 'engineering_debug_dev'  # if dev env, send to this specified channel
                request_body = {"channel": channel, "text": message,
                                "username": "Platform Message", "icon_emoji": icon_emoji}

                response = requests.post(url=slack_api, data=json.dumps(request_body),
                                         headers={'Content-Type': 'application/json'})
                return True if response.status_code == 200 else False

            else:
                print('Slack information only sent from production' + message)
                return True
        except Exception as err:
            print('Slack Sending Fail, Error msg: %s, original info: %s' % (err, message))
