import os
import platform
import tempfile
import boto3
import requests
from retrying import retry
import settings


def get_article_img_and_upload_to_s3(img_url, img_file_name):
    """
    Download Article Image and Upload to S3
    :param url:
    :param title:
    :return:  S3 image url
    """
    img = requests.get(img_url)
    temp_foler = tempfile.gettempdir() + '\\' if "Windows" in platform.platform() else '/tmp/'
    file_abs_path = temp_foler + img_file_name
    print(file_abs_path)
    file_handler = open(file_abs_path, 'wb')
    file_handler.write(img.content)
    file_handler.close()
    upload_file_to_s3(file_abs_path, 'intellifeeds/social_media/%s' % img_file_name, 'image/png')

    static_image_in_s3_path = settings.AWS_S3_Intellifeed_HEAD + 'social_media/' + img_file_name
    try:
        os.remove(img_file_name)
    except Exception as e:  # pylint: disable=unused-variable
        pass
    return static_image_in_s3_path


def get_avatar_and_upload_to_s3(avatar_url, avatar_file_name):
    """
    Download User Profile Avatar and Upload to S3
    :param url:
    :param title:
    :return:  S3 image url
    """
    print(avatar_file_name)
    img = requests.get(avatar_url)
    temp_foler = tempfile.gettempdir() + '\\' if "Windows" in platform.platform() else '/tmp/'
    file_abs_path = temp_foler + avatar_file_name
    print(file_abs_path)
    file_handler = open(file_abs_path, 'wb')
    file_handler.write(img.content)
    file_handler.close()
    upload_file_to_s3(file_abs_path, 'intellifeeds/kol_profile/%s' % avatar_file_name, 'image/png')

    static_image_in_s3_path = settings.AWS_S3_Intellifeed_HEAD + 'kol_profile/' + avatar_file_name
    try:
        os.remove(avatar_file_name)
    except Exception as e:  # pylint: disable=unused-variable
        pass
    return static_image_in_s3_path


#  send user profile to mvp redpulse  and return user id
@retry()
def upload_file_to_s3(file_name, key_name, content_type='application/json', bucket_name=None):
    """
    :param file_name: file abs PATH to be uploaded.
    :param bucket_name: The name of the bucket to upload to.
    :param key_name: The name of the key to upload to. (Manually to set the same to file name by default)
    key name is the path of S3
    :param content_type: upload file type
    :return: True or False for success or not
    """
    if not bucket_name:
        bucket_name = settings.AWS_BUCKET_NAME
    try:
        s3 = boto3.client(
            's3',
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
        )
        print(1)
        s3.upload_file(file_name, bucket_name, key_name, ExtraArgs={'ContentType': content_type})
        return True
    except TypeError as err:  # pylint: disable=unused-variable
        raise
