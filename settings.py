import os
from os import environ

# -------------------  GENERAL SETTINGS   ----------------------------------
steps = ''
ROOT_DIR = os.path.dirname(os.path.dirname(__file__))
APPS_DIR = os.path.dirname(__file__)

# Obey robots.txt rules
ROBOTSTXT_OBEY = True

LOG_ENABLED = environ.get('LOG_ENABLED', False)

PDF_PAGES_TO_TEXT = environ.get('PDF_PAGES_TO_TRANSLATE', 2)
PDF_PAGES_TO_TRANSLATE = environ.get('PDF_PAGES_TO_TRANSLATE', 3)

MVP_CONNECTION = environ.get("MVP_CONNECTION", True)

MVP_BACKEND_URL_BASE = environ.get('MVP_BACKEND_URL_BASE', 'https://api.redpulse.com/')

LOCAL_MVP_BACKEND_URL_BASE = environ.get('MVP_BACKEND_URL_BASE', 'http://localhost:8000/')
MVP_BACKEND_URL_BASE_DEV = environ.get('MVP_BACKEND_URL_BASE', 'https://api.v2-dev.redpulse.com/')

MVP_BACKEND_URL_KOLS = environ.get('MVP_BACKEND_URL_INTELLIFEED', 'api/v1/intellifeeds/')

LOCAL_MVP_BACKEND_URL_BASE = environ.get('MVP_BACKEND_URL_BASE', 'https://localhost:8000/')

MVP_BACKEND_URL_INTELLIFEEDS = environ.get('MVP_BACKEND_URL_INTELLIFEED', 'api/v1/intellifeeds/')

MVP_BACKEND_URL_USER_PROFILES = environ.get('MVP_BACKEND_URL_USER_PROFILE', 'api/v1/kol-profile/')

MVP_BACKEND_URL_CHECK_USER_PROFILES = environ.get('MVP_BACKEND_URL_USER_PROFILE', 'api/v1/account/check-kol-profile/')

MVP_BACKEND_URL_CHECK_INTELLIFEEDS = environ.get('MVP_BACKEND_URL_INTELLIFEED',
                                                 'api/v1/account/check-intellifeed-content/')
MVP_BACKEND_URL_NEWS_DATA = environ.get('MVP_BACKEND_URL_NEWS_DATA', 'api/v1/receive-news-data/')

MVP_BACKEND_URL_INTELLIFEEDS_STATUS = environ.get('MVP_INTELLIFEED_STATUS_RECORD',
                                                  'api/v1/receive-intellifeed-regular-jobs-status/')

MVP_BACKEND_URL_QUERY_USER_PROFILES = environ.get('MVP_BACKEND_URL_USER_PROFILE', 'api/v1/account/query-kol-profile/')

MVP_BACKEND_URL_UPDATE_USER_PROFILES = environ.get('MVP_BACKEND_URL_USER_PROFILE', 'api/v1/account/update-kol-profile/')

MVP_ACCOUNT_USERNAME = environ.get('MVP_ACCOUNT_USERNAME', 'testing@redpulse.com')
MVP_ACCOUNT_PASSWORD = environ.get('MVP_ACCOUNT_PASSWORD', 'mvp4ever')

MVP_ACCOUNT_TOKEN = environ.get('MVP_ACCOUNT_TOKEN', '20cd69d3770e1a8c21be8ca05cbf592b689382e3')

LOCAL_MVP_ACCOUNT_TOKEN = environ.get('LOCAL_MVP_ACCOUNT_TOKEN', 'bce44bc88fecce71732a8e99f2536cf95aab9395')
IS_PUBLIC_PROD_ENV = environ.get('IS_PUBLIC_PROD_ENV', default=False)

IS_PRODUCTION = environ.get('IS_PRODUCTION', False)

AWS_SECRET_ACCESS_KEY = environ.get('AWS_SECRET_ACCESS_KEY', default='UdX7VI+fccXExZ0gMRSUPfZFUaxXOvXhif0KeM7m')

AWS_BUCKET_NAME = environ.get('AWS_BUCKET_NAME', default='research-static-prod')
AWS_ACCESS_KEY_ID = environ.get('AWS_ACCESS_KEY_ID', default='AKIAJO77UCJUZKBZSNCA')

# To send data to slack
SLACK_HOOK_API = 'https://hooks.slack.com/services/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'

PDF_PAGES_TO_TEXT = environ.get('PDF_PAGES_TO_TRANSLATE', 10)

AWS_S3_CONNECTION = environ.get('AWS_S3_CONNECTION', True)
MVP_BACKEND_CONNECTION = environ.get('MVP_BACKEND_CONNECTION', True)
# OC_CONNECTION = environ.get('OC_CONNECTION', True)
#
# OC_URL = environ.get('OC_URL', 'https://api.thomsonreuters.com/permid/calais')

# Token from liyang-leon@hotmail.
# OC_TOKEN = environ.get('OC_TOKEN', 'k0WEvloFPxGf1tUT30LsIQP4Gc4KEx7j')
#
# GOOGLE_TRANSLATION = environ.get('GOOGLE_TRANSLATION', True)
# GOOGLE_TRANSLATION_URL = environ.get('GOOGLE_TRANSLATION_URL',
#                                      'https://translation.googleapis.com/language/translate/v2/')
# GOOGLE_TRANSLATION_TOKEN = environ.get('GOOGLE_TRANSLATION_TOKEN', 'AIzaSyC1bxoX20MvtuJZu2TYiTGxf-mjksfsz_4')


# Place the url for Scrape  here for easy adjustment.
URL_WEIBO_KOLS = environ.get('URL_WEIBO_KOLS', 'https://weibo.com/')

URL_XUEQIU_KOLS = environ.get('URL_XUEQIU_KOLS', 'https://www.zhihu.com/')

URL_ZHIHU_KOLS = environ.get('URL_ZHIHU_KOLS', 'https://xueqiu.com/')

# SELENIUM WAITING TIME
T_MINI = 2
T_SHORT = 5
T_MIDDLE = 10
T_LONG = 20

# SELENIUM DRIVER
# DRIVER_PATH_CHROME = os.path.join(os.path.dirname(__file__),
#                                   'driver_binary_files',
#                                   'chromedriver_mac')
# #

DRIVER_PATH_CHROME = os.path.join(os.path.dirname(__file__),
                                  'driver_binary_files',
                                  'headless-chromium')

os.environ["webdriver.chrome.driver"] = DRIVER_PATH_CHROME

DEMO = True

AWS_S3_Intellifeed_HEAD = 'https://static.redpulse.com/intellifeeds/'

# -------------------  ZHIHU SETTINGS   ----------------------------------

ZHIHU_G_TOP_WRITTER_URLS = ['https://www.zhihu.com/people/gao-er-ji-13/activities',
                            'https://www.zhihu.com/people/xia-xia-17/activities',
                            'https://www.zhihu.com/people/patrickluo/activities']

ZHIHU_KOL_IDs = {}

ZHIHU_URLS = ['https://www.zhihu.com/topic/19609455/top-writer',
              'https://www.zhihu.com/topic/19550780/top-writer',
              'https://www.zhihu.com/topic/19555355/top-writer',
              'https://www.zhihu.com/topic/19600228/top-writer',
              'https://www.zhihu.com/topic/19901773/top-writer']

ZHIHU_ACCOUNTS = [
    {
        "username": '18001627762',
        "password": 'mvp4ever',
    },
]

ZHIHU_HEADERS = {'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                 'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7,zh-CN;q=0.6',
                 'Connection': 'keep-alive',
                 'Cookie': '_zap=2e10ea93-2025-4b82-b46a-6555229acf48; d_c0="APBiEfBskA6PTs_'
                           'skmkbhI1BTJg4bXS4lNA=|1542990402"; _xsrf=sJ91D2iFqWrtBR5Ud103Q0S9mb7RLUBc; '
                           '__gads=ID=2155fc22a21eb181:T=1546844047:S=ALNI_MY4i2QQ-7EUkMEp5mXx9GfgS1k42Q; '
                           'l_n_c=1; n_c=1; q_c1=1191deb3ced042aabd97cd746b5fd3ad|1546845140000|1543283282000;'
                           ' __utmc=51854390; __utmz=51854390.1546845142.1.1.utmcsr=zhihu.com|utmccn=(referral)|'
                           'utmcmd=referral|utmcct=/signup; __utmv=51854390.100--|2=registration_date=20170322=1^3='
                           'entry_date=20170322=1; __utma=51854390.674286145.1546845142.1546845142.1547027573.2; '
                           'l_cap_id="NWU3OWQ5M2Q3NmM5NGQ0ZmEzZTYwYzc5YTA0NzJmNzE=|1547027590|'
                           '119fea4321ac57275ed979b9f671df248bd273f6"; '
                           'r_cap_id="ZWY5NTE5YzRjYTIzNDZiZjk5M2ExNThjYjkxY2I4NGE=|1547027589|'
                           'a681b0127a3f805f3e5d1336366aa1b2f42c82c1"; cap_id="MGMyNDFkMGYyYzllNDNiNWI0NTA4ZDJiZTZkZTky'
                           'Y2E=|1547027589|084d1d54bdb9e1bd12900c380eb2a065979e9735"; tst=r; '
                           'tgw_l7_route=66cb16bc7f45da64562a077714739c11; capsion_ticket="2|1:0|10:1547090056|14:'
                           'capsion_ticket|44:MzI1MzlkMmJlNjE0NDgxMzhhY2IyMTkwOTUyNDU1MmM=|'
                           'd51c5f0343644f056fbc4afd35045708762da95761c150e2cf00234a529a7e85"; z_c0="2|1:0|10:154709058'
                           '4|4:z_c0|92:Mi4xVlJUbERRQUFBQUFBOEdJUjhHeVFEaVlBQUFCZ0FsVk5tQWdrWFFBOC1NWUFSSTN4YWtfWGla'
                           'SE5nazVUM1FoM3N3|ece3ff8e86485acac5b96755d22147af376b0cb2e9406faf4c67e7bd427375fb"',
                 'Host': 'www.zhihu.com',
                 'Referer': 'https://www.zhihu.com/signup?next=%2Ftopic%2F19609455%2Ftop-writer',
                 'Upgrade-Insecure-Requests': '1',
                 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                               'Chrome/71.0.3578.98 Safari/537.36'}

ZHIHU_LOGIN_URL = 'https://www.zhihu.com/signup?next=%2F'

# -------------------  CNINFO SETTINGS   ----------------------------------
CNINFO_STATIC_URL_BASE = environ.get('CNINFO_STATIC_URL_BASE', 'http://static.cninfo.com.cn/')
CNINFO_URL_IN_S3 = environ.get('CNINFO_URL_IN_S3', 'https://s3-us-west-1.amazonaws.com/research-static-prod/intellif'
                                                   'eeds/cninfo/pdf/')
CNINFO_COMPANY_INFO_URL_BASE = environ.get('CNINFO_COMPANY_INFO_URL_BASE',
                                           'http://www.cninfo.com.cn/information/brief/')
URL_CNINFO_PDF_DOWNLOAD = environ.get('URL_CNINFO_PDF_DOWNLOAD',
                                      'http://www.cninfo.com.cn/cninfo-new/disclosure/szse/download/')

# -------------------  XUEQIU SETTINGS   ----------------------------------

XUEQIU_HEADERS = headers = {'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7,zh-CN;q=0.6',
                            'Connection': 'keep-alive',
                            'Cookie': '_zap=2e10ea93-2025-4b82-b46a-6555229acf48; d_c0="APBiEfBskA6PTs_skmkbhI1BTJg4bX'
                                      'S4lNA=|1542990402"; _xsrf=sJ91D2iFqWrtBR5Ud103Q0S9mb7RLUBc; __gads=ID=2155fc22'
                                      'a21eb181:T=1546844047:S=ALNI_MY4i2QQ-7EUkMEp5mXx9GfgS1k42Q; l_n_c=1; n_c=1; q_'
                                      'c1=1191deb3ced042aabd97cd746b5fd3ad|1546845140000|1543283282000; __utmc=518543'
                                      '90; __utmz=51854390.1546845142.1.1.utmcsr=zhihu.com|utmccn=(referral)|utmcmd=r'
                                      'eferral|utmcct=/signup; __utmv=51854390.100--|2=registration_date=20170322=1^3'
                                      '=entry_date=20170322=1; __utma=51854390.674286145.1546845142.1546845142.154702'
                                      '7573.2; l_cap_id="NWU3OWQ5M2Q3NmM5NGQ0ZmEzZTYwYzc5YTA0NzJmNzE=|1547027590|119fe'
                                      'a4321ac57275ed979b9f671df248bd273f6"; r_cap_id="ZWY5NTE5YzRjYTIzNDZiZjk5M2ExNTh'
                                      'jYjkxY2I4NGE=|1547027589|a681b0127a3f805f3e5d1336366aa1b2f42c82c1"; cap_id="MG'
                                      'MyNDFkMGYyYzllNDNiNWI0NTA4ZDJiZTZkZTkyY2E=|1547027589|084d1d54bdb9e1bd12900c38'
                                      '0eb2a065979e9735"; tst=r; tgw_l7_route=66cb16bc7f45da64562a077714739c11; capsio'
                                      'n_ticket="2|1:0|10:1547090056|14:capsion_ticket|44:MzI1MzlkMmJlNjE0NDgxMzhhY2I'
                                      'yMTkwOTUyNDU1MmM=|d51c5f0343644f056fbc4afd35045708762da95761c150e2cf00234a529a7'
                                      'e85"; z_c0="2|1:0|10:1547090584|4:z_c0|92:Mi4xVlJUbERRQUFBQUFBOEdJUjhHeVFEaVlB'
                                      'QUFCZ0FsVk5tQWdrWFFBOC1NWUFSSTN4YWtfWGlaSE5nazVUM1FoM3N3|ece3ff8e86485acac5b96'
                                      '755d22147af376b0cb2e9406faf4c67e7bd427375fb"',
                            'Host': 'www.zhihu.com',
                            'Referer': 'https://www.zhihu.com/signup?next=%2Ftopic%2F19609455%2Ftop-writer',
                            'Upgrade-Insecure-Requests': '1',
                            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36'}

# -------------------  Government Data { CSRC / yuncai / Pbc } ----------------------------------

DEFAULT_CHROME_DOWNLOAD_PATH = '/var/codes/intellifeed-kol/tmp/'

LOCAL_DEFAULT_CHROME_DOWNLOAD_PATH = '/var/codes/intellifeed-kol/tmp/'