pycodestyle==2.4.0
pylint==1.9.1
pylint-common==0.2.5
pylint-plugin-utils==0.2.6

requests==2.18.4
boto==2.49.0
boto3==1.7.52

pdfminer.six==20181108

beautifulsoup4==4.4.0
lxml==4.3.0

pyselenium-framework

html5lib==1.0b8
newspaper3k==0.2.8
retrying==1.3.3

pdftotext==2.0.0

readability-lxml==0.7

translate==3.5.0
