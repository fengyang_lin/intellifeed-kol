import json
import datetime
import urllib.request
import requests
from bs4 import BeautifulSoup as BS
from newspaper import Article
TABLE_NAME = ''
COLUM_LIST = []


class ScrapyData:
    def __init__(self):

        self.result_json = []
        self.headers = {'Accept': '*/*',
                        'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7,zh-CN;q=0.6',
                        'Connection': 'keep-alive',
                        'Host': 'zhuanlan.zhihu.com',
                        'Referer': 'https://www.zhihu.com/people/zhang-xiao-cheng/posts',
                        'User-Agent': 'Mozilla / 5.0(Windows NT 10.0; Win64; x64) AppleWebKit / \
                        537.36(KHTML, like Gecko) Chrome / 71.0.3578.98 Safari / 537.36',
                        'Cookie': '_xsrf=XK4RCBYCjMq9V3MpaAURaitAtFafllpt; _zap=8686fb9a-b03e-42ad-ac1c-95b4cc096b5f; \
                        d_c0="APAieRLruw6PTvD01M2iUANv8YXHHTAZ_64=|1545909148"; tst=r; \
                        q_c1=c1fe00fe1b584559b56587493bf45c93|1545909187000|1545909187000; \
                        __utma=243313742.483984172.1546829336.1546829336.1546829336.1; __utmc=243313742;\
                         __utmz=243313742.1546829336.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); \
                         l_n_c=1; n_c=1; __utma=155987696.214306048.1546837735.1546837735.1546837735.1; \
                         __utmc=155987696; __utmz=155987696.1546837735.1.1.utmcsr=(direct)|utmccn=(direct)\
                         |utmcmd=(none); tgw_l7_route=73af20938a97f63d9b695ad561c4c10c; \
                         capsion_ticket="2|1:0|10:1546848608|14:capsion_ticket|\
                         44:MDljMjIyNDNmMjJiNDVhZTk5ODE5OWQ3NmY2MzQ3NDQ=|\
                         c599b573f40280ef14174b380e3a1890d80aca20cf87dc796d159c93273b1b3e"; \
                         l_cap_id="OGM4Y2NkODA3ODhiNGViNzg3OWI5NjU5MDM5Y2ExZTg=|1546848620|\
                         e620a68a36ec2429e253c8bf46fcc31f587d2e29"; r_cap_id="YzlhMGJkYTllOD\
                         gyNDUyNTlhODViMzcyZjA4MGJiOWU=|1546848620|347e35a1e5debbd28f84f97188b8aafaddb25e7f"; \
                         cap_id="Mzc0YTMxOWVlOTA0NGQ0MjhjMDJmMmYxNTUyYTA0M2E=|1546848620|\
                         4783a89de9eb1227e689946dde553ed07fb6f7bf"',
                        'x-requested-with': 'fetch',
                        'X-Ab-Param': 'se_click2=0;top_brand=1;top_login_card=1;top_nad=1;\
                        top_root_web=0;qa_answerlist_ad=0;top_recall_query=1;tp_header_style=0;\
                        tp_sticky_android=0;se_new_market_search=off;top_question_ask=1;top_video_score=1;\
                        li_gbdt=0;pin_efs=orig;top_root_few_topic=0;top_vd_gender=0;se_time_search=origin;\
                        tp_discussion_feed_card_type=0;se_filter=0;top_mt=0;ls_is_use_zrec=0;se_backsearch=0;\
                        top_root_ac=1;tp_discussion_feed_type_android=0;top_newfollow=0;pf_creator_card=1;\
                        se_prf=0;se_spb309=0;gw_guide=0;tp_qa_metacard_top=0;top_billpic=0;top_recall_tb=1;\
                        top_root=0;top_fqai=0;top_round_table=0;tp_sft=a;top_bill=0;top_distinction=0;top_cc_at=1;\
                        top_recall_deep_user=1;top_v_album=1;top_30=0;se_nweb_refact=0;top_ab_validate=0;\
                        tp_write_pin_guide=3;zr_ans_rec=gbrank;top_raf=y;top_recall_tb_long=51;tp_answer_meta_guide=0;\
                        se_billboardsearch=0;se_wiki_box=1;top_recall=0;top_billupdate1=2;top_recall_core_interest=81;\
                        ls_new_video=0;se_bert=0;ls_new_score=1;top_sj=2;top_tr=0;top_thank=1;top_root_mg=1;\
                        top_feedre_cpt=101;se_daxuechuisou=new;top_feedre_itemcf=31;se_engine=0;top_billab=0;\
                        top_feedre=1;top_new_feed=1;top_recall_exp_v2=1;top_core_session=-1;se_consulting_switch=off;\
                        top_gif=0;top_limit_num=0;top_rerank_video=-1;top_universalebook=1;top_wonderful=1;\
                        zr_video_rec=zr_video_rec:base;se_ad_index=10;soc_brandquestion=1;top_card=-1;top_quality=0;\
                        top_f_r_nb=1;top_reason=1;se_correct_ab=0;se_major_onebox=major;top_new_user_gift=0;top_yc=0;\
                        tp_related_topics= a;se_mfq=0;top_ebook=0;top_no_weighing=1;zr_article_rec_rank=base;\
                        zr_art_rec_rank=base;top_promo=1;top_rank=0;tp_qa_metacard=0;soc_zero_follow=0;\
                        se_auto_syn=0;top_ydyq=X;top_newfollowans=0;se_consulting_price=n;top_recall_follow_user=91;\
                        top_topic_feedre=21;top_user_gift=0;qa_test=0;top_billvideo=0;top_new_user_rec=0;se_colos=0;\
                        top_follow_reason=0;top_recall_exp_v1=1;top_scaled_score=0;tp_related_tps_movie=a;\
                        se_majorob_style=0;zr_art_rec=base;se_likebutton=0;top_recall_tb_follow=71;pin_ef=orig;\
                        se_gemini_service=content;top_accm_ab=1;se_entity=on;top_ntr=1;top_followtop=1;top_nucc=0;\
                        tp_dis_version=0;tp_m_intro_re_topic=0;se_search_feed=N;ls_topic_is_use_zrec=0;top_is_gr=0;\
                        ug_zero_follow=0;top_rerank_reformat=-1;top_test_4_liguangyi=1;tp_favsku=a;se_minor_onebox=d;\
                        top_recall_tb_short=61;top_yhgc=0;se_websearch=0;top_newuser_feed=0;top_feedre_rtt=41',
                        'X-UDID': 'APAieRLruw6PTvD01M2iUANv8YXHHTAZ_64='}

    @staticmethod
    def get_html(url):
        headers = {'Accept': 'text/html, application/xhtml+xml, image/jxr, */*',
                   'Accept - Encoding': 'gzip, deflate',
                   'Accept-Language': 'zh-Hans-CN, zh-Hans; q=0.5',
                   'Connection': 'Keep-Alive',
                   'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chr\
                   ome/52.0.2743.116 Safari/537.36 Edge/15.15063'}

        r = requests.get(url, headers=headers)
        r.encoding = r.apparent_encoding
        html = BS(r.text, 'lxml')
        arti = html.find('article').find('div', class_='RichText ztext Post-RichText')
        return arti

    @staticmethod
    def get_text(url):
        news = Article(url, language='zh')
        news.download()
        news.parse()
        return news.text

    def reponse_data(self):
        is_end = False
        offset = 0
        while not is_end:
            url = 'https://zhuanlan.zhihu.com/api2/columns/trading/articles?include=data%5B%2A%5D.admin_closed_commen' \
                  't%2Ccomment_count%2Csuggest_edit%2Cis_title_image_full_screen%2Ccan_comment%2Cupvoted_followees' \
                  '%2Ccan_open_tipjar%2Ccan_tip%2Cvoteup_count%2Cvoting%2Ctopics%2Creview_info%2Cauthor.is_following' \
                  '%2Cis_labeled%2Clabel_info&offset=' + str(offset)

            data = None
            req = urllib.request.Request(url, data, self.headers)
            response = urllib.request.urlopen(req)
            str_response = response.read().decode('utf8')
            json_obj = json.loads(str_response)
            for artis in json_obj['data']:
                dict_item = dict()
                dict_item['title'] = artis['title']
                dict_item['url'] = artis['url']
                dict_item['html_content'] = self.get_html(dict_item['url'])
                dict_item['pure text'] = self.get_text(dict_item['url'])
                if artis['created']:
                    dict_item['created_at'] = datetime.datetime.utcfromtimestamp(artis['created'])
                if artis['updated']:
                    dict_item['updated_at'] = datetime.datetime.utcfromtimestamp(artis['updated'])
                dict_item['author'] = artis['author']['name']
                self.result_json.append(dict_item)
                is_end = json_obj['paging']['is_end']

            offset += 10
        return self.result_json
