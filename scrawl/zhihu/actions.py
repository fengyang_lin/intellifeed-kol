import sys
import os

this_file_path = os.path.abspath('.')
abs_path = os.path.join(this_file_path, '../', '../')
sys.path.append(abs_path)
import time
import requests
from bs4 import BeautifulSoup as BS
from selenium_framework.utils import BaseAction
from scrawl.zhihu.pages import LoginPage
import settings


class Login(BaseAction):

    @staticmethod
    def login(driver):
        loginpage_obj = LoginPage()
        loginpage_obj.btn_switch_login_register.click(driver)
        account = settings.ZHIHU_ACCOUNTS[0]
        loginpage_obj.txt_username.set_txt(driver, account['username'])
        loginpage_obj.txt_password.set_txt(driver, account['password'])
        loginpage_obj.btn_login.click(driver)
        if loginpage_obj.field_capcha.if_exist(driver):
            print('Please Deal with Capcha Manually~')
            time.sleep(settings.T_LONG)
            loginpage_obj.btn_login.click(driver)


def generate_top_writer_urls():
    for url in settings.ZHIHU_URLS:
        r = requests.get(url, headers=settings.ZHIHU_HEADERS)
        r.encoding = r.apparent_encoding
        html = BS(r.text, 'lxml')
        html_list = html.find_all('a', class_='zg-link author-link')
        for item in html_list:
            full_link = 'http://www.zhihu.com' + item.attrs['href'] + '/activities'
            # writer_name = item.string
            settings.ZHIHU_G_TOP_WRITTER_URLS.append(full_link)

# Start Browser
# import time
# from selenium_framework.utils import start_browser, close_browser
# print(settings.DRIVER_PATH_CHROME)
# driver = start_browser(URL=settings.ZHIHU_LOGIN_URL,
#                        brower_driver_location=settings.DRIVER_PATH_CHROME)
# login_action_obj = Login()
# login_action_obj.login(driver)
# time.sleep(settings.T_MINI)
