# -*- coding: utf-8 -*-
import json
from scrawl.zhihu import userPro_local
from scrawl.zhihu import getIntroPhoto_local
from scrawl.zhihu import actions_local
from scrawl.zhihu import ColArticles_local
from scrawl.zhihu import uploadAvatar_local
import settings

# get kol urls
actions_local.generate_top_writer_urls()

urls = settings.ZHIHU_G_TOP_WRITTER_URLS
test = urls[:]

# upload avatar

# print(uploadAvatar_local.get_avatar(test))

# upload kol profile and save kol ids in settings.ZHIHU_KOL_IDs
profile = getIntroPhoto_local.get_intro(test)

# save kol urls and ids in file
# with open('kolID_update.json', 'w') as f:
#     json.dump(settings.ZHIHU_KOL_IDs, f)
#     f.close()
with open('kolID_update.json', 'r') as f:
    settings.ZHIHU_KOL_IDs = json.load(f)
    f.close()

# upload articles and answers
userPro_local.run(test)

# 专栏文章
colarticles_instance = ColArticles_local.ScrapyData()
# ret = colarticles_instance.reponse_data()
# print(ret[0],ret[-1])
