# -*- coding: utf-8 -*-
import urllib.request
import time
import json
import requests
from bs4 import BeautifulSoup as BS
from newspaper import Article
import settings
from common import common
from common import api
import datetime

TABLE_NAME = ''
COLUM_LIST = []


class scrapy_data:
    def __init__(self):
        # self.artiheaders = {'Accept': '*/*',
        #                     'Accept - Encoding': 'gzip, deflate,br',
        #                     'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7,zh-CN;q=0.6',
        #                     'Connection': 'keep-alive',
        #                     'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 \
        #                     (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
        #                     'Cookie': '_xsrf=XK4RCBYCjMq9V3MpaAURaitAtFafllpt; _zap=8686fb9a-b03e-42ad-ac1c-95b4cc096b
        #                     5f; d_c0="APAieRLruw6PTvD01M2iUANv8YXHHTAZ_64=|1545909148"; tst=r; q_c1=c1fe00fe1b584559b5
        #                     6587493bf45c93|1545909187000|1545909187000; l_n_c=1; n_c=1; __utmc=51854390; __utmv=518543
        #                     90.100--|2=registration_date=20170916=1^3=entry_date=20170916=1; l_cap_id="NjM4NGQ4NjdlZDN
        #                     jNDJkZTllZGMxMGYzYWVhZGQ3NGY=|1547013336|6b37fcef10c498f7f03497df55c9445d5b890b9b"; r_cap_
        #                     id="YjAxM2U2OGUyZDUwNGI0YWIwMDIyMzFmOWVlYTNiNzM=|1547013336|ed2ba471d758c0c97fed5c00d25ea5
        #                     578c162cdb"; cap_id="N2IyODQ1MzhiMzQyNDQ1YmJlOWVjZmNlYWUyNTIyMWQ=|1547013336|f758df04b2972
        #                     34928e4c1935f5b986b693aef1a"; capsion_ticket="2|1:0|10:1547013348|14:capsion_ticket|44:ZTd
        #                     kZTE2NDI0YjcwNGVjOGJhNmE0ZjM5ODVlMzJmNTE=|9cb32c35ffb66c7ae7c81482dd23304523bcdb35ef5e432e
        #                     961605809d21788f"; z_c0="2|1:0|10:1547013353|4:z_c0|92:Mi4xN0hUMEJRQUFBQUFBOENKNUV1dTdEaVl
        #                     BQUFCZ0FsVk42ZG9pWFFBd3pJS01YYXU4MklHMEhTMnl5YUNkWVJZdVZR|ba0078de9cb6587b85826719978aecca
        #                     024ab97b1e4430be49bdab9dfcd8d032"; __utmz=51854390.1547013356.3.3.utmcsr=zhihu.com|utmccn=
        #                     (referral)|utmcmd=referral|utmcct=/signup; __utma=51854390.6997768.1546917143.1547015628.1
        #                     547108028.5; tgw_l7_route=f2979fdd289e2265b2f12e4f4a478330',
        #                     'Host': 'www.zhihu.com',
        #                     'Referer': 'https://www.zhihu.com/people/zhang-xiao-cheng/posts',
        #                     'X-Ab-Param': 'top_core_session=-1;top_recall_core_interest=81;tp_header_style=0;tp_qa_met
        #                     acard_top=0;top_f_r_nb=1;top_recall_tb_follow=71;top_vd_gender=0;pf_creator_card=1;top_fee
        #                     dre_itemcf=31;zr_art_rec_rank=base;top_login_card=1;top_tr=0;top_root=1;top_distinction=0;
        #                     top_fqai=0;top_is_gr=0;top_limit_num=0;tp_write_pin_guide=3;top_raf=n;qa_answerlist_ad=0;t
        #                     op_feedre_cpt=101;tp_answer_meta_guide=0;top_no_weighing=1;top_root_ac=1;se_new_market_sea
        #                     rch=on;top_followtop=1;top_root_mg=1;se_bert=0;se_billboardsearch=0;top_newuser_feed=1;ug_
        #                     zero_follow=0;top_recall_follow_user=91;top_mt=0;top_quality=0;top_topic_feedre=21;tp_disc
        #                     ussion_feed_type_android=2;zr_ans_rec=gbrank;tp_discussion_feed_card_type=2;top_ab_validat
        #                     e=2;top_recall_tb=4;se_correct_ab=0;top_test_4_liguangyi=1;top_nucc=0;top_universalebook=1
        #                     ;se_entity=on;top_brand=1;top_wonderful=1;zr_art_rec=new;qa_test=0;soc_zero_follow=0;top_q
        #                     uestion_ask=1;pf_newguide_vertical=0;soc_brandquestion=1;tp_m_intro_re_topic=0;ls_new_vide
        #                     o=1;se_websearch=3;top_recall=2;top_thank=1;top_yhgc=0;se_filter=1;top_30=0;top_feedre=1;t
        #                     op_sj=2;li_gbdt=default;pin_efs=orig;top_round_table=0;se_click2=1;top_nad=1;top_recall_ex
        #                     p_v1=1;tp_sft=a;ls_topic_is_use_zrec=1;top_card=-1;top_ebook=0;se_gemini_service=content;s
        #                     e_likebutton=0;top_source=0;top_billvideo=0;se_mfq=0;top_billpic=0;top_bill=0;tp_dis_versi
        #                     on=0;ls_is_use_zrec=1;se_spb309=0;top_hotlist=1;top_newfollow=0;top_recall_tb_long=51;top_
        #                     root_web=0;se_search_feed=N;se_time_search=origin;top_tagextend=1;se_ad_index=10;se_backse
        #                     arch=0;se_consulting_switch=off;se_km_ad_locate=1;se_wiki_box=1;top_new_feed=1;top_rank=9;
        #                     tp_related_topics= a;ls_new_score=1;top_v_album=1;pin_ef=orig;top_recall_exp_v2=9;top_yc=0
        #                     ;se_daxuechuisou=new;top_feedre_rtt=41;top_reason=1;top_user_gift=0;top_video_rerank=2;se_
        #                     webmajorob=0;se_webrs=0;top_billab=0;top_recall_deep_user=1;se_auto_syn=0;se_colos=0;tp_st
        #                     icky_android=1;se_minor_onebox=d;se_expired_ob=0;zr_article_rec_rank=base;se_prf=0;zr_vide
        #                     o_rec=zr_video_rec:base;top_native_answer=1;top_ntr=1;se_consulting_price=n;top_new_user_g
        #                     ift=0;top_video_score=1;top_recall_tb_short=61;top_rerank_video=-1;se_engine=1;top_gif=0;t
        #                     op_promo=1;se_webtimebox=0;top_billupdate1=3;top_new_user_rec=0;top_rerank_reformat=-1;top
        #                     _ydyq=X;top_accm_ab=1;top_cc_at=1;top_follow_reason=0;top_recall_query=1;top_scaled_score=
        #                     0;tp_favsku=a;qa_video_answer_list=0;se_major_onebox=major;top_newfollowans=0;tp_related_t
        #                     ps_movie=a;top_root_few_topic=0;gw_guide=0;se_majorob_style=0;tp_qa_metacard=1',
        #                     'x-requested-with': 'fetch',
        #                     'X-UDID': 'APAieRLruw6PTvD01M2iUANv8YXHHTAZ_64=',
        #                     'X-Zse-83': '3_1.1',
        #                     'X-Zse-84': 'n98kPC_kNcbaRorlgg9k0KKlSoNuQork-1NkSW5lNkbvOcrxik8w9d_mMwuvAxbk'}
        self.artiheaders = {'User-Agent': 'Mozilla/5.0'}
        # self.ansheaders = {'Accept': '*/*',
        #                    'Accept - Encoding': 'gzip, deflate,br',
        #                    'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7,zh-CN;q=0.6',
        #                    'Connection': 'keep-alive',
        #                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Ge
        #                    cko) Chrome/71.0.3578.98 Safari/537.36',
        #                    'Cookie': '_xsrf=XK4RCBYCjMq9V3MpaAURaitAtFafllpt; _zap=8686fb9a-b03e-42ad-ac1c-95b4cc096b5
        #                    f; d_c0="APAieRLruw6PTvD01M2iUANv8YXHHTAZ_64=|1545909148"; tst=r; q_c1=c1fe00fe1b584559b565
        #                    87493bf45c93|1545909187000|1545909187000; l_n_c=1; n_c=1; l_cap_id="NjM4NGQ4NjdlZDNjNDJkZTl
        #                    lZGMxMGYzYWVhZGQ3NGY=|1547013336|6b37fcef10c498f7f03497df55c9445d5b890b9b"; r_cap_id="YjAxM
        #                    2U2OGUyZDUwNGI0YWIwMDIyMzFmOWVlYTNiNzM=|1547013336|ed2ba471d758c0c97fed5c00d25ea5578c162cdb
        #                    "; cap_id="N2IyODQ1MzhiMzQyNDQ1YmJlOWVjZmNlYWUyNTIyMWQ=|1547013336|f758df04b297234928e4c193
        #                    5f5b986b693aef1a"; capsion_ticket="2|1:0|10:1547013348|14:capsion_ticket|44:ZTdkZTE2NDI0Yjc
        #                    wNGVjOGJhNmE0ZjM5ODVlMzJmNTE=|9cb32c35ffb66c7ae7c81482dd23304523bcdb35ef5e432e961605809d217
        #                    88f"; z_c0="2|1:0|10:1547013353|4:z_c0|92:Mi4xN0hUMEJRQUFBQUFBOENKNUV1dTdEaVlBQUFCZ0FsVk42Z
        #                    G9pWFFBd3pJS01YYXU4MklHMEhTMnl5YUNkWVJZdVZR|ba0078de9cb6587b85826719978aecca024ab97b1e4430b
        #                    e49bdab9dfcd8d032"; __gads=ID=31ee3290d01c8b78:T=1547113495:S=ALNI_MZDmWffzE2KxzOJNWliVSocD
        #                    vHlAA; __utma=155987696.2000643740.1547113590.1547113590.1547113590.1; __utmb=155987696.0.1
        #                    0.1547113590; __utmc=155987696; __utmz=155987696.1547113590.1.1.utmcsr=(direct)|utmccn=(dir
        #                    ect)|utmcmd=(none); tgw_l7_route=80f350dcd7c650b07bd7b485fcab5bf7',
        #                    'Host': 'www.zhihu.com',
        #                    'Referer': 'https://www.zhihu.com/people/zhang-xiao-cheng/answers',
        #                    'X-Ab-Param': 'top_ydyq=X;tp_related_topics= a;top_rank=9;top_follow_reason=0;se_km_ad_loca
        #                    te=1;top_yc=0;ug_zero_follow=0;tp_sft=a;top_cc_at=1;tp_dis_version=0;tp_m_intro_re_topic=0;
        #                    top_new_feed=1;top_recall_exp_v2=9;se_wiki_box=1;top_core_session=-1;pf_creator_card=1;top_
        #                    quality=0;ls_topic_is_use_zrec=1;se_click2=1;top_is_gr=0;top_rerank_reformat=-1;se_websearc
        #                    h=3;top_card=-1;top_ntr=1;top_recall=2;se_major_onebox=major;top_video_rerank=2;se_likebutt
        #                    on=0;top_brand=1;top_mt=0;zr_article_rec_rank=base;se_backsearch=0;top_newuser_feed=1;tp_qa
        #                    _metacard=1;se_ad_index=10;se_bert=0;top_yhgc=0;top_feedre_cpt=101;top_recall_tb=4;top_hotl
        #                    ist=1;top_recall_tb_long=51;zr_ans_rec=gbrank;pin_ef=orig;top_f_r_nb=1;top_source=0;se_webt
        #                    imebox=0;top_newfollowans=0;qa_video_answer_list=0;se_consulting_price=n;se_daxuechuisou=ne
        #                    w;se_prf=0;top_limit_num=0;top_wonderful=1;ls_new_video=1;se_spb309=0;top_scaled_score=0;se
        #                    _gemini_service=content;top_recall_tb_follow=71;qa_answerlist_ad=0;se_time_search=origin;to
        #                    p_vd_gender=0;se_entity=on;se_billboardsearch=0;top_billvideo=0;top_native_answer=1;top_fol
        #                    lowtop=1;top_recall_tb_short=61;top_round_table=0;top_v_album=1;tp_discussion_feed_card_typ
        #                    e=2;se_minor_onebox=d;se_consulting_switch=off;top_billab=0;top_nucc=0;tp_qa_metacard_top=0
        #                    ;top_billupdate1=3;tp_header_style=0;tp_write_pin_guide=3;top_new_user_gift=0;pin_efs=orig;
        #                    se_search_feed=N;top_feedre_itemcf=31;tp_answer_meta_guide=0;se_expired_ob=0;zr_art_rec_ran
        #                    k=base;top_promo=1;top_thank=1;se_webrs=0;top_login_card=1;top_reason=1;top_tagextend=1;se_
        #                    auto_syn=0;top_ebook=0;top_ab_validate=2;top_fqai=0;top_topic_feedre=21;se_correct_ab=0;se_
        #                    mfq=0;top_billpic=0;top_nad=1;top_raf=n;ls_new_score=1;qa_test=0;se_filter=1;zr_art_rec=new
        #                    ;top_recall_query=1;top_user_gift=0;top_video_score=1;top_bill=0;top_new_user_rec=0;zr_vide
        #                    o_rec=zr_video_rec:base;gw_guide=0;soc_brandquestion=1;top_recall_core_interest=81;top_accm
        #                    _ab=1;top_gif=0;top_sj=2;se_majorob_style=0;tp_discussion_feed_type_android=2;top_30=0;tp_f
        #                    avsku=a;tp_sticky_android=1;top_feedre_rtt=41;tp_related_tps_movie=a;soc_zero_follow=0;top_
        #                    recall_follow_user=91;top_root_few_topic=0;top_universalebook=1;top_rerank_video=-1;pf_newg
        #                    uide_vertical=0;li_gbdt=default;se_engine=1;top_distinction=0;top_recall_deep_user=1;top_ro
        #                    ot_web=0;top_tr=0;top_root_ac=1;top_root_mg=1;ls_is_use_zrec=1;top_feedre=1;top_root=1;se_c
        #                    olos=0;se_webmajorob=0;top_test_4_liguangyi=1;se_new_market_search=on;top_newfollow=0;top_r
        #                    ecall_exp_v1=1;top_no_weighing=1;top_question_ask=1',
        #                    'x-requested-with': 'fetch',
        #                    'X-UDID': 'APAieRLruw6PTvD01M2iUANv8YXHHTAZ_64=',
        #                    'X-Zse-83': '3_1.1',
        #                    'X-Zse-84': 'h1NlTKomNcbaRorlgg9k05dl82Nb7pbxnorl9Pol02rv8lAxhgbl70KkL18aTg8k'}
        self.ansheaders = {'User-Agent': 'Mozilla/5.0'}

    @staticmethod
    def get_html(url):
        headers = {'Accept': 'text/html, application/xhtml+xml, image/jxr, */*',
                   'Accept - Encoding': 'gzip, deflate',
                   'Accept-Language': 'zh-Hans-CN, zh-Hans; q=0.5',
                   'Connection': 'Keep-Alive',
                   'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) \
                   Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063'}

        r = requests.get(url, headers=headers)
        r.encoding = r.apparent_encoding
        html = BS(r.text, 'lxml')
        arti = html.find('article').find('div', class_='RichText ztext Post-RichText')

        return arti

    @staticmethod
    def get_text(url):
        news = Article(url, language='zh')
        news.download()
        news.parse()
        return news.text

    def answer(self, oriurl, result):
        is_end = False
        offset = 0
        print(oriurl)
        while not is_end:

            url = oriurl + str(offset)
            # print(url)
            data = None
            time.sleep(3)
            req = urllib.request.Request(url, data, self.ansheaders)
            response = urllib.request.urlopen(req)
            str_response = response.read().decode('utf8')
            json_obj = json.loads(str_response)
            # print(json_obj)
            for artis in json_obj['data']:
                likes = artis['voteup_count']
                comment_count = artis['comment_count']
                dict_item = dict()
                dict_item['source_website_name'] = 'Zhihu'
                dict_item['source_domain_url'] = 'https://zhihu.com/'
                dict_item['title_cn'] = artis['question']['title']
                dict_item['source_full_destination_url'] = \
                    'https://www.zhihu.com/question/' + str(artis['question']['id']) + '/answer/' + str(artis['id'])
                content = artis['content']
                content_bs = BS(content, 'lxml')
                get_img = content_bs.find_all("img", class_="origin_image zh-lightbox-thumb")
                img_urls = []
                for item in get_img:
                    url = item['src']
                    img_urls.append(url)

                for url in img_urls:
                    img_name = 'zhihu_' + url.split('/')[-1]
                    img_path = common.get_article_img_and_upload_to_s3(url, img_name)

                    content = content.replace(url, img_path)

                dict_item['content_cn'] = str(content)
                dict_item['publish_time'] = time.strftime("%Y-%m-%d %H:%M", time.localtime(artis['created_time']))
                now_time = (datetime.datetime.now() - datetime.timedelta(days=3)).strftime('%Y-%m-%d %H:%M')
                if now_time > dict_item['publish_time']:
                    return
                dict_item['types'] = 'social_media'
                if oriurl.split('/')[6] in settings.ZHIHU_KOL_IDs:
                    dict_item['kol_id'] = settings.ZHIHU_KOL_IDs[oriurl.split('/')[6]]
                else:
                    return 0
                api_instance = api.IntellifeedAPICaller()
                new_followers = {}
                json_data = {"user_id": dict_item['kol_id']}
                query_response = api_instance.query_kol_profile(json_data)
                try:
                    print('query status_code', query_response.status_code)
                    print('query json', query_response.json())
                except:
                    continue
                old_followers = query_response.json()['followers']
                if 'likes' in old_followers['zhihu']:
                    old_followers['zhihu']['likes'] = str(int(old_followers['zhihu']['likes']) + int(likes))
                else:
                    old_followers['zhihu']['likes'] = '0'
                new_followers['user_id'] = dict_item['kol_id']
                new_followers['followers'] = old_followers
                update_response = api_instance.update_kol_profile(new_followers)
                try:
                    print('update status_code', update_response.status_code)
                    print('update json--', update_response.json())
                except:
                    continue
                content_info_dict = {'content_info': dict_item}
                result.append(content_info_dict)
                # dict_item['pure text'] = self.get_text(dict_item['url'])
                # dict_item['asked_at'] = datetime.datetime.utcfromtimestamp(artis['question']['created'])
                # dict_item['answered_at'] = datetime.datetime.utcfromtimestamp(artis['created_time'])

                # print(dict_item['answered_at'])
                # dict_item['author'] = artis['author']['name']
                print(content_info_dict)
                ret = api_instance.new_content(content_info_dict)
                print(str(ret.status_code))
                print(ret.text)
                print('offset', offset)
            is_end = json_obj['paging']['is_end']
            offset += 10

    def article(self, oriurl, result):
        is_end = False
        offset = 0
        print(oriurl)
        while not is_end:
            url = oriurl + str(offset)

            data = None
            time.sleep(3)
            req = urllib.request.Request(url, data, self.artiheaders)
            response = urllib.request.urlopen(req)
            str_response = response.read().decode('utf8')

            json_obj = json.loads(str_response)

            for artis in json_obj['data']:
                likes = artis['voteup_count']
                comment_count = artis['comment_count']
                dict_item = dict()
                dict_item['source_website_name'] = 'Zhihu'
                dict_item['source_domain_url'] = 'https://zhihu.com/'
                dict_item['title_cn'] = artis['title']
                dict_item['source_full_destination_url'] = artis['url']

                content = self.get_html(dict_item['source_full_destination_url'])
                str_content = str(content)
                try:
                    get_img = content.find_all("img", class_="origin_image zh-lightbox-thumb")
                    # print(get_img)
                    img_urls = []
                    for item in get_img:
                        url = item['src']
                        img_urls.append(url)
                    # print(img_urls)

                    for url in img_urls:
                        img_name = 'zhihu_' + url.split('/')[-1]
                        img_path = common.get_article_img_and_upload_to_s3(url, img_name)
                        print(img_path)
                        str_content = str_content.replace(url, img_path)

                    print(str_content)
                except Exception as err:  # pylint: disable=unused-variable
                    continue

                dict_item['content_cn'] = str_content
                dict_item['publish_time'] = time.strftime("%Y-%m-%d %H:%M", time.localtime(artis['created']))
                print(dict_item['publish_time'])
                now_time = (datetime.datetime.now() - datetime.timedelta(days=3)).strftime('%Y-%m-%d %H:%M')
                if now_time > dict_item['publish_time']:
                    return
                dict_item['types'] = 'social_media'

                if oriurl.split('/')[6] in settings.ZHIHU_KOL_IDs:
                    dict_item['kol_id'] = settings.ZHIHU_KOL_IDs[oriurl.split('/')[6]]
                else:
                    return 0
                api_instance = api.IntellifeedAPICaller()
                new_followers = {}
                json_data = {"user_id": dict_item['kol_id']}
                query_response = api_instance.query_kol_profile(json_data)
                try:
                    print('query status_code', query_response.status_code)
                    print('query json', query_response.json())
                except:
                    continue
                old_followers = query_response.json()['followers']
                if 'likes' in old_followers['zhihu']:
                    old_followers['zhihu']['likes'] = str(int(old_followers['zhihu']['likes']) + int(likes))
                else:
                    old_followers['zhihu']['likes'] = '0'
                new_followers['user_id'] = dict_item['kol_id']
                new_followers['followers'] = old_followers
                update_response = api_instance.update_kol_profile(new_followers)
                try:
                    print('update status_code', update_response.status_code)
                    print('update json--', update_response.json())
                except:
                    continue
                # dict_item['kol_id'] = 'b7502bbe-6e2c-4839-b9b4-fe1cdad55f21' 伦敦交易员的kol id
                content_info_dict = {'content_info': dict_item}
                # dict_item['author'] = artis['author']['name']
                print(content_info_dict)
                ret = api_instance.new_content(content_info_dict)
                print(str(ret.status_code))
                # print(ret.text)
                result.append(content_info_dict)
                print(offset)
            is_end = json_obj['paging']['is_end']
            offset += 10
        return


def run(urls):
    print(urls)
    for url in urls:
        print('********new KOL**********')
        name = url.split('/')[4]
        print(url)
        print('*************************')
        scrapy_data_v1 = scrapy_data()
        arti = 'https://www.zhihu.com/api/v4/members/' + name + '/articles?include=data%5B*%5D.comment_count%2Csugg' \
                                                                'est_edit%2Cis_normal%2Cthumbnail_extra_info%2Cthum' \
                                                                'bnail%2Ccan_comment%2Ccomment_permission%2Cadmin_c' \
                                                                'losed_comment%2Ccontent%2Cvoteup_count%2Ccreated%2' \
                                                                'Cupdated%2Cupvoted_followees%2Cvoting%2Creview_inf' \
                                                                'o%2Cis_labeled%2Clabel_info%3Bdata%5B*%5D.author.b' \
                                                                'adge%5B%3F(type%3Dbest_answerer)%5D.topics&limit=1' \
                                                                '0&sort_by=created&offset='
        ans = 'https://www.zhihu.com/api/v4/members/' + name + '/answers?include=data%5B*%5D.is_normal%2Cadmin_closed' \
                                                               '_comment%2Creward_info%2Cis_collapsed%2Cannotation_ac' \
                                                               'tion%2Cannotation_detail%2Ccollapse_reason%2Ccollapse' \
                                                               'd_by%2Csuggest_edit%2Ccomment_count%2Ccan_comment%2Cc' \
                                                               'ontent%2Cvoteup_count%2Creshipment_settings%2Ccomment' \
                                                               '_permission%2Cmark_infos%2Ccreated_time%2Cupdated_tim' \
                                                               'e%2Creview_info%2Cquestion%2Cexcerpt%2Cis_labeled%2Cl' \
                                                               'abel_info%2Crelationship.is_authorized%2Cvoting%2Cis_' \
                                                               'author%2Cis_thanked%2Cis_nothelp%3Bdata%5B*%5D.author' \
                                                               '.badge%5B%3F(type%3Dbest_answerer)%5D.topics&limit=10' \
                                                               '&sort_by=created&offset='
        result = []
        scrapy_data_v1.answer(ans, result)
        scrapy_data_v1.article(arti, result)
        # return result
