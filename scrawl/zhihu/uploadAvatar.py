import sys
import os

this_file_path = os.path.abspath('.')
abs_path = os.path.join(this_file_path, '../', '../')
sys.path.append(abs_path)
import time
from bs4 import BeautifulSoup as BS
from selenium import webdriver
import common.common
from scrawl.news.base import BaseScraper
import settings
import uuid


class ScrapeZhihu_avatar(BaseScraper):
    headers = {'Accept': '*/*',
               'Encoding': 'gzip, deflate, br',
               'Language': 'zh-CN,zh;q=0.9',
               'Connection': 'keep-alive',
               'Cookie': 'CXID=87C4B04070D96B96DA45D758CC96969F; SUID=8578E7653865860A5AC2C21F00038887; SUV=008D'
                         '437027B43D015B13F7BD398E5453; dt_ssuid=3281138552; pex=C864C03270DED3DD8A06887A372DA21'
                         '9231FFAC25A9D64AE09E82AED12E416AC; ssuid=1070970760; IPLOC=CN3100; ad=4IRkvlllll2txMHQ'
                         'lllllVZ4fCYlllllBWNcPkllll9lllllxv7ll5@@@@@@@@@@; ABTEST=0|1546483965|v1; weixinIndexV'
                         'isited=1; JSESSIONID=aaaEYHTxDr_7BbOV3gdDw',
               'Host': 'weixin.sogou.com',
               'Referer': 'https://weixin.sogou.com/',
               'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome'
                             '/71.0.3578.98 Safari/537.36',
               'X-Requested-With': 'XMLHttpRequest'}

    _tmp_folder = '/tmp/{}'.format(uuid.uuid4())
    if not os.path.exists(_tmp_folder):
        os.makedirs(_tmp_folder)

    if not os.path.exists(_tmp_folder + '/user-data'):
        os.makedirs(_tmp_folder + '/user-data')

    if not os.path.exists(_tmp_folder + '/data-path'):
        os.makedirs(_tmp_folder + '/data-path')

    if not os.path.exists(_tmp_folder + '/cache-dir'):
        os.makedirs(_tmp_folder + '/cache-dir')

    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-gpu')
    chrome_options.add_argument('--window-size=1280x1696')
    chrome_options.add_argument('--user-data-dir={}'.format(_tmp_folder + '/user-data'))
    chrome_options.add_argument('--hide-scrollbars')
    chrome_options.add_argument('--enable-logging')
    chrome_options.add_argument('--log-level=0')
    chrome_options.add_argument('--v=99')
    chrome_options.add_argument('--single-process')
    chrome_options.add_argument('--data-path={}'.format(_tmp_folder + '/data-path'))
    chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument('--homedir={}'.format(_tmp_folder))
    chrome_options.add_argument('--disk-cache-dir={}'.format(_tmp_folder + '/cache-dir'))
    chrome_options.add_argument(
        'user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
        'Chrome/61.0.3163.100 Safari/537.36')
    prefs = {"download.default_directory": "/var/codes/intellifeed-kol"}
    # chrome_options.add_experimental_option("prefs", prefs)
    # driver = webdriver.Chrome(chrome_options=chrome_options)
    chrome_options.binary_location = settings.DRIVER_PATH_CHROME
    browser = webdriver.Chrome(chrome_options=chrome_options)

    def __init__(self, domain_url, media, lang, website=None):
        super(ScrapeZhihu_avatar, self).__init__(domain_url, media, lang)

    def get_avatar(self, urls):
        s3_urls = []
        for url in urls:
            print(urls)
            # browser = webdriver.Chrome(r"../../driver_binary_files/chromedriver_mac")
            try:
                self.browser.get(url)
            except Exception as err:
                print('browser.get(url) error: ' + str(err))
                time.sleep(1200)
                self.browser.get(url)
            time.sleep(1)
            source = self.browser.page_source
            time.sleep(1)
            html = BS(source, 'lxml')
            img_url = html.find('img', class_='Avatar Avatar--large UserAvatar-inner').attrs['src']
            title = 'zhihu_' + url.split('/')[-2] + '.' + img_url.split('.')[-1]
            s3_url = common.common.get_avatar_and_upload_to_s3(img_url, title)
            s3_urls.append(s3_url)
            print(s3_url)
        self.browser.quit()
        return s3_urls
