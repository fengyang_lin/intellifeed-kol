from selenium_framework import web_element
from selenium_framework.web_element import identifier
from selenium_framework.utils import BasePage


class LoginPage(BasePage):

    btn_switch_login_register = web_element.WebButton(identifier.xpath,
                                                      '//*[@id="root"]/div/main/div/div/div/div[2]/div[2]/span',
                                                      'Switch Between Login and Register Button')
    btn_login = web_element.WebButton(identifier.xpath,
                                      '//*[@id="root"]/div/main/div/div/div/div[2]/div[1]/form/button',
                                      'Login Button')
    txt_username = web_element.WebEdit(
        identifier.xpath,
        '//*[@id="root"]/div/main/div/div/div/div[2]/div[1]/form/div[1]/div[2]/div[1]/input',
        'User Name')
    txt_password = web_element.WebEdit(
        identifier.xpath,
        '//*[@id="root"]/div/main/div/div/div/div[2]/div[1]/form/div[2]/div/div[1]/input',
        'Password')
    field_capcha = web_element.WebGeneral(identifier.xpath,
                                          '//*[@id="root"]/div/main/div/div/div/div[2]/div[1]/form/div[3]/div',
                                          'Capcha')
