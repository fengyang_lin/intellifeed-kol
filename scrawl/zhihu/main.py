# -*- coding: utf-8 -*-
import sys
import os

this_file_path = os.path.abspath('.')
abs_path = os.path.join(this_file_path, '../', '../')
sys.path.append(abs_path)
import json
from scrawl.zhihu import userPro
from scrawl.zhihu.getIntroPhoto import ScrapeZhihu_intro
from scrawl.zhihu import actions
from scrawl.zhihu import ColArticles
from scrawl.zhihu.uploadAvatar import ScrapeZhihu_avatar
import settings
import time

while True:
    # get kol urls
    actions.generate_top_writer_urls()

    urls = settings.ZHIHU_G_TOP_WRITTER_URLS
    test = urls[:]

    # upload avatar
    zhihu_avatar_obj = ScrapeZhihu_avatar('www.baidu.com', 'zhihu', 'cn')

    print(zhihu_avatar_obj.get_avatar(test))

    # upload kol profile and save kol ids in settings.ZHIHU_KOL_IDs
    zhihu_intro_obj = ScrapeZhihu_intro('www.baidu.com', 'zhihu', 'cn')

    profile = zhihu_intro_obj.get_intro(test)

    # save kol urls and ids in file
    with open('kolID_update.json', 'w') as f:
        json.dump(settings.ZHIHU_KOL_IDs, f)
        f.close()
    with open('kolID_update.json', 'r') as f:
        settings.ZHIHU_KOL_IDs = json.load(f)
        f.close()

    # upload articles and answers
    userPro.run(test)

    # 专栏文章
    colarticles_instance = ColArticles.ScrapyData()
    # ret = colarticles_instance.reponse_data()
    # print(ret[0],ret[-1])
    print('sleeping!!~~~~~~~~~~')
    time.sleep(86400)
