import time
import json
from selenium import webdriver
from bs4 import BeautifulSoup as BS
from common.api import IntellifeedAPICaller
import settings
import re
from urllib import request
from lxml import etree


def get_intro(urls):
    intros = []
    for url in urls:
        headers = {
            'User-Agent': 'User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
            'Cookie': '_zap=fca9acb7-2d68-4712-9912-f6b21782466b; d_c0="ADDkugaLAg-PTuttrqedvi2vojpWQKiIOvU=|1550648700"; __utmv=51854390.100--|2=registration_date=20190110=1^3=entry_date=20190110=1; _xsrf=7386c127b7de1500b589c15bc7e05fdd; __utmc=51854390; _xsrf=7386c127b7de1500b589c15bc7e05fdd; __gads=ID=4f981003a182519c:T=1553494507:S=ALNI_MY90lAtKD3XqwBzbfJ4aSLi5bJvig; q_c1=df6f5e7865b849318de435053eb09d91|1553494588000|1550651560000; tst=h; __utmz=51854390.1553654780.6.2.utmcsr=baidu|utmccn=(organic)|utmcmd=organic; __utma=51854390.990028381.1550651576.1553654780.1553828197.7; tgw_l7_route=060f637cd101836814f6c53316f73463; capsion_ticket="2|1:0|10:1553835696|14:capsion_ticket|44:N2QwY2NhYWNkYzE5NGMzYjk2MmYxNzBiNmJkMDE1MjI=|46cbf49df6d3371aeca8c54686eccba4ab89948864870a28f3974b1abf54b11c"; z_c0="2|1:0|10:1553835704|4:z_c0|92:Mi4xVlJUbERRQUFBQUFBTU9TNkJvc0NEeVlBQUFCZ0FsVk51UFNLWFFDSkhKZmFTU3JmVXhiUGwyY01WM3NsRXpMSklR|5893bc3580582dc7620ba8ac1e596c78c36b52e2cdc76b0632eceef27c0ecb58"'}
        req = request.Request(url, headers=headers)
        reponse = request.urlopen(req)
        page_source = reponse.read()
        selector = etree.HTML(page_source)
        fans_followers = selector.xpath('//strong[@class="NumberBoard-itemValue"]')
        fans = fans_followers[0].xpath('./@title')[0]
        follower = fans_followers[1].xpath('./@title')[0]
        likes_list = selector.xpath('//div[@class="IconGraf"]/text()')
        likes_str = "".join(likes_list).replace(',', '')
        likes = re.findall(r'([1-9]\d*) 次赞同', likes_str)[0]
        browser = webdriver.Chrome(r"../../driver_binary_files/chromedriver_mac")
        time.sleep(1)
        browser.get(url)
        try:
            browser.find_element_by_xpath('//*[@id="ProfileHeader"]/div/div[2]/div/div[2]/div[3]/button').click()
        except Exception as err:  # pylint: disable=unused-variable
            browser.find_element_by_xpath('//*[@id="ProfileHeader"]/div/div[2]/div/div[2]/div[3]/div[1]/button').click()
        # number = browser.find_elements_by_xpath('//strong[@class="NumberBoard-itemValue"]')
        # for i in number:
        #     print(i.get_attribute('title'))
        # follower = number[1].get_attribute('title')
        # fans = number[0].get_attribute('title')
        source = browser.page_source
        time.sleep(0.2)
        browser.quit()

        html = BS(source, 'lxml')
        intro = dict()
        intro['nick_name'] = html.find('span', class_='ProfileHeader-name').string
        intro['real_name'] = ''
        intro['email'] = ''
        # img_url = html.find('img', class_='Avatar Avatar--large UserAvatar-inner').attrs['src']
        intro['avatar'] = 'https://static.redpulse.com/intellifeeds/kol_profile/zhihu_' + url.split('/')[-2] + '.jpg'
        # intro['id'] = url.split('/')[-2]
        intro['original_url'] = url
        try:
            intro['gender'] = \
                html.find('div', class_='ProfileHeader-iconWrapper').find('svg').attrs['class'].split('--')[-1]
        except Exception as err:  # pylint: disable=unused-variable
            intro['gender'] = 'unknown'
        intro['living_location'] = ''
        intro['industry'] = ''
        intro['career'] = ''
        try:
            intro['self_intro'] = html.find('span', class_='RichText ztext ProfileHeader-headline').string + '; '
        except Exception as err:  # pylint: disable=unused-variable
            intro['self_intro'] = ''

        tags = html.find_all('div', class_='ProfileHeader-detailItem')
        for tag in tags:
            key = tag.find('span', class_='ProfileHeader-detailLabel').string
            if key == '居住地':
                key = 'living_location'
            if key == '所在行业':
                key = 'industry'
            if key == '职业经历':
                key = 'career'
            if key == '个人简介':
                key = 'self_intro'
            if key == '教育经历':
                key = 'education'

            subtags = tag.find_all('div', class_='ProfileHeader-field')
            if subtags != []:
                intro[key] = []
                for subtag in subtags:
                    intro[key].append(''.join([s for s in subtag.strings]))
            else:
                intro[key] += ''.join([s for s in tag.find('div', class_='ProfileHeader-detailValue').strings])

        achieves = html.find_all('div', class_='Profile-sideColumnItem')

        intro['achievement'] = []
        for ach in achieves:
            key = ''.join([s for s in ach.find('div', class_='IconGraf').strings])
            value = ''.join([s for s in ach.find('div', class_='Profile-sideColumnItemValue').strings])
            intro['achievement'].append((key.strip() + ': ' + value.strip()).rstrip(': '))
            numbers = html.find_all('strong', class_='NumberBoard-itemValue')
            # intro['followers'] = {"zhihu": {"follower": ''.join([tag.string for tag in numbers][0].split(',')),
            #                                 "fans": ''.join([tag.string for tag in numbers][-1].split(','))}}
            # intro['followers'] = {"zhihu": {"follower": ''.join([tag.string for tag in numbers][-1].split(',')),
            #                                 "fans": ''.join([tag.string for tag in numbers][0].split(','))}}
        intro['followers'] = {"zhihu": {"follower": follower,
                                        "fans": fans, 'likes': likes}}
        print('INTRO')
        print(intro)
        intros.append(intro)
        r = IntellifeedAPICaller()
        response = r.new_kol_profile(intro)
        print('***********')
        # print(response.content)
        print(response.status_code)
        # dict_result = response.content.decode('utf-8')
        # print('#########dict_result\n')
        # print(dict_result)
        # json_result = json.loads(dict_result)
        # print('############JSON\n')
        # print(json_result)
        if str(response.status_code) == '201' or str(response.status_code) == '200':
            json_result = response.json()
            print(json_result)
            uuid = json_result['id']
            settings.ZHIHU_KOL_IDs[url.split('/')[-2]] = uuid
    return intros

# actions.generate_top_writer_urls()
# print(settings.ZHIHU_G_TOP_WRITTER_URLS)
# get_intro(settings.ZHIHU_G_TOP_WRITTER_URLS)
