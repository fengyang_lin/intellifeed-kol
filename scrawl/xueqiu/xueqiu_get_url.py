import sys
import os

this_file_path = os.path.abspath('.')
abs_path = os.path.join(this_file_path, '../', '../')
sys.path.append(abs_path)
import time
import uuid
import settings
from scrawl.news.base import BaseScraper
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import datetime
import re


class ScrapeXueqiu(BaseScraper):
    headers = {'Accept': '*/*',
               'Encoding': 'gzip, deflate, br',
               'Language': 'zh-CN,zh;q=0.9',
               'Connection': 'keep-alive',
               'Cookie': 'CXID=87C4B04070D96B96DA45D758CC96969F; SUID=8578E7653865860A5AC2C21F00038887; SUV=008D'
                         '437027B43D015B13F7BD398E5453; dt_ssuid=3281138552; pex=C864C03270DED3DD8A06887A372DA21'
                         '9231FFAC25A9D64AE09E82AED12E416AC; ssuid=1070970760; IPLOC=CN3100; ad=4IRkvlllll2txMHQ'
                         'lllllVZ4fCYlllllBWNcPkllll9lllllxv7ll5@@@@@@@@@@; ABTEST=0|1546483965|v1; weixinIndexV'
                         'isited=1; JSESSIONID=aaaEYHTxDr_7BbOV3gdDw',
               'Host': 'weixin.sogou.com',
               'Referer': 'https://weixin.sogou.com/',
               'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome'
                             '/71.0.3578.98 Safari/537.36',
               'X-Requested-With': 'XMLHttpRequest'}

    _tmp_folder = '/tmp/{}'.format(uuid.uuid4())
    if not os.path.exists(_tmp_folder):
        os.makedirs(_tmp_folder)

    if not os.path.exists(_tmp_folder + '/user-data'):
        os.makedirs(_tmp_folder + '/user-data')

    if not os.path.exists(_tmp_folder + '/data-path'):
        os.makedirs(_tmp_folder + '/data-path')

    if not os.path.exists(_tmp_folder + '/cache-dir'):
        os.makedirs(_tmp_folder + '/cache-dir')

    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-gpu')
    chrome_options.add_argument('--window-size=1280x1696')
    chrome_options.add_argument('--user-data-dir={}'.format(_tmp_folder + '/user-data'))
    chrome_options.add_argument('--hide-scrollbars')
    chrome_options.add_argument('--enable-logging')
    chrome_options.add_argument('--log-level=0')
    chrome_options.add_argument('--v=99')
    chrome_options.add_argument('--single-process')
    chrome_options.add_argument('--data-path={}'.format(_tmp_folder + '/data-path'))
    chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument('--homedir={}'.format(_tmp_folder))
    chrome_options.add_argument('--disk-cache-dir={}'.format(_tmp_folder + '/cache-dir'))
    chrome_options.add_argument(
        'user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
        'Chrome/61.0.3163.100 Safari/537.36')
    prefs = {"download.default_directory": "/var/codes/intellifeed-kol"}
    # chrome_options.add_experimental_option("prefs", prefs)
    # driver = webdriver.Chrome(chrome_options=chrome_options)
    chrome_options.binary_location = settings.DRIVER_PATH_CHROME
    browser = webdriver.Chrome(chrome_options=chrome_options)

    def __init__(self, domain_url, media, lang, website=None):
        super(ScrapeXueqiu, self).__init__(domain_url, media, lang)

    def main(self):
        # chromedriver = "../../driver_binary_files/chromedriver_mac"
        # driver = webdriver.Chrome(chromedriver)
        f = open("xueqiu_users.txt")
        lines = f.readlines()
        with open('xueqiu_url.txt', 'r') as r:
            existed_url = r.readlines()
        text = ''
        for line in lines:
            lst = []
            url = line.strip('\n')
            url = 'https://xueqiu.com/' + url + '/column'
            try:
                self.browser.get(url)
            except Exception as err:
                print('browser.get(url) error : ' + str(err))
                time.sleep(1200)
                self.browser.get(url)
            self.browser.maximize_window()
            if self.browser.current_url == 'https://xueqiu.com/column/application':
                continue
            for i in range(5):
                print(i)
                try:
                    WebDriverWait(self.browser, 10, 0.5).until(
                        EC.presence_of_element_located((By.CLASS_NAME, "column__item__title")))
                except Exception as err:
                    print(err)
                    continue
                time.sleep(2)
                # WebDriverWait(self.browser, 10, 0.5).until(
                #     EC.presence_of_element_located((By.CLASS_NAME, "column__item__title")))
                hreflist = self.browser.find_elements_by_xpath("//*[@class='column__item__title']/a")
                publish_time = self.browser.find_elements_by_xpath("//*[@class='column__item__time']")
                publish_time = publish_time[0].get_attribute("textContent").strip()
                today = datetime.date.today()
                one_day = datetime.timedelta(days=1)
                yesterday = today - one_day
                t = publish_time.lstrip(' 发布于 ')
                if t[0] == '昨':
                    t = t.replace("昨天", str(yesterday))
                elif t[0] == '今':
                    t = t.replace("今天", str(today))
                elif '分钟' in t:
                    minute = re.findall('([1-9]\d*)', t)
                    t = (datetime.datetime.now() - datetime.timedelta(minutes=int(minute[0]))).strftime(
                        '%Y-%m-%d %H:%M')
                elif len(t) < 12:
                    t = '2019 ' + t
                print(t)
                now_time = (datetime.datetime.now() - datetime.timedelta(days=3)).strftime('%Y-%m-%d %H:%M')
                if now_time > t:
                    break
                for href_obj in hreflist:
                    href = href_obj.get_attribute("href")
                    if href + '\n' not in existed_url:
                        print(href)
                        lst.append(href + '\n')
                text = self.browser.find_element_by_class_name("pagination__next").get_attribute('style')
                if text != 'display: none;':
                    try:
                        # driver.implicitly_wait(6)
                        # WebDriverWait(self.browser, 10, 0.5).until(
                        #     EC.presence_of_element_located((By.CLASS_NAME, "column__item__title")))
                        time.sleep(2)
                        self.browser.find_element_by_class_name("pagination__next").click()
                        print('next')
                    except Exception as err:
                        print('print err!::: ', err)
                        time.sleep(5)
                        self.browser.find_element_by_class_name("pagination__next").click()
                else:
                    break
                time.sleep(1)
            print(lst)
            with open('xueqiu_url.txt', 'a') as f:
                f.writelines(lst)
            time.sleep(2)
        self.browser.quit()
        f.close()

# if __name__ == '__main__':
#     main()
