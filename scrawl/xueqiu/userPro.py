# coding=utf-8

import json
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from common.api import IntellifeedAPICaller
import common.common
import settings
import time
from translate import Translator

browser = webdriver.Chrome(r"../../driver_binary_files/chromedriver_mac")
# read data from text file
fauthor = open('xueqiu_users.txt', 'r', encoding="utf-8")
r_relationship = open('relationship.json', 'r', encoding="utf-8")
uuid_dict_r = json.load(r_relationship)
frelationship = open('relationship-2.json', 'w', encoding="utf-8")
authors = fauthor.readlines()
d = {}

for authorn in authors:
    author = authorn.replace('\n', '')
    print(author)
    if author not in uuid_dict_r:
        link = 'https://xueqiu.com/u/' + author
        browser.get(link)
        source = browser.find_element_by_xpath('//*[@id="app"]/div[2]/div/div[1]/div[1]/img').get_attribute('src')
        s3_url = common.common.get_avatar_and_upload_to_s3(source, 'xueqiu_' + author + '.png')
        a = {}
        nick_name = browser.find_element_by_xpath('//*[@id="app"]/div[2]/div/div[1]/div[2]/h2').text
        living_location = browser.find_element_by_class_name('profiles__address').find_element_by_tag_name('li').text[
                          1:]. \
            replace('城市/地区', '').replace(' 不限', '')
        self_intro = browser.find_element_by_xpath('//*[@id="app"]/div[2]/div/div[1]/div[2]/p').text
        try:
            g = browser.find_element_by_class_name('profiles__icon--m')
            gender = 'male'
        except NoSuchElementException:
            gender = 'female'
        try:
            achievement = browser.find_element_by_class_name('profiles__verify-info__item').text
        except NoSuchElementException:
            achievement = ''
        follower = browser.find_element_by_xpath(
            '//*[@id="app"]/div[2]/div/div[1]/div[2]/div[1]/ul[1]/li[1]/a/strong').text
        fans = browser.find_element_by_xpath('//*[@id="app"]/div[2]/div/div[1]/div[2]/div[1]/ul[1]/li[2]/a/strong').text
        print(fans)
        a['nick_name'] = nick_name
        a['living_location'] = living_location
        a['self_intro'] = self_intro
        a['gender'] = gender
        a['achievement'] = str(achievement).replace('\n', '')
        a['avatar'] = 'https://static.redpulse.com/intellifeeds/kol_profile/xueqiu_' + author + '.png'
        a['original_url'] = 'https://xueqiu.com/u/' + author
        a['followers'] = {'xueqiu': {'follower': fans,
                                     'fans': follower}}
        a['real_name'] = ''
        a['email'] = ''
        a['industry'] = ''
        a['career'] = ''
        a['education'] = ''
        translator = Translator(from_lang="zh_CN", to_lang="en")
        result = translator.translate(a['nick_name'])
        a['nick_name'] = result
        r = IntellifeedAPICaller()
        print(a)
        response = r.new_kol_profile(a)
        print(response.status_code)
        print(response.text)
        dict_result = response.content.decode('utf-8')
        json_result = json.loads(dict_result)
        uuid = json_result['id']
        d[author] = uuid
        uuid_dict_r[author] = uuid
        w_relationship = open('relationship.json', 'w', encoding="utf-8")
        w_relationship.write(json.dumps(uuid_dict_r))
        w_relationship.close()

# print(detail)
r_relationship.close()
frelationship.write(json.dumps(d))
browser.quit()
