# coding=utf-8

from selenium import webdriver
import common.common
import settings

f = open('xueqiu_users.txt', 'r', encoding="utf-8")
users = f.readlines()
f.close()

browser = webdriver.Chrome(r"../../driver_binary_files/chromedriver_mac")
for usern in users:
    user = usern.replace('\n', '')
    browser.get('https://xueqiu.com/u/' + user)
    source = browser.find_element_by_xpath('//*[@id="app"]/div[2]/div/div[1]/div[1]/img').get_attribute('src')
    s3_url = common.common.get_avatar_and_upload_to_s3(source, 'xueqiu_' + user + '.png')
    print(s3_url)

browser.quit()
