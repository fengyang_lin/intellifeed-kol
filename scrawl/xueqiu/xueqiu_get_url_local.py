import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import datetime
import re


def main():
    chromedriver = "../../driver_binary_files/chromedriver_mac"
    browser = webdriver.Chrome(chromedriver)
    f = open("xueqiu_users.txt")
    lines = f.readlines()
    with open('xueqiu_url.txt', 'r') as r:
        existed_url = r.readlines()
    text = ''
    for line in lines:
        lst = []
        url = line.strip('\n')
        url = 'https://xueqiu.com/' + url + '/column'
        browser.get(url)
        browser.maximize_window()
        if browser.current_url == 'https://xueqiu.com/column/application':
            continue
        for i in range(5):
            print(i)
            try:
                WebDriverWait(browser, 10, 0.5).until(
                    EC.presence_of_element_located((By.CLASS_NAME, "column__item__title")))
            except Exception as err:
                print(err)
                continue
            time.sleep(2)
            hreflist = browser.find_elements_by_xpath("//*[@class='column__item__title']/a")
            publish_time = browser.find_elements_by_xpath("//*[@class='column__item__time']")
            publish_time = publish_time[0].get_attribute("textContent").strip()
            today = datetime.date.today()
            one_day = datetime.timedelta(days=1)
            yesterday = today - one_day
            t = publish_time.lstrip(' 发布于 ')
            if t[0] == '昨':
                t = t.replace("昨天", str(yesterday))
            elif t[0] == '今':
                t = t.replace("今天", str(today))
            elif '分钟' in t:
                minute = re.findall('([1-9]\d*)', t)
                t = (datetime.datetime.now() - datetime.timedelta(minutes=int(minute[0]))).strftime('%Y-%m-%d %H:%M')
            elif len(t) < 12:
                t = '2019 ' + t
            print(t)
            now_time = (datetime.datetime.now() - datetime.timedelta(days=3)).strftime('%Y-%m-%d %H:%M')
            if now_time > t:
                break
            for href_obj in hreflist:
                href = href_obj.get_attribute("href")
                if href + '\n' not in existed_url:
                    print(href)
                    lst.append(href + '\n')
            text = browser.find_element_by_class_name("pagination__next").get_attribute('style')
            if text != 'display: none;':
                try:
                    # driver.implicitly_wait(6)
                    WebDriverWait(browser, 10, 0.5).until(
                        EC.presence_of_element_located((By.CLASS_NAME, "column__item__title")))
                    browser.find_element_by_class_name("pagination__next").click()
                    print('next')
                except Exception as err:
                    print(err)
                    time.sleep(5)
                    browser.find_element_by_class_name("pagination__next").click()
            else:
                break
            time.sleep(1)
        print(lst)
        with open('xueqiu_url.txt', 'a') as f:
            f.writelines(lst)
        time.sleep(2)
    browser.close()
    f.close()


if __name__ == '__main__':
    main()
