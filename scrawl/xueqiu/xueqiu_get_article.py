# coding=utf-8
import sys
import os

this_file_path = os.path.abspath('.')
abs_path = os.path.join(this_file_path, '../', '../')
sys.path.append(abs_path)
import re
import json
import datetime
import time
import urllib.request as urllib2
from urllib import error
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from common import api
from common import common
import settings
from scrawl.xueqiu.xueqiu_get_url import ScrapeXueqiu


# get article urls from author column website
def xueqiu_urls():
    browser = webdriver.Chrome(settings.DRIVER_PATH_CHROME)
    # read data from text file
    fauthor = open('authors.txt')
    authors = []
    c = fauthor.readline()
    while c:
        authors.append(c)
        c = fauthor.readline()
    fauthor.close()

    f1 = open('urls.txt', 'r', encoding="utf-8")
    urls = []
    c = f1.readline()
    while c:
        urls.append(c)
        c = f1.readline()
    f1.close()
    farticle = open('urls.txt', 'a+')

    for author in authors:
        a = 'https://xueqiu.com/' + author + '/column'
        browser.get(a)
        path = '//*[@id="app"]/div[2]/div/div[2]/div/div/div/div[1]/h3'

        for i in range(1, 20):
            try:
                path = '//*[@id="app"]/div[2]/div[2]/div/div[' + str(i) + ']/h3/a'
                link = browser.find_element_by_xpath(path).get_attribute(name="href")
                path = '//*[@id="app"]/div[2]/div[2]/div/div[' + str(i) + ']/div[2]'
                date = browser.find_element_by_xpath(path).text
            except NoSuchElementException:
                continue
                browser.refresh()
                time.sleep(3)
                path = '//*[@id="app"]/div[2]/div[2]/div/div[' + str(i) + ']/h3/a'
                link = browser.find_element_by_xpath(path).get_attribute("href")
                path = '//*[@id="app"]/div[2]/div[2]/div/div[' + str(i) + ']/div[2]'
                date = browser.find_element_by_xpath(path).text
            if link + '\n' in urls or "201" in date:
                break
            else:
                farticle.write(link + '\n')
                print(link)
    farticle.close()
    browser.quit()


# scrape urls and get articles
def xueqiu(url):
    author_id = url[19:29]
    uuid = info[author_id]
    article_id = url[30:]

    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0'}
    try:
        req = urllib2.Request(url, headers=headers)
        content = urllib2.urlopen(req).read().decode('utf-8')
    except error.HTTPError as err:
        return err.code
    # get likes
    likes_lst = re.findall(r'\"like_count\":([1-9]\d*)', content)
    if likes_lst != []:
        likes = likes_lst[0]
    else:
        likes = 0

    # update kol-porfile followers
    new_followers = {}
    json_data = {"user_id": uuid}
    query_response = api_instance.query_kol_profile(json_data)
    try:
        print('query status_code', query_response.status_code)
        print(query_response.json())
    except:
        return 0
    old_followers = query_response.json()['followers']
    if 'likes' in old_followers['xueqiu']:
        old_followers['xueqiu']['likes'] = str(int(old_followers['xueqiu']['likes']) + int(likes))
    else:
        old_followers['xueqiu']['likes'] = '0'
    new_followers['user_id'] = uuid
    new_followers['followers'] = old_followers
    update_response = api_instance.update_kol_profile(new_followers)
    try:
        print('update status_code', update_response.status_code)
        print('update-', update_response.json())
    except:
        return 0
    soup = BeautifulSoup(content, 'lxml')
    content = soup.select("article > div.article__bd__detail")
    content = str(content[0])

    # other information
    try:
        title = soup.select("h1.article__bd__title")[0].text
    except:
        return 0
    author = soup.select("body > div > div > div > div > a.name")[0].text.rstrip('()')

    # time
    time_ = soup.select("body > div > div > div > div > a.time")
    today = datetime.date.today()
    one_day = datetime.timedelta(days=1)
    yesterday = today - one_day

    if time_:
        t = time_[0].text.lstrip('发布于')
        if t[0] == '昨':
            t = t.replace("昨天", str(yesterday))
        elif t[0] == '今':
            t = t.replace("今天", str(today))
        elif t[0] != '2':
            t = '2019-' + t

    else:
        t = soup.findAll("a", {"class": "edit-time"})[0].text.lstrip("修改于")
        if t[0] == '昨':
            t = t.replace("昨天", str(yesterday))
        if t[0] == '今':
            t = t.replace("今天", str(today))

            # replace original urls with s3 links
    get_img = soup.find_all("img", {"class": "ke_img"})
    img = []
    for item in get_img:
        image = item['src']
        img.append(image)

    for i, j in enumerate(img):
        img_name = 'xueqiu_' + str(article_id) + '_' + str(i) + '.jpg'
        try:
            img_path = common.get_article_img_and_upload_to_s3(j, img_name)
        except:
            time.sleep(1200)
            img_path = common.get_article_img_and_upload_to_s3(j, img_name)
        print(img_path)
        content = content.replace(j, img_path)

    content_info = dict()
    content_info['source_website_name'] = "Xueqiu"
    content_info['source_domain_url'] = "https://xueqiu.com/"
    content_info['source_full_destination_url'] = url
    content_info['title_cn'] = title
    content_info['content_cn'] = content
    content_info['publish_time'] = t
    content_info['author'] = author
    content_info['kol_id'] = uuid
    content_info['types'] = "social_media"
    article_dict = {"content_info": content_info}

    return article_dict


if __name__ == '__main__':
    while True:
        # get urls
        # xueqiu_urls()

        xueqiu_obj = ScrapeXueqiu('www.baidu.com', 'xueqiu', 'cn')
        xueqiu_obj.main()
        # run scraper
        list_ = []
        file = open('relationship.json', 'r')
        info = json.load(file)
        file.close()

        f = open('xueqiu_url.txt', 'r')
        url_list = f.readlines()
        f.close()
        for url_ in url_list:
            url_file = open('scraped_urls_1.txt', 'r')
            scraped = url_file.readlines()
            current_location = url_list.index(url_)

            # print(scraped)
            url_file.close()
            if url_ != '\n' and url_ not in scraped:
                print(url_)
                # print('left: ' + str((len(url_list) - len(scraped))))
                api_instance = api.IntellifeedAPICaller()
                content_info_dict = xueqiu(url_.strip())
                if content_info_dict == 404:
                    continue
                if content_info_dict == 0:
                    continue
                ret = api_instance.new_content(content_info_dict)
                if type(ret) is str:
                    continue
                # append the url to list
                if str(ret.status_code) == '201' or str(ret.status_code) == '200':
                    url_file = open('scraped_urls_1.txt', 'a')
                    url_file.write(url_)
                    url_file.close()
                    print(str(ret.status_code))
                    print(ret.json())
                    print(content_info_dict)
                else:
                    print(str(ret.status_code))
        print("sleeping ~~~~~~~~~~~~~~~~~~")
        time.sleep(86400)
