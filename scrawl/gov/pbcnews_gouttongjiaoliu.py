"""
# run the code every 2 day(9:00 am), now we just scrape first page of the website

URL： http://www.pbc.gov.cn/

some Tips:
  1. Now just scrawl the first page, because the website update so slow
  2. sometimes, it's hard to upload file to Aws s3. You can run the code again

"""

from scrawl.news.base import BaseScraper
import settings
import requests
import time
import uuid
from selenium import webdriver
from newspaper import Article
from bs4 import BeautifulSoup as BS
from selenium.common import exceptions
from selenium.webdriver.common.keys import Keys
from common import api
from urllib.parse import quote, unquote
import os
# from selenium_framework.utils import BaseAction, close_browser

outside_api_caller = api.IntellifeedAPICaller()


class PbcGovNews(BaseScraper):

    _tmp_folder = '/tmp/{}'.format(uuid.uuid4())
    if not os.path.exists(_tmp_folder):
        os.makedirs(_tmp_folder)

    if not os.path.exists(_tmp_folder + '/user-data'):
        os.makedirs(_tmp_folder + '/user-data')

    if not os.path.exists(_tmp_folder + '/data-path'):
        os.makedirs(_tmp_folder + '/data-path')

    if not os.path.exists(_tmp_folder + '/cache-dir'):
        os.makedirs(_tmp_folder + '/cache-dir')

    chrome_options = webdriver.ChromeOptions()

    ####### for linux
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-gpu')
    chrome_options.add_argument('--window-size=1280x1696')
    chrome_options.add_argument('--user-data-dir={}'.format(_tmp_folder + '/user-data'))
    chrome_options.add_argument('--hide-scrollbars')
    chrome_options.add_argument('--enable-logging')
    chrome_options.add_argument('--log-level=0')
    chrome_options.add_argument('--v=99')
    chrome_options.add_argument('--single-process')
    chrome_options.add_argument('--data-path={}'.format(_tmp_folder + '/data-path'))
    chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument('--homedir={}'.format(_tmp_folder))
    chrome_options.add_argument('--disk-cache-dir={}'.format(_tmp_folder + '/cache-dir'))
    chrome_options.add_argument(
        'user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
        'Chrome/61.0.3163.100 Safari/537.36')

    prefs = {"download.default_directory": "/tmp/"}
    chrome_options.add_experimental_option("prefs", prefs)

    chrome_options.binary_location = settings.DRIVER_PATH_CHROME
    browser = webdriver.Chrome(chrome_options=chrome_options)

    browser.set_window_position(500, 200)

    # ####### for linux
    # browser_file_download = webdriver.Chrome(chrome_options=chrome_options)
    browser_detail = webdriver.Chrome(chrome_options=chrome_options)

    ####### for MAC
    # browser_file_download = webdriver.Chrome(chrome_driver_location, chrome_options=chrome_options)
    # browser_detail = webdriver.Chrome(chrome_driver_location)

    browser_detail.set_window_position(200, 200)
    # browser_file_download.set_window_position(200, 200)
    api_s3 = api.IntellifeedAPICaller()

    part_1_url = '//*[@id="11040"]/div[2]/div[1]/table/tbody/tr[2]/td/table['
    part_2_url = ']/tbody/tr/td[2]/span'  # for Pub Time
    item_1_xpath = '//*[@id="11040"]/div[2]/div[1]/table/tbody/tr[2]/td/table['
    item_2_xpath = ']/tbody/tr/td[2]/font/a'  # for URL

    part_1_diao_cha_url = '//*[@id="11871"]/div[2]/div[1]/table/tbody/tr[2]/td/table['
    part_2_diao_cha_url = ']/tbody/tr/td[2]/span'
    item_1_diao_cha_xpath = '//*[@id="11871"]/div[2]/div[1]/table/tbody/tr[2]/td/table['
    item_2_diao_cha_xpath = ']/tbody/tr/td[2]/font/a'

    def __init__(self, domain_url, media, lang):

        super(PbcGovNews, self).__init__(domain_url, media, lang)

    def get_response(self):

        self.browser.set_window_position(200, 200)
        self.browser.get(self.domain_url)
        for item in range(1, 16):
            item_obj = self.browser.find_element_by_xpath(self.item_1_xpath + str(item) + self.item_2_xpath)
            title = item_obj.text
            href = item_obj.get_attribute("href")
            pub_time = self.browser.find_element_by_xpath(self.part_1_url + str(item) + self.part_2_url).text
            self.browser_detail.get(href)
            content = self.browser_detail.find_element_by_id('zoom').get_attribute('innerHTML')
            to_replace_file_link_to_s3 = self.browser_detail.find_element_by_id('zoom').find_elements_by_css_selector(
                'a[href]')  # 'pdf or xls'
            to_replace_img_link_to_s3 = self.browser_detail.find_element_by_id('zoom').find_elements_by_css_selector(
                'img[src]')  # images

            replaced_content = content
            for sub_item in to_replace_file_link_to_s3:
                link_string = str(sub_item.get_attribute("href"))
                print('link_string: ' + link_string)
                if link_string.endswith('.com') or link_string.endswith('.net/'):  # is a website Url
                    continue
                elif link_string.find('.xls') > 0:
                    file_type = 'application/x-xls'
                    print('replace the xls ...')

                elif link_string.find('.pdf') > 0:
                    file_type = 'application/pdf'
                    print('replace the pdf ...')
                else:
                    file_type = 'image/png'
                    print('replace the pdf ...')
                file_name = link_string.split('/')[-1]
                path_file_upload_to_s3 = outside_api_caller.get_gov_file_and_upload_to_s3(link_string, file_name, '',
                                                                                          file_type)
                if path_file_upload_to_s3:
                    content = content.get_attribute('innerHTML')
                    content = content.replace(link_string, path_file_upload_to_s3)
                else:
                    print('upload Failed the detail_div is %s' % detail_url)
                    break  # the img failed to upload to s3 , we will abandon the article

            for sub_img_item in to_replace_img_link_to_s3:
                print('start to replace the Img link ')
                link_string = sub_img_item.get_attribute("src")
                decode_url = unquote(link_string, 'utf-8')
                file_name = decode_url.split('/')[-1]
                suffix_name = file_name.split('.')[-1]
                file_type = 'image/png'
                return_value = self.api_s3.get_gov_file_and_upload_to_s3(decode_url, file_name, suffix_name, file_type)
                if return_value:
                    static_image_in_s3_path = settings.AWS_S3_Intellifeed_HEAD + 'gov/' + file_name
                    content = content.replace(sub_img_item, static_image_in_s3_path)
                else:
                    content = ''
                    break
            # if the img/xls/pdf failed to upload to s3 , we will abandon the article
            if replaced_content:
                content_info = dict()
                content_info['url'] = href
                content_info['title'] = title
                content_info['content'] = content
                content_info['published_at'] = pub_time
                content_info['types'] = "gov"
                content_info['language'] = 'cn'
                content_info['is_added_to_source'] = False
                content_info['media'] = self.media
                self.result_json.append(content_info)
            else:
                print('the Content is missing ')
                continue
        self.browser.quit()
        self.browser_detail.quit()

        response = outside_api_caller.send_latest_news_data_to_mvp(self.result_json, 'gov')
        if not response:
            print('Upload Failed ')
            return None
        else:
            print('The status code is %s ' % response.status_code)
            return response

    def get_response_from_diaochatongji(self, url):

        self.browser.set_window_position(200, 200)
        self.browser.get(url)
        for item in range(1, 21):
            item_obj = self.browser.find_element_by_xpath(self.item_1_diao_cha_xpath + str(item) + self.item_2_diao_cha_xpath)
            title = item_obj.text
            href = item_obj.get_attribute("href")
            pub_time = self.browser.find_element_by_xpath(self.part_1_diao_cha_url + str(item) + self.part_2_diao_cha_url).text
            self.browser_detail.get(href)

            content = self.browser_detail.find_element_by_id('zoom').get_attribute('innerHTML')
            to_replace_file_link_to_s3 = self.browser_detail.find_element_by_id('zoom').find_elements_by_css_selector(
                'a[href]')  # 'pdf or xls'
            to_replace_img_link_to_s3 = self.browser_detail.find_element_by_id('zoom').find_elements_by_css_selector(
                'img[src]')  # images

            for sub_item in to_replace_file_link_to_s3:
                link_string = str(sub_item.get_attribute("href"))
                print('link_string: ' + link_string)
                if link_string.endswith('.com') or link_string.endswith('.net/'):  # is a website Url
                    continue
                elif link_string.find('.xls') > 0:
                    file_type = 'application/x-xls'
                    print('replace the xls ...')

                elif link_string.find('.pdf') > 0:
                    file_type = 'application/pdf'
                    print('replace the pdf ...')
                else:
                    file_type = 'image/png'
                    print('replace the pdf ...')
                file_name = link_string.split('/')[-1]
                path_file_upload_to_s3 = outside_api_caller.get_gov_file_and_upload_to_s3(link_string, file_name, '',
                                                                                          file_type)
                if path_file_upload_to_s3:
                    content = content.replace(link_string, path_file_upload_to_s3)
                else:
                    print('upload Failed the detail_div is %s' % detail_url)
                    break  # the img failed to upload to s3 , we will abandon the article

            for sub_img_item in to_replace_img_link_to_s3:
                print('start to replace the Img link ')
                link_string = sub_img_item.get_attribute("src")
                decode_url = unquote(link_string, 'utf-8')
                print(decode_url)
                file_name = decode_url.split('/')[-1]
                suffix_name = file_name.split('.')[-1]
                file_type = 'image/png'
                return_value = self.api_s3.get_gov_file_and_upload_to_s3(decode_url, file_name, suffix_name, file_type)
                if return_value:
                    static_image_in_s3_path = settings.AWS_S3_Intellifeed_HEAD + 'gov/' + file_name
                    content = content.replace(link_string, static_image_in_s3_path)
                else:
                    content = ''
                    break
            # if the img/xls/pdf failed to upload to s3 , we will abandon the article
            if content:
                content_info = dict()
                content_info['url'] = href
                content_info['title'] = title
                content_info['content'] = content
                content_info['published_at'] = pub_time
                content_info['types'] = "gov"
                content_info['language'] = 'cn'
                content_info['is_added_to_source'] = False
                content_info['media'] = self.media
                self.result_json.append(content_info)
            else:
                print('the Content is missing ')
                continue
        self.browser.quit()
        self.browser_detail.quit()

        response = outside_api_caller.send_latest_news_data_to_mvp(self.result_json, 'gov')
        if not response:
            print('Upload Failed ')
            return None
        else:
            print('The status code is %s ' % response.status_code)
            return response




#
#
# def pbc_gov_news():
#     browser = webdriver.Chrome(settings.DRIVER_PATH_CHROME)
#     browser_detail = webdriver.Chrome(settings.DRIVER_PATH_CHROME)
#     browser.set_window_position(200, 300)  # make sure the screen doesn't in the left
#     result_list = []
#     api_s3 = api.IntellifeedAPICaller()
#     for item in range(1, 21):
#         browser.get('http://www.pbc.gov.cn/diaochatongjisi/116219/116225/index.html')
#         item_obj = browser.find_element_by_xpath(
#             '//*[@id="11871"]/div[2]/div[1]/table/tbody/tr[2]/td/table[' + str(item) +']/tbody/tr/td[2]/font/a')
#         title = item_obj.text
#         href = item_obj.get_attribute("href")
#         pub_time = browser.find_element_by_xpath('//*[@id="11871"]/div[2]/div[1]/table/tbody/tr[2]/td/table[' + str(item) + ']/tbody/tr/td[2]/span').text
#         browser_detail.get(href)
#         content = browser_detail.find_element_by_id('zoom').get_attribute('innerHTML')
#         replaced_file_link_to_s3 = browser_detail.find_element_by_id('zoom').find_elements_by_css_selector('a[href]')
#         replaced_img_link_to_s3 = browser_detail.find_element_by_id('zoom').find_elements_by_css_selector('img[src]')
#
#         for sub_item in replaced_file_link_to_s3:
#             link_string = sub_item.getAttribute("href")
#             file_name = link_string.split('/')[-1]   # in the article is the part of URL
#             suffix_name = file_name.split('.')[-1]
#             if suffix_name == 'pdf':
#                 file_type = 'application/pdf'
#             elif suffix_name == 'xls':
#                 file_type == 'application/vnd.ms-excel'
#             else:
#                 file_type = 'image/png'
#             return_value = api_s3.get_gov_file_and_upload_to_s3(link_string, file_name, suffix_name, file_type)
#             if return_value:
#                 replaced_content = content.replace(link_string_in_article, return_value)
#             else:
#                 replaced_content = ''
#                 break # the img failed to upload to s3 , we will abandon the article
#
#         for sub_img_item in replaced_img_link_to_s3:
#             link_string = sub_img_item.get_attribute("src")
#             # in the article is the part of URL
#             decode_url = unquote(link_string, 'utf-8')
#             link_string_in_article = decode_url.replace('http://www.pbc.gov.cn', '')
#             file_name = decode_url.split('/')[-1]
#             suffix_name = file_name.split('.')[-1]
#             file_type = 'image/png'
#             return_value = api_s3.get_gov_file_and_upload_to_s3(decode_url, file_name, suffix_name, file_type)
#             if return_value:
#                 replaced_content = content.replace(link_string_in_article, return_value)
#             else:
#                 replaced_content = ''
#                 break # the img failed to upload to s3 , we will abandon the article
#         # send data to S3
#         if replaced_content:
#             content_info = dict()
#             content_info['source_website_name'] = "pbc"
#             content_info['source_domain_url'] = "http://www.pbc.gov.cn"
#             content_info['source_full_destination_url'] = href
#             content_info['title_cn'] = title
#             content_info['content_cn'] = replaced_content
#             content_info['publish_time'] = pub_time
#             content_info['types'] = "gov"
#             result_list.append(content_info)

# def tets_pbc_gov_news():
#     api_s3 = api.IntellifeedAPICaller()
#     test_url = 'http://www.pbc.gov.cn/diaochatongjisi/116219/116225/index.html'
#     detail_page_url = '//*[@id="11871"]/div[2]/div[1]/table/tbody/tr[2]/td/table[5]/tbody/tr/td[2]/font/a'
#     browser = webdriver.Chrome(settings.DRIVER_PATH_CHROME)
#     browser.set_window_position(200, 300)
#     browser.get(test_url)
#     href = browser.find_element_by_xpath(detail_page_url).get_attribute("href")
#     browser.get(href)
#     content = browser.find_element_by_id('zoom').get_attribute('innerHTML')
#     replaced_file_link_to_s3 = browser.find_element_by_id('zoom').find_elements_by_css_selector('a[href]')
#      # find all the
#     for link in replaced_file_link_to_s3:
#         link_string = link.get_attribute("href")
#         file_name = link_string.split('/')[-1]
#         suffix_name = file_name.split('.')[-1]
#         if suffix_name == 'pdf':
#             file_type = 'application/pdf'
#         elif suffix_name == 'xls':
#             file_type == 'application/x-xls'
#         else:
#             file_type = 'image/png'
#         return_value = api_s3.get_gov_file_and_upload_to_s3(link_string, file_name, file_type)
#         print(return_value)






    # print(len(child_elements))
    # for item in child_elements:
    #     print(item.text)
        # try:
        #     a = item.find_element_by_tag_name('p')
        #     print(a.text)
        # except exceptions.NoSuchElementException as err:
        #     a = item.find_element_by_tag_name('table')
        #     print(a)


        # if hasattr(item, 'p'):
        #     print(item.text)
        # elif hasattr(item, 'table'):
        #     print(item.find_element_by_xpath('/table[2]/tbody/tr[1]/td[2]/p'))
        # else:
        #     pass

    # response = requests.get(href, headers=header_data)
    # bs_response = BS(response.content, 'lxml')
    # print(bs_response)
    # test = bs_response.find('div', class_='header')


# class PbcGovNews(BaseScraper):
#     def __init__(self, domain_url, media, lang):
#
#         self.headers = {
#             'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
#             'Accept-Encoding': 'gzip, deflate',
#             'Accept-Language': 'en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7',
#             'Cache-Control': 'max-age=0',
#             'Cookie': 'ccpassport=8cd3491a66ee9726ab4d427f37b6305c; wzwsconfirm=e446596f10d061196f4dd32a49abfa22; wzwstemplate=OA==; wzwschallenge=-1; wzwsvtime=1550217355; wzws_cid=c0bd8da249455c91e208f41329ee734e8d9e53795e3f53de37eecf8c2fc19d20def81f88f7a2e40148204dbb0670110d7e2025be33e355c906c4b880eba06b83',
#             'Connection': 'keep-alive',
#             'Host': 'www.pbc.gov.cn',
#             'If-Modified-Since': 'Fri, 15 Feb 2019 06:58:58 GMT',
#             'If-None-Match': 'W/"118ba-8ad8-581e94f19b080"',
#             'Upgrade-Insecure-Requests': '1',
#             'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
#             'Referer': 'http://www.pbc.gov.cn/diaochatongjisi/116219/116225/index.html'
#         }
#         super(PbcGovNews, self).__init__(domain_url, media, lang)
#
#     def response_data(self):
#
#         page = 1
#         while page <= 2:
#
#             if page == 1:
#                 web_news_url = self.domain_url + '.html'
#             else:
#                 web_news_url = self.domain_url + str(page) + '.html'
#             bs_response = self.extract_content_by_bs_header(web_news_url, self.headers)
#             print(bs_response)
#             all_article_list = bs_response.findAll('table')
#             for item in all_article_list:
#                 print(item.find('tbody').find('tr').find('td', height="22").find('font').find('a')['href'])
#                 return ''
#             page += 1






