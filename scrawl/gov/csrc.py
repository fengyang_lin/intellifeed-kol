"""
# run the code every 2 day(9:00 am), now we just scrape first page of the website

URL： http://www.csrc.gov.cn/pub/newsite/zjhxwfb/xwdd/
some Tips:
  1. Now just scrawl the first page, because the website update so slow
  2. sometimes, it's hard to upload file to Aws s3. You can run the code again

"""

from scrawl.news.base import BaseScraper
import settings
import requests
import time
from selenium import webdriver
from newspaper import Article
from bs4 import BeautifulSoup as BS
from selenium.common import exceptions
from selenium.webdriver.common.keys import Keys
from common import api
from urllib.parse import quote, unquote
from readability.readability import Document
import urllib.request as urllib2


class CsrcGovNews(BaseScraper):
    def __init__(self, domain_url, media, lang):
        super(CsrcGovNews, self).__init__(domain_url, media, lang)
        self.item_1_xpath = '//*[@id="myul"]/li['
        self.item_2_xpath = ']/a'  # href url link
        self.item_3_xpath = ']/span'  # pub time
        self.api_s3 = api.IntellifeedAPICaller()

    def response_data(self):
        selector = self.extract_content_by_bs_content(self.domain_url)
        for item in range(1, 21):
            title = selector.find('ul', id='myul').findAll('li')[item-1].find('a')['title']
            pub_time = selector.find('ul', id='myul').findAll('li')[item-1].find('span').text
            url_link = selector.find('ul', id='myul').findAll('li')[item-1].find('a')['href']
            detail_link_url = 'http://www.csrc.gov.cn/pub/newsite/zjhxwfb/xwdd' + url_link[1:]
            response = requests.get(detail_link_url)
            content = Document(response.content).summary()
            if content:
                content_info = dict()
                content_info['url'] = url_link
                content_info['title'] = title
                content_info['content'] = content
                content_info['published_at'] = pub_time
                content_info['types'] = "gov"
                content_info['language'] = 'cn'
                content_info['is_added_to_source'] = False
                content_info['media'] = self.media
                self.result_json.append(content_info)
        return self.result_json



#
#
# def pbc_gov_news():
#     browser = webdriver.Chrome(settings.DRIVER_PATH_CHROME)
#     browser_detail = webdriver.Chrome(settings.DRIVER_PATH_CHROME)
#     browser.set_window_position(200, 300)  # make sure the screen doesn't in the left
#     result_list = []
#     api_s3 = api.IntellifeedAPICaller()
#     for item in range(1, 21):
#         browser.get('http://www.pbc.gov.cn/diaochatongjisi/116219/116225/index.html')
#         item_obj = browser.find_element_by_xpath(
#             '//*[@id="11871"]/div[2]/div[1]/table/tbody/tr[2]/td/table[' + str(item) +']/tbody/tr/td[2]/font/a')
#         title = item_obj.text
#         href = item_obj.get_attribute("href")
#         pub_time = browser.find_element_by_xpath('//*[@id="11871"]/div[2]/div[1]/table/tbody/tr[2]/td/table[' + str(item) + ']/tbody/tr/td[2]/span').text
#         browser_detail.get(href)
#         content = browser_detail.find_element_by_id('zoom').get_attribute('innerHTML')
#         replaced_file_link_to_s3 = browser_detail.find_element_by_id('zoom').find_elements_by_css_selector('a[href]')
#         replaced_img_link_to_s3 = browser_detail.find_element_by_id('zoom').find_elements_by_css_selector('img[src]')
#
#         for sub_item in replaced_file_link_to_s3:
#             link_string = sub_item.getAttribute("href")
#             file_name = link_string.split('/')[-1]   # in the article is the part of URL
#             suffix_name = file_name.split('.')[-1]
#             if suffix_name == 'pdf':
#                 file_type = 'application/pdf'
#             elif suffix_name == 'xls':
#                 file_type == 'application/vnd.ms-excel'
#             else:
#                 file_type = 'image/png'
#             return_value = api_s3.get_gov_file_and_upload_to_s3(link_string, file_name, suffix_name, file_type)
#             if return_value:
#                 replaced_content = content.replace(link_string_in_article, return_value)
#             else:
#                 replaced_content = ''
#                 break # the img failed to upload to s3 , we will abandon the article
#
#         for sub_img_item in replaced_img_link_to_s3:
#             link_string = sub_img_item.get_attribute("src")
#             # in the article is the part of URL
#             decode_url = unquote(link_string, 'utf-8')
#             link_string_in_article = decode_url.replace('http://www.pbc.gov.cn', '')
#             file_name = decode_url.split('/')[-1]
#             suffix_name = file_name.split('.')[-1]
#             file_type = 'image/png'
#             return_value = api_s3.get_gov_file_and_upload_to_s3(decode_url, file_name, suffix_name, file_type)
#             if return_value:
#                 replaced_content = content.replace(link_string_in_article, return_value)
#             else:
#                 replaced_content = ''
#                 break # the img failed to upload to s3 , we will abandon the article
#         # send data to S3
#         if replaced_content:
#             content_info = dict()
#             content_info['source_website_name'] = "pbc"
#             content_info['source_domain_url'] = "http://www.pbc.gov.cn"
#             content_info['source_full_destination_url'] = href
#             content_info['title_cn'] = title
#             content_info['content_cn'] = replaced_content
#             content_info['publish_time'] = pub_time
#             content_info['types'] = "gov"
#             result_list.append(content_info)

# def tets_pbc_gov_news():
#     api_s3 = api.IntellifeedAPICaller()
#     test_url = 'http://www.pbc.gov.cn/diaochatongjisi/116219/116225/index.html'
#     detail_page_url = '//*[@id="11871"]/div[2]/div[1]/table/tbody/tr[2]/td/table[5]/tbody/tr/td[2]/font/a'
#     browser = webdriver.Chrome(settings.DRIVER_PATH_CHROME)
#     browser.set_window_position(200, 300)
#     browser.get(test_url)
#     href = browser.find_element_by_xpath(detail_page_url).get_attribute("href")
#     browser.get(href)
#     content = browser.find_element_by_id('zoom').get_attribute('innerHTML')
#     replaced_file_link_to_s3 = browser.find_element_by_id('zoom').find_elements_by_css_selector('a[href]')
#      # find all the
#     for link in replaced_file_link_to_s3:
#         link_string = link.get_attribute("href")
#         file_name = link_string.split('/')[-1]
#         suffix_name = file_name.split('.')[-1]
#         if suffix_name == 'pdf':
#             file_type = 'application/pdf'
#         elif suffix_name == 'xls':
#             file_type == 'application/x-xls'
#         else:
#             file_type = 'image/png'
#         return_value = api_s3.get_gov_file_and_upload_to_s3(link_string, file_name, file_type)
#         print(return_value)






    # print(len(child_elements))
    # for item in child_elements:
    #     print(item.text)
        # try:
        #     a = item.find_element_by_tag_name('p')
        #     print(a.text)
        # except exceptions.NoSuchElementException as err:
        #     a = item.find_element_by_tag_name('table')
        #     print(a)


        # if hasattr(item, 'p'):
        #     print(item.text)
        # elif hasattr(item, 'table'):
        #     print(item.find_element_by_xpath('/table[2]/tbody/tr[1]/td[2]/p'))
        # else:
        #     pass

    # response = requests.get(href, headers=header_data)
    # bs_response = BS(response.content, 'lxml')
    # print(bs_response)
    # test = bs_response.find('div', class_='header')


# class PbcGovNews(BaseScraper):
#     def __init__(self, domain_url, media, lang):
#
#         self.headers = {
#             'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
#             'Accept-Encoding': 'gzip, deflate',
#             'Accept-Language': 'en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7',
#             'Cache-Control': 'max-age=0',
#             'Cookie': 'ccpassport=8cd3491a66ee9726ab4d427f37b6305c; wzwsconfirm=e446596f10d061196f4dd32a49abfa22; wzwstemplate=OA==; wzwschallenge=-1; wzwsvtime=1550217355; wzws_cid=c0bd8da249455c91e208f41329ee734e8d9e53795e3f53de37eecf8c2fc19d20def81f88f7a2e40148204dbb0670110d7e2025be33e355c906c4b880eba06b83',
#             'Connection': 'keep-alive',
#             'Host': 'www.pbc.gov.cn',
#             'If-Modified-Since': 'Fri, 15 Feb 2019 06:58:58 GMT',
#             'If-None-Match': 'W/"118ba-8ad8-581e94f19b080"',
#             'Upgrade-Insecure-Requests': '1',
#             'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
#             'Referer': 'http://www.pbc.gov.cn/diaochatongjisi/116219/116225/index.html'
#         }
#         super(PbcGovNews, self).__init__(domain_url, media, lang)
#
#     def response_data(self):
#
#         page = 1
#         while page <= 2:
#
#             if page == 1:
#                 web_news_url = self.domain_url + '.html'
#             else:
#                 web_news_url = self.domain_url + str(page) + '.html'
#             bs_response = self.extract_content_by_bs_header(web_news_url, self.headers)
#             print(bs_response)
#             all_article_list = bs_response.findAll('table')
#             for item in all_article_list:
#                 print(item.find('tbody').find('tr').find('td', height="22").find('font').find('a')['href'])
#                 return ''
#             page += 1






