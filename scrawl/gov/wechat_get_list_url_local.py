import sys
import os
import base64
import time
import uuid
import settings
from scrawl.news.base import BaseScraper
from selenium import webdriver
from scrawl.zhihu.yundama_1 import YDMHTTP
from PIL import Image


def main():
    chromedriver = "../../driver_binary_files/chromedriver_mac"
    browser = webdriver.Chrome(chromedriver)
    browser.get('https://weixin.sogou.com/')
    url_lst = []
    with open('wechat_list_name.txt', 'r') as f:
        name_lst = f.readlines()
    for name in name_lst:
        browser.find_element_by_id('query').send_keys(name.split('\n'))
        browser.find_element_by_class_name('swz2').click()
        try:
            browser.find_element_by_xpath('//*[@uigs="account_name_0"]').click()
            windows = browser.window_handles
            browser.switch_to_window(windows[1])
            href = browser.current_url
            url_lst.append(href + '\n')
            time.sleep(1)
            print('try', href)
        except:
            img_base = browser.get_screenshot_as_base64()
            imgdata = base64.b64decode(img_base)
            file = open('weixin_google_list.png', 'wb')
            file.write(imgdata)
            file.close()
            im = Image.open(r"weixin_google_list.png")
            login_img_area = browser.find_element_by_id("seccodeImage")
            x = int(login_img_area.location["x"]) - 50
            y = int(login_img_area.location["y"]) - 50
            im.crop((x, y, x + 300, y + 300)).save(r"vcode_list.png")
            result = YDMHTTP.identify(r"vcode_list.png")
            browser.find_element_by_id("seccodeInput").send_keys(result[1])
            browser.find_element_by_id('submit').click()
            img_base = browser.get_screenshot_as_base64()
            imgdata = base64.b64decode(img_base)
            file = open('weixin_google_list.png', 'wb')
            file.write(imgdata)
            file.close()
            time.sleep(1)
            browser.find_element_by_xpath('//*[@uigs="account_name_0"]').click()
            windows = browser.window_handles
            browser.switch_to_window(windows[1])
            href = browser.current_url
            url_lst.append(href + '\n')
            time.sleep(10)
            print('11111111')
            print('except', href)
        browser.close()
        browser.switch_to_window(windows[0])

        browser.get('https://weixin.sogou.com/')
    print(url_lst)
    with open('wechat_list_url.txt', 'w') as f_1:
        f_1.writelines(url_lst)
    # self.browser.quit()


print(sys.path)
main()
