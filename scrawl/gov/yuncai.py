"""
# run the code every 2 day(9:00 am), now we just scrape first page of the website

URL： https://www.yuncaijing.com/tag/id_5.html


"""

from scrawl.news.base import BaseScraper
import datetime as dt
from datetime import datetime, timezone, timedelta
import time
import calendar


class ScrapeYunCai(BaseScraper):
    def __init__(self, domain_url, media, lang):

        self.headers = {
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7',
            'cache-control': 'max-age=0',
            'Connection': 'keep-alive',
            'upgrade-insecure-requests': '1',
            'user-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36(KHTML, like Gecko) '
                          'Chrome/71.0.3578.98 Safari/537.36',
            'cookie': 'ycj_wafsid=wafsid_8744a9c5bc60c80de553707b1dd08dba; ycj_uuid=ae0ae1211a9f828169deec89e65f06e8; ycj_locate=aHR0cHM6Ly93d3cueXVuY2FpamluZy5jb20vdGFnL2lkXzUuaHRtbA%3D%3D; '
                      'YUNSESSID=bkb9dnljfpcsfg8d2ojcncaqp0; Qs_lvt_168612=1550197447; '
                      'Hm_lvt_b68ec780c488edc31b70f5dadf4e94f8=1550197451; PHPSESSID=ra6tj52mokl75dooh5p8tmt6o4; ycj_from_url=aHR0cHM6Ly93d3cueXVuY2FpamluZy5jb20vdGFnL2lkXzUuaHRtbA%3D%3D; Qs_pv_168612=2011882518687037200%2C869288495403718000%2C3986164387568982500%2C2532128546197350000%2C920270624766891400; Hm_lpvt_b68ec780c488edc31b70f5dadf4e94f8=1550201828',
            'host': 'www.yuncaijing.com',
        }
        self.year = str(dt.date.today())[0:4]
        self.now_time = datetime.now()
        super(ScrapeYunCai, self).__init__(domain_url, media, lang)

    def response_data(self, start_date):

        bs_response = super(ScrapeYunCai, self).extract_content_by_bs_header('https://www.yuncaijing.com/tag/id_5.html',
                                                                          self.headers)
        data_amount = bs_response.find('section', class_='main fl').find('ul').findAll('li')
        if start_date:
            start_date_format = dt.datetime.strptime(start_date, "%Y-%m-%d")
        for item in data_amount:
            dict_item = {}
            item_date_str = item.find('div', class_='time').find('time').getText()
            if len(item_date_str) == 5:
                item_date_format = dt.datetime.strptime(item_date_str + ' ' + str(dt.date.today()),
                                                        "%H:%M %Y-%m-%d")
            elif len(item_date_str) == 11:
                item_date_format = dt.datetime.strptime(item_date_str + '-' + self.year, "%H:%M %m-%d-%Y")
            else:
                item_date_format = dt.datetime.strptime(item_date_str, "%H:%M %Y-%m-%d")
            if start_date:
                if item_date_format > start_date_format:
                    dict_item['title'] = item.find('a', class_='title').attrs['title']
                    dict_item['content'] = item.find('span', class_='des').getText()
                    timestamp1 = calendar.timegm(item_date_format.timetuple())
                    dict_item['url'] = self.domain_url + '#' + str(timestamp1)
                    dict_item['language'] = 'cn'
                    dict_item['is_added_to_source'] = False
                    dict_item['media'] = self.media
                    dict_item['published_at'] = item_date_format.strftime("%Y-%m-%d %H:%M:%S")
                    self.result_json.append(dict_item)
                else:
                    return self.result_json
            else:
                if len(item_date_str) == 11:  # today's data date length is 11
                    dict_item['title'] = item.find('a', class_='title').attrs['title']
                    dict_item['content'] = item.find('span', class_='des').getText()
                    dict_item['url'] = self.domain_url
                    dict_item['language'] = 'cn'
                    dict_item['is_added_to_source'] = False
                    dict_item['media'] = self.media
                    dict_item['published_at'] = item_date_format.strftime("%Y-%m-%d %H:%M:%S")
                    self.result_json.append(dict_item)
                else:
                    return self.result_json
        # if the data in today is too much, some data will be missing ,changed it in the future
        return self.result_json

        # rp_id = self.read_id_to_text()
        # for item in json_obj['data']['items']:
        #     dict_item = {}
        #     if item['id'] >= int(rp_id):
        #         dict_item['title'] = item['title']
        #         dict_item['content'] = item['description']
        #         dict_item['published_at'] = item['published_at']
        #         dict_item['url'] = item['news_url']
        #         dict_item['language'] = 'cn'
        #         dict_item['is_added_to_source'] = False
        #         dict_item['media'] = '36kr'
        #         self.result_json.append(dict_item)
        #     else:
        #         max_rp_id = json_obj['data']['items'][0]['id']
        #         self.write_id_to_text(str(max_rp_id))
        #         return self.result_json
        # return self.result_json


if __name__ == "__main__":
    test = ScrapeYunCai()
    a = test.response_data()

