import sys
import os

this_file_path = os.path.abspath('.')
abs_path = os.path.join(this_file_path, '../', '../')
sys.path.append(abs_path)
import base64
import time
import uuid
import settings
from scrawl.news.base import BaseScraper
from selenium import webdriver
from scrawl.zhihu.yundama_1 import YDMHTTP
from PIL import Image


class ScrapeWeiChat_list(BaseScraper):
    headers = {'Accept': '*/*',
               'Encoding': 'gzip, deflate, br',
               'Language': 'zh-CN,zh;q=0.9',
               'Connection': 'keep-alive',
               'Cookie': 'CXID=87C4B04070D96B96DA45D758CC96969F; SUID=8578E7653865860A5AC2C21F00038887; SUV=008D'
                         '437027B43D015B13F7BD398E5453; dt_ssuid=3281138552; pex=C864C03270DED3DD8A06887A372DA21'
                         '9231FFAC25A9D64AE09E82AED12E416AC; ssuid=1070970760; IPLOC=CN3100; ad=4IRkvlllll2txMHQ'
                         'lllllVZ4fCYlllllBWNcPkllll9lllllxv7ll5@@@@@@@@@@; ABTEST=0|1546483965|v1; weixinIndexV'
                         'isited=1; JSESSIONID=aaaEYHTxDr_7BbOV3gdDw',
               'Host': 'weixin.sogou.com',
               'Referer': 'https://weixin.sogou.com/',
               'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome'
                             '/71.0.3578.98 Safari/537.36',
               'X-Requested-With': 'XMLHttpRequest'}

    _tmp_folder = '/tmp/{}'.format(uuid.uuid4())
    if not os.path.exists(_tmp_folder):
        os.makedirs(_tmp_folder)

    if not os.path.exists(_tmp_folder + '/user-data'):
        os.makedirs(_tmp_folder + '/user-data')

    if not os.path.exists(_tmp_folder + '/data-path'):
        os.makedirs(_tmp_folder + '/data-path')

    if not os.path.exists(_tmp_folder + '/cache-dir'):
        os.makedirs(_tmp_folder + '/cache-dir')

    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-gpu')
    chrome_options.add_argument('--window-size=1280x1696')
    chrome_options.add_argument('--user-data-dir={}'.format(_tmp_folder + '/user-data'))
    chrome_options.add_argument('--hide-scrollbars')
    chrome_options.add_argument('--enable-logging')
    chrome_options.add_argument('--log-level=0')
    chrome_options.add_argument('--v=99')
    chrome_options.add_argument('--single-process')
    chrome_options.add_argument('--data-path={}'.format(_tmp_folder + '/data-path'))
    chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument('--homedir={}'.format(_tmp_folder))
    chrome_options.add_argument('--disk-cache-dir={}'.format(_tmp_folder + '/cache-dir'))
    chrome_options.add_argument(
        'user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
        'Chrome/61.0.3163.100 Safari/537.36')
    prefs = {"download.default_directory": "/var/codes/intellifeed-kol"}
    # chrome_options.add_experimental_option("prefs", prefs)
    # driver = webdriver.Chrome(chrome_options=chrome_options)
    chrome_options.binary_location = settings.DRIVER_PATH_CHROME
    browser = webdriver.Chrome(chrome_options=chrome_options)

    def __init__(self, domain_url, media, lang, website=None):
        super(ScrapeWeiChat_list, self).__init__(domain_url, media, lang)

    def main(self):
        self.browser.get('https://weixin.sogou.com/')
        url_lst = []
        with open('wechat_list_name.txt', 'r') as f:
            name_lst = f.readlines()
        for name in name_lst:
            self.browser.find_element_by_id('query').send_keys(name.split('\n'))
            try:
                self.browser.find_element_by_class_name('swz2').click()
            except:
                print('click error sleeping~~')
                time.sleep(60)
                self.browser.find_element_by_class_name('swz2').click()
            try:
                self.browser.find_element_by_xpath('//*[@uigs="account_name_0"]').click()
                windows = self.browser.window_handles
                self.browser.switch_to_window(windows[1])
                href = self.browser.current_url
                url_lst.append(href + '\n')
                print('try', href)
            except:
                img_base = self.browser.get_screenshot_as_base64()
                imgdata = base64.b64decode(img_base)
                file = open('weixin_google_list.png', 'wb')
                file.write(imgdata)
                file.close()
                im = Image.open(r"weixin_google_list.png")
                login_img_area = self.browser.find_element_by_id("seccodeImage")
                x = int(login_img_area.location["x"]) - 50
                y = int(login_img_area.location["y"]) - 50
                im.crop((x, y, x + 300, y + 300)).save(r"vcode_list.png")
                result = YDMHTTP.identify(r"vcode_list.png")
                self.browser.find_element_by_id("seccodeInput").send_keys(result[1])
                self.browser.find_element_by_id('submit').click()
                time.sleep(5)
                self.browser.find_element_by_xpath('//*[@uigs="account_name_0"]').click()
                windows = self.browser.window_handles
                self.browser.switch_to_window(windows[1])
                href = self.browser.current_url
                url_lst.append(href + '\n')
                print('except', href)
            self.browser.close()
            self.browser.switch_to_window(windows[0])
            self.browser.get('https://weixin.sogou.com/')
            time.sleep(10)
        print(url_lst)
        with open('wechat_list_url.txt', 'w') as f_1:
            f_1.writelines(url_lst)
        # self.browser.quit()
