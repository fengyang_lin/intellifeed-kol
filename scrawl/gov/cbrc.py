"""
# run the code every 2 day(9:00 am), now we just scrape first page of the website

URL： http://www.cbrc.gov.cn/chinese/newListDoc/111002/1.html
some Tips:
  1. Now just scrawl the first page, because the website update so slow
  2. sometimes, it's hard to upload file to Aws s3. You can run the code again

"""

from scrawl.news.base import BaseScraper
import settings
import requests
import time
from selenium import webdriver
from newspaper import Article
from bs4 import BeautifulSoup as BS
from selenium.common import exceptions
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
from common import api
from urllib.parse import quote, unquote
from readability.readability import Document
import urllib.request as urllib2
import uuid
import os


class CbrcGovNews(BaseScraper):
    _tmp_folder = '/tmp/{}'.format(uuid.uuid4())
    if not os.path.exists(_tmp_folder):
        os.makedirs(_tmp_folder)

    if not os.path.exists(_tmp_folder + '/user-data'):
        os.makedirs(_tmp_folder + '/user-data')

    if not os.path.exists(_tmp_folder + '/data-path'):
        os.makedirs(_tmp_folder + '/data-path')

    if not os.path.exists(_tmp_folder + '/cache-dir'):
        os.makedirs(_tmp_folder + '/cache-dir')


    # for linux
    chrome_options = webdriver.ChromeOptions()

    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--window-size=1280,1000')
    chrome_options.add_argument('--allow-running-insecure-content')
    chrome_options.add_argument('--user-data-dir={}'.format(_tmp_folder + '/user-data'))
    chrome_options.add_argument('--hide-scrollbars')
    chrome_options.add_argument('--enable-logging')
    chrome_options.add_argument('--log-level=0')
    chrome_options.add_argument('--v=99')
    chrome_options.add_argument('--single-process')
    chrome_options.add_argument('--data-path={}'.format(_tmp_folder + '/data-path'))
    chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument('--homedir={}'.format(_tmp_folder))
    chrome_options.add_argument('--disk-cache-dir={}'.format(_tmp_folder + '/cache-dir'))
    chrome_options.add_argument('user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 '
                                '(KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36')

    chrome_options.binary_location = settings.DRIVER_PATH_CHROME
    browser = webdriver.Chrome(chrome_options=chrome_options)

    detail_browser = webdriver.Chrome(chrome_options=chrome_options)

    def __init__(self, domain_url, media, lang):

        super(CbrcGovNews, self).__init__(domain_url, media, lang)

    def response_data(self):
        self.browser.set_window_position(200, 200)
        self.browser.get(self.domain_url)
        time.sleep(3)
        title_and_href_list = self.browser.find_element_by_id('testUI').find_elements_by_class_name('cc')
        for item in range(1, 16):
            title = title_and_href_list[item-1].text
            index_for_pub = '//*[@id="testUI"]/tbody/tr[' + str(item + 1) + ']/td[2]'
            pub_time = self.browser.find_element_by_xpath(index_for_pub).text.strip(" ")
            detail_link_url = self.browser.find_elements_by_xpath('//a[contains(@href, "newShouDoc")]')[item-1].get_attribute("href")
            self.detail_browser.get(detail_link_url)
            # some structure of html pages is different
            try:
                content = self.detail_browser.find_element_by_class_name(" f12c").text
            except NoSuchElementException:
                try:
                    content = self.detail_browser.find_element_by_class_name('Section0').text
                except NoSuchElementException:
                    content = ''
            if content:
                content_info = dict()
                content_info['url'] = detail_link_url
                content_info['title'] = title
                content_info['content'] = content
                content_info['published_at'] = pub_time.strip()
                content_info['types'] = "gov"
                content_info['language'] = 'cn'
                content_info['is_added_to_source'] = False
                content_info['media'] = self.media
                self.result_json.append(content_info)
            else:
                print('the page is broken : ' + detail_link_url)
        return self.result_json



        # '//*[@id="testUI"]/tbody/tr[2]/td[2]'
        # '//*[@id="testUI"]/tbody/tr[5]/td[2]'
        # '//*[@id="testUI"]/tbody/tr[2]/td[1]'
        # pub_time_list = selector.find('table', id='testUI').findAll('td', colspan='2')
        # selector = selector.find('table', id='testUI').findAll('td', class_='cc')
        # pub_time_list = selector.find('table', id='testUI').findAll('td', colspan='2')
        # for item in range(1, 16):
        #     title = title_and_href_list[item-1].find('a')['title']
        #     pub_time = pub_time_list[item-1].text.strip(" ")
        #     url = title_and_href_list[item-1].find('a')['href']
        #     detail_link_url = 'http://www.cbrc.gov.cn' + url
        #     response = requests.get(detail_link_url, headers=self.detail_header)
        #     content = Document(response.content).summary()
        #     if content:
        #         content_info = dict()
        #         content_info['url'] = detail_link_url
        #         content_info['title'] = title
        #         content_info['content'] = content
        #         content_info['published_at'] = pub_time.strip()
        #         content_info['types'] = "gov"
        #         content_info['language'] = 'cn'
        #         content_info['is_added_to_source'] = False
        #         content_info['media'] = self.media
        #         self.result_json.append(content_info)
        # print(self.result_json)


# class CbrcGovNews(BaseScraper):
#
#
#     item_1_xpath = '//*[@id="testUI"]/tbody/tr['
#     item_2_xpath = ']/td[1]/a'  # href url link
#     item_3_xpath = ']'  # pub time
#     api_s3 = api.IntellifeedAPICaller()
#
#     def __init__(self, domain_url, media, lang):
#         super(CbrcGovNews, self).__init__(domain_url, media, lang)
#
#     def response_data(self):
#
#
#         selector = self.extract_content_by_bs_header_for_cbrc(self.domain_url, self.header)
#         title_and_href_list = selector.find('table', id='testUI').findAll('td', class_='cc')
#         pub_time_list = selector.find('table', id='testUI').findAll('td', colspan='2')
#         for item in range(1, 16):
#             title = title_and_href_list[item-1].find('a')['title']
#             pub_time = pub_time_list[item-1].text.strip(" ")
#             url = title_and_href_list[item-1].find('a')['href']
#             detail_link_url = 'http://www.cbrc.gov.cn' + url
#             response = requests.get(detail_link_url, headers=self.detail_header)
#             content = Document(response.content).summary()
#             if content:
#                 content_info = dict()
#                 content_info['url'] = detail_link_url
#                 content_info['title'] = title
#                 content_info['content'] = content
#                 content_info['published_at'] = pub_time.strip()
#                 content_info['types'] = "gov"
#                 content_info['language'] = 'cn'
#                 content_info['is_added_to_source'] = False
#                 content_info['media'] = self.media
#                 self.result_json.append(content_info)
            # if isinstance(pub_time[item-1], str):
            #     pub_time = pub_time[item - 1]
            # else:
            #     pub_time = pub_time[item - 1].text
            # print(pub_time)
            # print(title)

            # pub_time = selector.find('table', id='testUI').findAll('td', height='cc')[item-1].find('a')['href']
            # url_link = selector.find('table', id='testUI').findAll('td', class_='cc')[item-1].find('a')['title']
            # detail_link_url = 'http://www.csrc.gov.cn/pub/newsite/zjhxwfb/xwdd' + url_link[1:]
            # response = requests.get(detail_link_url)
            # content = Document(response.content).summary()
            # if content:
            #     content_info = dict()
            #     content_info['url'] = url_link
            #     content_info['title'] = title
            #     content_info['content'] = content
            #     content_info['published_at'] = pub_time
            #     content_info['types'] = "gov"
            #     content_info['language'] = 'cn'
            #     content_info['is_added_to_source'] = False
            #     content_info['media'] = self.media
            #     self.result_json.append(content_info)
        # return self.result_json



#
#
# def pbc_gov_news():
#     browser = webdriver.Chrome(settings.DRIVER_PATH_CHROME)
#     browser_detail = webdriver.Chrome(settings.DRIVER_PATH_CHROME)
#     browser.set_window_position(200, 300)  # make sure the screen doesn't in the left
#     result_list = []
#     api_s3 = api.IntellifeedAPICaller()
#     for item in range(1, 21):
#         browser.get('http://www.pbc.gov.cn/diaochatongjisi/116219/116225/index.html')
#         item_obj = browser.find_element_by_xpath(
#             '//*[@id="11871"]/div[2]/div[1]/table/tbody/tr[2]/td/table[' + str(item) +']/tbody/tr/td[2]/font/a')
#         title = item_obj.text
#         href = item_obj.get_attribute("href")
#         pub_time = browser.find_element_by_xpath('//*[@id="11871"]/div[2]/div[1]/table/tbody/tr[2]/td/table[' + str(item) + ']/tbody/tr/td[2]/span').text
#         browser_detail.get(href)
#         content = browser_detail.find_element_by_id('zoom').get_attribute('innerHTML')
#         replaced_file_link_to_s3 = browser_detail.find_element_by_id('zoom').find_elements_by_css_selector('a[href]')
#         replaced_img_link_to_s3 = browser_detail.find_element_by_id('zoom').find_elements_by_css_selector('img[src]')
#
#         for sub_item in replaced_file_link_to_s3:
#             link_string = sub_item.getAttribute("href")
#             file_name = link_string.split('/')[-1]   # in the article is the part of URL
#             suffix_name = file_name.split('.')[-1]
#             if suffix_name == 'pdf':
#                 file_type = 'application/pdf'
#             elif suffix_name == 'xls':
#                 file_type == 'application/vnd.ms-excel'
#             else:
#                 file_type = 'image/png'
#             return_value = api_s3.get_gov_file_and_upload_to_s3(link_string, file_name, suffix_name, file_type)
#             if return_value:
#                 replaced_content = content.replace(link_string_in_article, return_value)
#             else:
#                 replaced_content = ''
#                 break # the img failed to upload to s3 , we will abandon the article
#
#         for sub_img_item in replaced_img_link_to_s3:
#             link_string = sub_img_item.get_attribute("src")
#             # in the article is the part of URL
#             decode_url = unquote(link_string, 'utf-8')
#             link_string_in_article = decode_url.replace('http://www.pbc.gov.cn', '')
#             file_name = decode_url.split('/')[-1]
#             suffix_name = file_name.split('.')[-1]
#             file_type = 'image/png'
#             return_value = api_s3.get_gov_file_and_upload_to_s3(decode_url, file_name, suffix_name, file_type)
#             if return_value:
#                 replaced_content = content.replace(link_string_in_article, return_value)
#             else:
#                 replaced_content = ''
#                 break # the img failed to upload to s3 , we will abandon the article
#         # send data to S3
#         if replaced_content:
#             content_info = dict()
#             content_info['source_website_name'] = "pbc"
#             content_info['source_domain_url'] = "http://www.pbc.gov.cn"
#             content_info['source_full_destination_url'] = href
#             content_info['title_cn'] = title
#             content_info['content_cn'] = replaced_content
#             content_info['publish_time'] = pub_time
#             content_info['types'] = "gov"
#             result_list.append(content_info)

# def tets_pbc_gov_news():
#     api_s3 = api.IntellifeedAPICaller()
#     test_url = 'http://www.pbc.gov.cn/diaochatongjisi/116219/116225/index.html'
#     detail_page_url = '//*[@id="11871"]/div[2]/div[1]/table/tbody/tr[2]/td/table[5]/tbody/tr/td[2]/font/a'
#     browser = webdriver.Chrome(settings.DRIVER_PATH_CHROME)
#     browser.set_window_position(200, 300)
#     browser.get(test_url)
#     href = browser.find_element_by_xpath(detail_page_url).get_attribute("href")
#     browser.get(href)
#     content = browser.find_element_by_id('zoom').get_attribute('innerHTML')
#     replaced_file_link_to_s3 = browser.find_element_by_id('zoom').find_elements_by_css_selector('a[href]')
#      # find all the
#     for link in replaced_file_link_to_s3:
#         link_string = link.get_attribute("href")
#         file_name = link_string.split('/')[-1]
#         suffix_name = file_name.split('.')[-1]
#         if suffix_name == 'pdf':
#             file_type = 'application/pdf'
#         elif suffix_name == 'xls':
#             file_type == 'application/x-xls'
#         else:
#             file_type = 'image/png'
#         return_value = api_s3.get_gov_file_and_upload_to_s3(link_string, file_name, file_type)
#         print(return_value)






    # print(len(child_elements))
    # for item in child_elements:
    #     print(item.text)
        # try:
        #     a = item.find_element_by_tag_name('p')
        #     print(a.text)
        # except exceptions.NoSuchElementException as err:
        #     a = item.find_element_by_tag_name('table')
        #     print(a)


        # if hasattr(item, 'p'):
        #     print(item.text)
        # elif hasattr(item, 'table'):
        #     print(item.find_element_by_xpath('/table[2]/tbody/tr[1]/td[2]/p'))
        # else:
        #     pass

    # response = requests.get(href, headers=header_data)
    # bs_response = BS(response.content, 'lxml')
    # print(bs_response)
    # test = bs_response.find('div', class_='header')


# class PbcGovNews(BaseScraper):
#     def __init__(self, domain_url, media, lang):
#
#         self.headers = {
#             'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
#             'Accept-Encoding': 'gzip, deflate',
#             'Accept-Language': 'en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7',
#             'Cache-Control': 'max-age=0',
#             'Cookie': 'ccpassport=8cd3491a66ee9726ab4d427f37b6305c; wzwsconfirm=e446596f10d061196f4dd32a49abfa22; wzwstemplate=OA==; wzwschallenge=-1; wzwsvtime=1550217355; wzws_cid=c0bd8da249455c91e208f41329ee734e8d9e53795e3f53de37eecf8c2fc19d20def81f88f7a2e40148204dbb0670110d7e2025be33e355c906c4b880eba06b83',
#             'Connection': 'keep-alive',
#             'Host': 'www.pbc.gov.cn',
#             'If-Modified-Since': 'Fri, 15 Feb 2019 06:58:58 GMT',
#             'If-None-Match': 'W/"118ba-8ad8-581e94f19b080"',
#             'Upgrade-Insecure-Requests': '1',
#             'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
#             'Referer': 'http://www.pbc.gov.cn/diaochatongjisi/116219/116225/index.html'
#         }
#         super(PbcGovNews, self).__init__(domain_url, media, lang)
#
#     def response_data(self):
#
#         page = 1
#         while page <= 2:
#
#             if page == 1:
#                 web_news_url = self.domain_url + '.html'
#             else:
#                 web_news_url = self.domain_url + str(page) + '.html'
#             bs_response = self.extract_content_by_bs_header(web_news_url, self.headers)
#             print(bs_response)
#             all_article_list = bs_response.findAll('table')
#             for item in all_article_list:
#                 print(item.find('tbody').find('tr').find('td', height="22").find('font').find('a')['href'])
#                 return ''
#             page += 1






