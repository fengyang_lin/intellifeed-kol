#  -*-coding:utf8 -*-

import sys
import os

# this_file_path = os.path.abspath('.')
# abs_path = os.path.join(this_file_path, '../', '../')
# sys.path.append(abs_path)


import json
import time
import requests
from retrying import retry
from bs4 import BeautifulSoup as BS
from dateutil.parser import parse
from newspaper import Article
from selenium import webdriver
from PIL import Image
from scrawl.zhihu.yundama import YDMHTTP
import common.common
from common.api import IntellifeedAPICaller
import settings
import uuid
from scrawl.news.base import BaseScraper
from common import api
import base64
from translate import Translator


def web_driver(url):
    _tmp_folder = '/tmp/{}'.format(uuid.uuid4())
    if not os.path.exists(_tmp_folder):
        os.makedirs(_tmp_folder)

    if not os.path.exists(_tmp_folder + '/user-data'):
        os.makedirs(_tmp_folder + '/user-data')

    if not os.path.exists(_tmp_folder + '/data-path'):
        os.makedirs(_tmp_folder + '/data-path')

    if not os.path.exists(_tmp_folder + '/cache-dir'):
        os.makedirs(_tmp_folder + '/cache-dir')

    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-gpu')
    chrome_options.add_argument('--window-size=1280x1696')
    chrome_options.add_argument('--user-data-dir={}'.format(_tmp_folder + '/user-data'))
    chrome_options.add_argument('--hide-scrollbars')
    chrome_options.add_argument('--enable-logging')
    chrome_options.add_argument('--log-level=0')
    chrome_options.add_argument('--v=99')
    chrome_options.add_argument('--single-process')
    chrome_options.add_argument('--data-path={}'.format(_tmp_folder + '/data-path'))
    chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument('--homedir={}'.format(_tmp_folder))
    chrome_options.add_argument('--disk-cache-dir={}'.format(_tmp_folder + '/cache-dir'))
    chrome_options.add_argument(
        'user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
        'Chrome/61.0.3163.100 Safari/537.36')
    prefs = {"download.default_directory": "/var/codes/intellifeed-kol"}
    # chrome_options.add_experimental_option("prefs", prefs)
    # driver = webdriver.Chrome(chrome_options=chrome_options)
    chrome_options.binary_location = settings.DRIVER_PATH_CHROME
    browser = webdriver.Chrome(chrome_options=chrome_options)

    browser.set_window_position(200, 200)
    browser.get(url)
    return browser


class ScrapeWeiChat(BaseScraper):
    headers = {'Accept': '*/*',
               'Encoding': 'gzip, deflate, br',
               'Language': 'zh-CN,zh;q=0.9',
               'Connection': 'keep-alive',
               'Cookie': 'CXID=87C4B04070D96B96DA45D758CC96969F; SUID=8578E7653865860A5AC2C21F00038887; SUV=008D'
                         '437027B43D015B13F7BD398E5453; dt_ssuid=3281138552; pex=C864C03270DED3DD8A06887A372DA21'
                         '9231FFAC25A9D64AE09E82AED12E416AC; ssuid=1070970760; IPLOC=CN3100; ad=4IRkvlllll2txMHQ'
                         'lllllVZ4fCYlllllBWNcPkllll9lllllxv7ll5@@@@@@@@@@; ABTEST=0|1546483965|v1; weixinIndexV'
                         'isited=1; JSESSIONID=aaaEYHTxDr_7BbOV3gdDw',
               'Host': 'weixin.sogou.com',
               'Referer': 'https://weixin.sogou.com/',
               'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome'
                             '/71.0.3578.98 Safari/537.36',
               'X-Requested-With': 'XMLHttpRequest'}

    _tmp_folder = '/tmp/{}'.format(uuid.uuid4())
    if not os.path.exists(_tmp_folder):
        os.makedirs(_tmp_folder)

    if not os.path.exists(_tmp_folder + '/user-data'):
        os.makedirs(_tmp_folder + '/user-data')

    if not os.path.exists(_tmp_folder + '/data-path'):
        os.makedirs(_tmp_folder + '/data-path')

    if not os.path.exists(_tmp_folder + '/cache-dir'):
        os.makedirs(_tmp_folder + '/cache-dir')

    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-gpu')
    chrome_options.add_argument('--window-size=1280x1696')
    chrome_options.add_argument('--user-data-dir={}'.format(_tmp_folder + '/user-data'))
    chrome_options.add_argument('--hide-scrollbars')
    chrome_options.add_argument('--enable-logging')
    chrome_options.add_argument('--log-level=0')
    chrome_options.add_argument('--v=99')
    chrome_options.add_argument('--single-process')
    chrome_options.add_argument('--data-path={}'.format(_tmp_folder + '/data-path'))
    chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument('--homedir={}'.format(_tmp_folder))
    chrome_options.add_argument('--disk-cache-dir={}'.format(_tmp_folder + '/cache-dir'))
    chrome_options.add_argument(
        'user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
        'Chrome/61.0.3163.100 Safari/537.36')
    prefs = {"download.default_directory": "/var/codes/intellifeed-kol"}
    # chrome_options.add_experimental_option("prefs", prefs)
    # driver = webdriver.Chrome(chrome_options=chrome_options)
    chrome_options.binary_location = settings.DRIVER_PATH_CHROME
    browser = webdriver.Chrome(chrome_options=chrome_options)

    def __init__(self, domain_url, media, lang, website=None):

        super(ScrapeWeiChat, self).__init__(domain_url, media, lang)
        self.aimed_website = website
        self.result_json = []

    # @retry(stop_max_attempt_number=3, wait_fixed=60000)
    def get_content_in_detail_page(self, url):
        news = Article(url, language='zh')
        try:
            news.download()
        except:
            print('news.download Error sleep')
            time.sleep(1200)
            news.download()
        try:
            news.parse()
        except:
            print('news.parse Error sleep')
            time.sleep(1200)
            news.parse()
        return news.text

    def caijingmi(self):
        idx = 0
        acct_links = dict()
        self.result_json = []
        while True:
            if idx == 0:
                link = 'https://weixin.sogou.com/pcindex/pc/pc_6/pc_6.html'
            else:
                link = 'https://weixin.sogou.com/pcindex/pc/pc_6/' + str(idx) + '.html'
            response = requests.get(link, headers=self.headers)
            response.encoding = response.apparent_encoding
            bs_test = BS(response.text, 'lxml')
            if bs_test.find('title') is not None:
                return self.result_json
            news_list = bs_test.find_all('div', class_='txt-box')
            print('*****NEWS LIST*****')
            for news in news_list:
                dict_item = dict()
                dict_item['source_website_name'] = 'Wechat'
                dict_item['source_domain_url'] = 'https://weixin.sogou.com/'
                # dict_item['source_full_destination_url'] = news.find('h3').a.attrs['href']
                dict_item['title_cn'] = news.find('h3').a.string
                # print(dict_item['title_cn'])
                # dict_item['content_cn'] = self.get_content_in_detail_page(dict_item['source_full_destination_url'])
                timestramp = news.find('span').attrs['t']
                dict_item['publish_time'] = time.strftime("%Y-%m-%d %H:%M", time.localtime(int(timestramp)))
                #    if datetime.now() - dict_item['publish_time'] > timedelta(days=1):
                #        return self.result_json, acct_links
                dict_item['media'] = news.find('a', class_='account').string
                dict_item['types'] = 'social_media'
                acct_link = news.find('a', class_='account').attrs['href']
                acct_links[dict_item['media']] = acct_link
                dict_item['kol_id'], newslist = self.upload_profile(acct_link)
                if newslist == 0:
                    continue
                content_info_dict = {'content_info': dict_item}
                # print(content_info_dict)
                # api_instance = IntellifeedAPICaller()
                # ret = api_instance.new_content(content_info_dict)
                # print(str(ret.status_code))
                # print(ret.text)

                news_in_acct = self.wechat_account(dict_item['media'], newslist, dict_item['kol_id'])
                print('idx', idx)
                self.result_json.append(content_info_dict)
                self.result_json.extend(news_in_acct)
            idx += 1
            if idx == 12:
                self.browser.quit()
                return 0
        # return self.result_json

    # @retry(stop_max_attempt_number=3)
    def get_html(self, url):
        try:
            self.browser.get(url)
        except Exception as err:
            print('browser.get(url) error : ' + str(err))
            time.sleep(1200)
            self.browser.get(url)
        time.sleep(1)
        self.browser.maximize_window()
        html = self.browser.find_element_by_xpath("//*").get_attribute("outerHTML")
        bs_test_1 = BS(html, 'lxml')
        news_list_1 = bs_test_1.find_all('div', class_='weui_media_box appmsg')
        if not news_list_1:  # check if empty
            # img_base = self.browser.get_screenshot_as_base64()
            # imgdata = base64.b64decode(img_base)
            # file = open('weixin_google.png', 'wb')
            # file.write(imgdata)
            # file.close()
            # print('save')
            # im = Image.open(r"weixin_google.png")
            self.browser.save_screenshot(r"weixin_google.png")
            im = Image.open(r"weixin_google.png")
            login_img_area = self.browser.find_element_by_id("verify_img")
            x = int(login_img_area.location["x"]) - 50
            y = int(login_img_area.location["y"]) - 50
            im.crop((x, y, x + 300, y + 300)).save(r"vcode.png")
            result = YDMHTTP.identify(r"vcode.png")
            self.browser.find_element_by_id("input").send_keys(result[1])
            self.browser.find_element_by_id('bt').click()
            time.sleep(3)
            html = self.browser.find_element_by_xpath("//*").get_attribute("outerHTML")
            print(result[1])
            bs_test_1 = BS(html, 'lxml')
            news_list_1 = bs_test_1.find_all('div', class_='weui_media_box appmsg')
            time.sleep(1)
        # self.upload_avatar(bs_test_1)
        # self.browser.quit()
        return bs_test_1, news_list_1

    # @retry(stop_max_attempt_number=3)
    def upload_profile(self, url):
        html, newslist = self.get_html(url)
        print(url)
        prodata = dict()
        prodata['real_name'] = ''
        try:
            prodata['nick_name'] = html.find('strong', class_='profile_nickname').string.strip()
        except Exception as err:
            print('find nick_name error :', err)
            return 0, 0
        prodata['gender'] = 'unknown'
        prodata['living_location'] = ''
        prodata['industry'] = ''
        prodata['career'] = ''
        prodata['email'] = ''
        prodata['education'] = ''
        prodata['weixin_info'] = {}
        prodata['is_weixin'] = 'True'
        prodata['original_url'] = url
        prodata['self_intro'] = ''
        prodata['achievement'] = ''
        prodata['followers'] = ''
        translator = Translator(from_lang="zh_CN", to_lang="en")
        result = translator.translate(prodata['nick_name'])
        prodata['nick_name'] = result
        try:
            prodata['weixin_info']['wechat_id'] = html.find('p', class_='profile_account').string[5:]
        except Exception as err:  # pylint: disable=unused-variable
            prodata['weixin_info']['wechat_id'] = None
        print(prodata['weixin_info']['wechat_id'])
        labels = html.find_all('li')
        # labels = html.find_all('label', class_='profile_desc_label')
        if labels:
            for label in labels:
                if label.find('label', class_='profile_desc_label').string == '功能介绍':
                    prodata['weixin_info']['function'] = label.find('div', class_='profile_desc_value').string
                if label.find('label', class_='profile_desc_label').string == '帐号主体':
                    prodata['weixin_info']['belong_to'] = label.find('div', class_='profile_desc_value').string
                    if prodata['weixin_info']['belong_to'] is None:
                        prodata['weixin_info']['belong_to'] = \
                            ''.join(label.find('div', class_='profile_desc_value').strings)

        img_url = html.find('span', class_='radius_avatar profile_avatar').find('img')['src']
        if prodata['weixin_info']['wechat_id'] is None:
            title = 'wxsogo_' + img_url.split('/')[-2] + '.jpg'
        else:
            title = 'wxsogo_' + prodata['weixin_info']['wechat_id'] + '.jpg'
        prodata['avatar'] = common.common.get_avatar_and_upload_to_s3(img_url, title)
        print(prodata)
        r = IntellifeedAPICaller()
        nick_name_dic = {'nick_name': prodata['nick_name']}
        try:
            status_text = r.check_kol_profile(nick_name_dic).json()
        except Exception as err:
            print('check_kol_profile Error', err)
            time.sleep(1200)
            status_text = r.check_kol_profile(nick_name_dic).json()
        if status_text == 'request success':
            response = r.new_kol_profile(prodata)
            print('***********')
            print('create success profile ', response.status_code)
            dict_result = response.content.decode('utf-8')
            print('#########dict_result\n')
            print(dict_result)
            json_result = json.loads(dict_result)
            print('############JSON\n')
            print(json_result)
            return json_result['id'], newslist
        else:
            print('no create profile ', status_text['id'])
            return status_text['id'], newslist

    # def upload_avatar(self, html):
    #     avatar_url = html.find('span',class_='radius_avatar profile_avatar').find('img')['src']

    def wechat_account(self, media, news_list, id_):
        print(media)

        results = []
        for news in news_list:
            dict_item = dict()
            dict_item['source_website_name'] = 'Wechat'
            dict_item['source_domain_url'] = 'https://weixin.sogou.com/'
            partial_url = news.find('h4', class_='weui_media_title').attrs['hrefs']

            if partial_url[0:23] == 'http://mp.weixin.qq.com':
                dict_item['source_full_destination_url'] = partial_url
            else:
                dict_item['source_full_destination_url'] = 'https://mp.weixin.qq.com' + partial_url

            dict_item['title_cn'] = news.find('h4', class_='weui_media_title').get_text().replace('\n', '').replace(
                '原创', '').strip(' ')
            print(dict_item['title_cn'])
            print(dict_item['source_full_destination_url'])
            dict_item['content_cn'] = self.get_content_in_detail_page(dict_item['source_full_destination_url'])
            # dict_item['publish_time'] = parse(news.find('p', class_='weui_media_extra_info').string,
            #                                   fuzzy=True).strftime("%Y-%m-%d %H:%M")
            dict_item['publish_time'] = news.find('p', class_='weui_media_extra_info').get_text().strip('原创').replace(
                '年', '-').replace('月', '-').replace('日', '').replace('/', '-') + ' 12:00'
            print(dict_item['publish_time'])
            # dict_item['publish_time'] = datetime.strptime(
            #    time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(timestramp))), "%Y-%m-%d %H:%M:%S")
            # dict_item['media'] = media
            dict_item['types'] = 'social_media'
            dict_item['kol_id'] = id_
            content_info_dict = {'content_info': dict_item}
            api_instance = IntellifeedAPICaller()
            print(content_info_dict)
            content_title_dic = {"title_cn": dict_item['title_cn']}
            check_response = api_instance.check_IntelliFeedContent(content_title_dic)
            try:
                check_result = check_response.json()
            except Exception as err:
                check_result = err
            print(check_result)
            if check_result == 'request success' and dict_item['content_cn'].strip() != '':
                results.append(content_info_dict)
                ret = api_instance.new_content(content_info_dict)
                print('create succrss content ', str(ret.status_code))
                print(ret.text)
            else:
                print('no create content ')

        return results


if __name__ == '__main__':
    scrapy_data_v1 = ScrapeWeiChat(website='https://weixin.sogou.com/')
    result_data_list = scrapy_data_v1.caijingmi()
    print(result_data_list)
    # with open('wechat.json', 'w') as f:
    #     json.dump(acct_urls, f)
    # # with open('wechat.json', 'r') as f:
    # #     acct_urls = json.load(f)
    # #     print(acct_urls)
    #
    # for k in acct_urls:
    #     news_list = scrapy_data_v1.get_html(acct_urls[k])
    #     results = scrapy_data_v1.wechat_account(k, news_list)
    #     print(results[0], results[-1])
    print('done')
