import re
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from common.api import IntellifeedAPICaller
from common.common import get_avatar_and_upload_to_s3, get_article_img_and_upload_to_s3

profile_obj = IntellifeedAPICaller()


def get_article_url():
    chromedriver = "../../driver_binary_files/chromedriver_mac_xu"
    driver = webdriver.Chrome(chromedriver)
    driver.get('https://www.baidu.com/')
    driver.find_element_by_id("kw").send_keys('麦格理 inurl:baijiahao.baidu.com')
    driver.find_element_by_id('su').click()
    for i in range(50):
        time.sleep(1)
        hreflist = driver.find_elements_by_xpath("//*[@class='result c-container ']/h3/a")
        for href in hreflist:
            href_text = href.get_attribute('href') + '\n'
            with open('baidu_url.txt', 'a') as f:
                f.write(href_text)
        driver.find_element_by_xpath('//a[text()="下一页>"]').click()
        time.sleep(3)
    # driver.get(href_text)
    # lst_new = {"nick_name": "", "real_name": "", "gender": "", "email": "", "avatar": "", "living_location": "",
    #            "industry": "", "career": "", "education": "", "self_intro": "", "original_url": "",
    #            "followers": {"weibo": {"follower": "", "fans": ""}}, "achievement": ""}
    # time.sleep(1)
    # # img_src = href.find_elements_by_xpath('//*[@id="article"]')
    # # print(img_src)
    # driver.back()
    # time.sleep(5)


def get_article_content():
    with open('baidu_url.txt', 'r') as f_1:
        all_baidu_lst = f_1.readlines()
        for url in all_baidu_lst:
            url = url.strip('\n')
            chromedriver = "../../driver_binary_files/chromedriver_mac_xu"
            driver = webdriver.Chrome(chromedriver)
            driver.get(url)
            user_dic = {"nick_name": "", "real_name": "", "gender": "", "email": "", "avatar": "",
                        "living_location": "",
                        "industry": "", "career": "", "education": "", "self_intro": "", "original_url": "",
                        "followers": {"baijiahao": {"follower": "", "fans": ""}}, "achievement": ""}
            img_obj = driver.find_elements_by_xpath('//*[@id="right-container"]/div[1]/div/div[1]/div[1]/img')
            img_src = img_obj[0].get_attribute('src')
            img_id = re.findall(r'\d+', driver.current_url)
            path = get_avatar_and_upload_to_s3(img_src, 'baidu_' + img_id[0])
            user_name = driver.find_element_by_xpath('//p[@class="author-name"]').text
            user_intro = driver.find_element_by_xpath('//div[@class="author-desc"]/p').text
            user_dic['nick_name'] = user_name
            user_dic['avatar'] = path
            user_dic['self_intro'] = user_intro
            user_dic['gender'] = 'male'
            user_dic['original_url'] = 'https://baijiahao.baidu.com/#' + user_name
            print(user_dic)
            kol_response = profile_obj.new_kol_profile(user_dic)
            print(kol_response.status_code)
            print(kol_response.json())
            with open('baidu_kol_id.txt', 'a') as f:
                f.write(kol_response.json()['id'] + '\n')
            print('create success kol profile!!!')
            content = {
                "content_info": {
                    "source_website_name": "Baijiahao",
                    "source_domain_url": "https://baijiahao.baidu.com/",
                    "source_full_destination_url": driver.current_url,
                    "publish_time": "",
                    "content_cn": "",
                    "title_cn": " ",
                    "kol_id": kol_response.json()['id'],
                    "types": "social_media"
                }
            }
            article_title = driver.find_element_by_xpath('//div[@class="article-title"]/h2').text
            article_content = driver.find_element_by_xpath('//div[@class="article-content"]').get_attribute('innerHTML')
            article_time_1 = driver.find_element_by_xpath('//span[@class="date"]').text
            article_time_2 = driver.find_element_by_xpath('//span[@class="time"]').text
            article_time = article_time_1 + ' ' + article_time_2
            if len(article_time) < 12:
                article_time = '2019-' + article_time
            else:
                article_time = '20' + article_time
            content['content_info']['title_cn'] = article_title
            content['content_info']['content_cn'] = article_content
            content['content_info']['publish_time'] = article_time
            print(content)
            article_response = profile_obj.new_content(content)
            print(article_response.status_code)
            print(article_response.text)
            print('create success intellifeeds content')
            time.sleep(1)
            driver.close()
            time.sleep(2)


if __name__ == '__main__':
    # get_article_url()
    get_article_content()
