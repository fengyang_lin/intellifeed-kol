# -*- coding: utf-8 -*-
"""
# run the code every 1 day(9:00 am), now we just scrape first page of the website

# URL: http://www.weilaicaijing.com

"""
from scrawl.news.base import BaseScraper
from datetime import datetime, timedelta


class ScrapeFutureCaiJing(BaseScraper):
    def __init__(self, domain_url, media, lang):
        self.headers = {'Host': 'www.weilaicaijing.com',
                        'Connection': 'keep-alive',
                        'Accept': '*/*',
                        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 '
                                      '(KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36)',
                        'Referer': 'http://www.weilaicaijing.com/NowExpress',
                        'blk': '0',
                        'Accept-Language': 'zh-CN,zh;q=0.9',
                        'Cookie': 'acw_tc=2f624a1c15457137379536610e25b894f273e2b01f3f07171cabdf0cc7bec5; '
                                  'Hm_lvt_90c17e90df01626597c851d142c5c238=1545713739; '
                                  'UM_distinctid=167e3b808c2c5-014d024ebbc3c8-58422116-100200-167e3b808c7143; '
                                  'CNZZDATA1273193969=751500642-1545710028-%7C1545793314; '
                                  'Hm_lpvt_90c17e90df01626597c851d142c5c238=1545794928; '
                                  'SERVERID=80ed57a37c15788c2220d1860013bfb4|1545794931|1545713737'}
        self.base_url = 'http://www.weilaicaijing.com/NowExpress/'
        super(ScrapeFutureCaiJing, self).__init__(domain_url, media, lang)

    def response_data(self):
        page = 1
        while True:
            url = self.domain_url + str(page)
            json_obj = super(ScrapeFutureCaiJing, self).response_data_by_request(self.headers)
            for day in json_obj['data']:
                for item in day['list']:
                    dict_item = dict()
                    dict_item['title'] = '[' + item['text'].split('】')[0][1:] + ']'
                    dict_item['url'] = 'http://www.weilaicaijing.com/NowExpress/' + str(item['id'])
                    dict_item['content'] = item['text']
                    dict_item['language'] = 'cn'
                    str_time = day['time'].split(' ')[0] + ' ' + item['hour'] + ':00'
                    dict_item['published_at'] = datetime.strptime(str_time, "%Y-%m-%d %H:%M:%S")
                    dict_item['is_added_to_source'] = False
                    dict_item['media'] = 'WeilaiCaijing'
                    if datetime.datetime.now() - dict_item['published_at'] > timedelta(days=1):
                        return self.result_json
                    self.result_json.append(dict_item)


# class scrapy_data:
#     def __init__(self):
#         self.result_json = []
#         self.headers = {'Host': 'www.weilaicaijing.com',
#                         'Connection': 'keep-alive',
#                         'Accept': '*/*',
#                         'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 '
#                                       '(KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36)', \
#                         'Referer': 'http://www.weilaicaijing.com/NowExpress',
#                         'blk': '0',
#                         'Accept-Language': 'zh-CN,zh;q=0.9',
#                         'Cookie': 'acw_tc=2f624a1c15457137379536610e25b894f273e2b01f3f07171cabdf0cc7bec5; '
#                                   'Hm_lvt_90c17e90df01626597c851d142c5c238=1545713739; '
#                                   'UM_distinctid=167e3b808c2c5-014d024ebbc3c8-58422116-100200-167e3b808c7143; '
#                                   'CNZZDATA1273193969=751500642-1545710028-%7C1545793314; '
#                                   'Hm_lpvt_90c17e90df01626597c851d142c5c238=1545794928; '
#                                   'SERVERID=80ed57a37c15788c2220d1860013bfb4|1545794931|1545713737'
#                         }
#
#     def reponse_data(self):
#         iter_ = True
#         page = 1
#         while iter_ == True:
#             url = 'http://www.weilaicaijing.com/api/Fastnews/lists?search_str=&page=' + str(page)
#             response = requests.get(url, headers=self.headers)
#             str_response = response.text
#             json_obj = json.loads(str_response)
#             for day in json_obj['data']:
#                 for item in day['list']:
#                     dict_item = {}
#                     dict_item['title'] = '[' + item['text'].split('】')[0][1:] + ']'
#                     dict_item['url'] = 'http://www.weilaicaijing.com/NowExpress/' + str(item['id'])
#                     dict_item['content'] = item['text']
#                     dict_item['language'] = 'cn'
#                     strtime = day['time'].split(' ')[0] + ' ' + item['hour'] + ':00'
#                     dict_item['published_at'] = datetime.datetime.strptime(strtime, "%Y-%m-%d %H:%M:%S")
#                     dict_item['is_added_to_source'] = False
#                     dict_item['media'] = 'WeilaiCaijing'
#                     if datetime.datetime.now() - dict_item['published_at'] > datetime.timedelta(days=1):
#                         return self.result_json
#                     self.result_json.append(dict_item)
#             page += 1
#         return self.result_json
#
#
# if __name__ == '__main__':
#     scrapy_data_v1 = scrapy_data()
#     result_data_list = scrapy_data_v1.reponse_data()
#     print('*'*20 + "begin to connect to red shift "+ '*'*20)
#     COLUM_LIST = ["title", "url", "content", "language", 'published_at', "is_added_to_source", "media"]
#     if result_data_list:
#     	con_obj = ScrapyConnectToRedshift()
# 	con_obj.bulk_insert('media_source_content', COLUM_LIST, result_data_list)
#     # con_obj.bulk_insert('media_source_content', COLUM_LIST, result_data_list
