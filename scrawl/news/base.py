#  -*-coding:utf-8 -*-
from urllib.request import urlopen
from datetime import datetime, timedelta
import json
import requests
from lxml import etree
from bs4 import BeautifulSoup as BS
from newspaper import Article
from common.api import IntellifeedAPICaller
import settings


class BaseScraper:
    column_list = ["url", "title", "content", "language", "published_at", "is_added_to_source", "media"]
    now_time = datetime.now()
    result_json = []
    api_mvp = IntellifeedAPICaller()
    EC2 = 'EC2'
    Job_Name = 'JOB_INELLIFEED_NEWS'

    def __init__(self, domain_url, media, lang):
        self.domain_url = domain_url
        self.media = media
        self.language = lang

    def response_data_by_urlopen(self):
        response = urlopen(self.domain_url)
        str_response = response.read().decode('utf-8')
        json_obj = json.loads(str_response)
        return json_obj

    def response_data_by_request(self, header_data):
        response = requests.get(self.domain_url, headers=header_data)
        json_obj = json.loads(response.text)
        return json_obj

    def response_data_by_request_post(self, header_data):
        response = requests.post(self.domain_url, headers=header_data)
        return response.text

    def extract_content_by_bs(self, url):
        response = requests.get(url)
        bs_test = BS(response.text, 'lxml')
        return bs_test

    def extract_content_by_bs_content(self, url):
        response = requests.get(url)
        bs_test = BS(response.content, 'lxml')
        return bs_test

    def extract_content_by_bs_header(self, url, headers):
        response = requests.get(url, headers=headers)
        bs_test = BS(response.text, 'lxml')
        print(response.status_code)
        return bs_test

    def extract_content_by_bs_header_for_cbrc(self, url, headers):
        response = requests.get(url, headers=headers)
        bs_test = BS(response.text, 'lxml')

        return bs_test

    def extract_content_by_newspaper3k(self, url, lang):
        try:
            article = Article(url, language=lang)
            article.download()
            article.parse()
            return article.text
        except Exception as err:
            return False

    def extarct_lates_data_from_call_api(self):
        response = requests.get(self.domain_url)
        soup = BS(response.content, 'html5lib')
        print(soup)
        return soup

    def extract_content_by_xpath(self, url):
        sourceHtml = requests.get(url).text
        selector = etree.HTML(sourceHtml)
        return selector

    def check_scrawl_process_status(self, result_list):
        # check the content ,if the amount of empty content is more than 3 ,the process is defined as failed
        to_check_content_list = [item['content'] for item in result_list]
        flg_for_process_success = 0
        for item in to_check_content_list:
            if item.strip() == '':
                flg_for_process_success += 1
        record_dict = dict()
        if flg_for_process_success >= 3:
            record_dict["name"] = self.Job_Name
            record_dict["over_all_status"] = False
            record_dict["trigger_by"] = self.EC2
            record_dict["details"] = result_list
            self.api_mvp.intellifeed_regular_job_record(record_dict)
            return False
        else:
            record_dict["name"] = self.Job_Name
            record_dict["over_all_status"] = True
            record_dict["trigger_by"] = self.EC2
            record_dict["details"] = result_list
            self.api_mvp.intellifeed_regular_job_record(record_dict)
            return True


