#  -*-coding:utf8 -*-
"""
# run the code every 1 day(9:00 am), now we just scrape first page of the website,
# call api to get data
# URL: https://www.scmp.com/topics/banking-finance

"""
from datetime import timedelta
from dateutil.parser import parse
from scrawl.news.base import BaseScraper


class ScrapeScmp(BaseScraper):
    def __init__(self, domain_url, media, lang):
        self.base_url = 'https://www.scmp.com'
        super(ScrapeScmp, self).__init__(domain_url, media, lang)

    def response_data(self):
        bs_response = super(ScrapeScmp, self).extarct_lates_data_from_call_api()
        # second_part_article = bs_response.find('div', class_='article-level article-level-three article article-area__content content thumb--large')

        article_list_html = bs_response.find_all('article-level article-level-three article article-area__content content thumb--large')
        print(len(article_list_html))
        for item in article_list_html:
            dict_item = dict()
            dict_item['url'] = self.base_url + item.find('a')['href']
            dict_item['url'] = dict_item['url'].strip()
            dict_item['title'] = item.find('a')['href']
            dict_item['content'] = self.extract_content_by_newspaper3k(dict_item['url'], 'en')
            dict_item['published_at'] = item.find('div', class_='content-wrapper').find('time')['content']
            if self.now_time - parse(dict_item['published_at']).replace(tzinfo=None) >= timedelta(days=2):
                return self.result_json
            dict_item['language'] = self.language
            dict_item['is_added_to_source'] = False
            dict_item['media'] = self.media
            self.result_json.append(dict_item)
        return self.result_json
