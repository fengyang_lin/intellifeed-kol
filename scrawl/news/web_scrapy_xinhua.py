# -*- coding: utf-8 -*-
"""
# run the code every 1 day(9:00 am), now we just scrape first page of the website

# URL: http://www.xinhuanet.com

"""
from datetime import timedelta, datetime
from scrawl.news.base import BaseScraper
import json


class ScrapeXinHua(BaseScraper):
    Job_Name = 'JOB_INELLIFEED_NEWS'

    def __init__(self, domain_url, media, lang):
        self.headers = {
            'Accept': '*/*',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
                          'Chrome/71.0.3578.98 Safari/537.36)',
            'Referer': 'http://www.xinhuanet.com/english/list/china-business.htm'
        }
        super(ScrapeXinHua, self).__init__(domain_url, media, lang)

    def response_data(self):
        text = self.response_data_by_request_post(self.headers)
        str_response = text.replace('sam(', '')[:-1]
        dict_response = json.loads(str_response)
        response_list = []
        for item in dict_response['data']['list']:
            dict_item = dict()
            dict_item['url'] = item['LinkUrl']
            date_flg = datetime.strptime(item['PubTime'], "%Y-%m-%d %H:%M:%S")
            dict_item['published_at'] = item['PubTime']
            if self.now_time - date_flg > timedelta(days=1):
                return response_list
            dict_item['is_added_to_source'] = False
            dict_item['media'] = self.media
            dict_item['title'] = item['Title']
            dict_item['language'] = self.language
            item['LinkUrl'] = item['LinkUrl'].strip()
            dict_item['content'] = self.extract_content_by_newspaper3k(item['LinkUrl'], 'en')
            response_list.append(dict_item)
        # _ = self.check_scrawl_process_status(response_list[:1])
        return response_list


# def get_source():
    # headers1 = {'Accept': '*/*',
    #             'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36
#  (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36)',
    #             'Referer': 'http://www.xinhuanet.com/english/list/china-business.htm'
    #             }
    #
    # url = 'http://qc.wa.news.cn/nodeart/list?nid=11143393&pgnum=1&cnt=50&tp=1&orderby=1?callback=sam&_=1545873337323'
    # text = requests.post(url, headers1)
    # str_response = response.text
    # str_response = str_response.encode("utf-8").replace('sam(', '')
    # str_response = str_response[:-1]
    # dict_response = json.loads(str_response)
    # response_list = []
    # for item in dict_response['data']['list']:
    #     dict_item = dict()
    #     dict_item['url'] = item['LinkUrl']
    #     dict_item['published_at'] = datetime.datetime.strptime(item['PubTime'], "%Y-%m-%d %H:%M:%S")
    #     if datetime.datetime.now() - dict_item['published_at'] > datetime.timedelta(days=1):
    #         return response_list
    #     dict_item['is_added_to_source'] = False
    #     dict_item['media'] = 'xin-hua-business'
    #     dict_item['title'] = item['Title']
    #     dict_item['language'] = 'en'
    # if item['Abstract']:
    #     dict_item['content'] = item['Abstract']
    # else:
    #     dict_item['content'] = ''
    #     response_list.append(dict_item)
    # return response_list

#
# if __name__ == '__main__':
#     response_data = get_source()
#     COLUM_LIST = ["url", "title", "content", "language", "published_at", "is_added_to_source", "media"]
#     if response_data:
#         con_obj = ScrapyConnectToRedshift()
#         con_obj.bulk_insert('media_source_content', COLUM_LIST, response_data)
# _process_status(response_list)
#         return response_list


# def get_source():
    # headers1 = {'Accept': '*/*',
    #             'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36
#  (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36)',
    #             'Referer': 'http://www.xinhuanet.com/english/list/china-business.htm'
    #             }
    #
    # url = 'http://qc.wa.news.cn/nodeart/list?nid=11143393&pgnum=1&cnt=50&tp=1&orderby=1?callback=sam&_=1545873337323'
    # text = requests.post(url, headers1)
    # str_response = response.text
    # str_response = str_response.encode("utf-8").replace('sam(', '')
    # str_response = str_response[:-1]
    # dict_response = json.loads(str_response)
    # response_list = []
    # for item in dict_response['data']['list']:
    #     dict_item = dict()
    #     dict_item['url'] = item['LinkUrl']
    #     dict_item['published_at'] = datetime.datetime.strptime(item['PubTime'], "%Y-%m-%d %H:%M:%S")
    #     if datetime.datetime.now() - dict_item['published_at'] > datetime.timedelta(days=1):
    #         return response_list
    #     dict_item['is_added_to_source'] = False
    #     dict_item['media'] = 'xin-hua-business'
    #     dict_item['title'] = item['Title']
    #     dict_item['language'] = 'en'
    # if item['Abstract']:
    #     dict_item['content'] = item['Abstract']
    # else:
    #     dict_item['content'] = ''
    #     response_list.append(dict_item)
    # return response_list

#
# if __name__ == '__main__':
#     response_data = get_source()
#     COLUM_LIST = ["url", "title", "content", "language", "published_at", "is_added_to_source", "media"]
#     if response_data:
#         con_obj = ScrapyConnectToRedshift()
#         con_obj.bulk_insert('media_source_content', COLUM_LIST, response_data)
