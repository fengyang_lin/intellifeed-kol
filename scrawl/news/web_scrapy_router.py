#  -*-coding:utf8 -*-
"""
# run the code every 1 day(9:00 am), now we just scrape first page of the website,
# call api to get data
# URL: https://api.jinse.com/v4/live/list?limit=200&reading=false&source=web

"""

from datetime import datetime, timedelta
from scrawl.news.base import BaseScraper


class ScrapeRouter(BaseScraper):

    def __init__(self, domain_url, media, lang):
        self.header_data = {
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7',
            'cache-control': 'max-age=0',
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36(KHTML, like Gecko) '
                          'Chrome/71.0.3578.98 Safari/537.36'
        }
        super(ScrapeRouter, self).__init__(domain_url, media, lang)

    def extract_content(self):
        json_obj = super(ScrapeRouter, self).response_data_by_request(self.header_data)
        json_obj = json_obj['wireitems']
        for item in json_obj:
            dict_rp = {}
            base_item = item['templates'][0]
            if 'story' in base_item.keys():
                story_item = base_item['story']
                dict_rp['url'] = story_item['url']
                dict_rp['title'] = story_item['hed']
                dict_rp['content'] = story_item['lede']
                dict_rp['language'] = self.language
                dict_rp['published_at'] = story_item['updated_at']
                dict_rp['is_added_to_source'] = False
                dict_rp['media'] = self.media
                test = datetime.strptime(dict_rp['published_at'], "%Y-%m-%dT%H:%M:%SZ")
                if datetime.now() - test < timedelta(days=1):
                    self.result_json.append(dict_rp)
                else:
                    return self.result_json
            else:
                pass
        return self.result_json

#
#
#
#
#
#
# def get_source():
#     header_data = {
#         'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
#         'accept-encoding': 'gzip, deflate, br',
#         'accept-language': 'en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7',
#         'cache-control': 'max-age=0',
#         'upgrade-insecure-requests': '1',
#         'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 '
#                       '(KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36'
#     }
#     try:
#         response = requests.get('https://wireapi.reuters.com/v3/feed/url/
# www.reuters.com/places/china', headers=header_data)
#         json_obj = json.loads(response.text)['wireitems']
#     except Exception as err:
#         sleep(10)
#         response = requests.get('https://wireapi.reuters.com/v3/feed/url/www.reuters.com/places/china',
#                                 headers=header_data)
#         json_obj = json.loads(response.text)['wireitems']
#
#     result_list = []
#     for rp_obj in json_obj:
#         dict_rp = {}
#         if 'story' in rp_obj['templates'][0].keys():
#             dict_rp['url'] = rp_obj['templates'][0]['story']['url']
#             dict_rp['title'] = rp_obj['templates'][0]['story']['hed']
#             dict_rp['content'] = rp_obj['templates'][0]['story']['lede']
#             dict_rp['language'] = 'en'
#             dict_rp['published_at'] = rp_obj['templates'][0]['story']['updated_at']
#             dict_rp['is_added_to_source'] = False
#             dict_rp['media'] = 'router'
#             test= datetime.strptime(dict_rp['published_at'], "%Y-%m-%dT%H:%M:%SZ")
#             if datetime.now() - test < timedelta(days=1):
#                 result_list.append(dict_rp)
#             else:
#                 return result_list
#         else:
#             pass
#     return result_list
#
#
# if __name__ == '__main__':
#     result = get_source()
#     COLUM_LIST = ["url", "title", "content", "language", "published_at", "is_added_to_source", "media"]
#     # print(result)
#     if result:
#         con_obj = ScrapyConnectToRedshift()
#         con_obj.bulk_insert('media_source_content', COLUM_LIST, result)
