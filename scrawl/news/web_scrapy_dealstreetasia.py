#  -*-coding:utf8 -*-
"""
# run the code every 2 day(9:00 am), now we just scrape first page of the website

# URL: http://www.chinadaily.com.cn/business/money/page_1.html

"""

from datetime import datetime, timedelta
from scrawl.news.base import BaseScraper


class ScrapeDealStreetAsia(BaseScraper):
    def extract_content_by_bs_data(self):
        page = 1
        while True:
            bs_response = self.extract_content_by_bs(self.domain_url + str(page))
            all_article_list = bs_response.findAll('div', class_='col-lg-12')
            for item in all_article_list:
                if item.find('h3'):
                    dict_item = dict()
                    dict_item['title'] = item.find('h3').find('a').getText()
                    dict_item['url'] = item.find('h3').find('a')['href']
                    dict_item['content'] = self.extract_content_by_newspaper3k(dict_item['url'], 'en')
                    dict_item['language'] = self.language
                    dict_item['media'] = self.media
                    str_date = item.find('small').find('time')['datetime']
                    data_format = datetime.strptime(str_date, "%Y-%m-%d")
                    if self.now_time - data_format > timedelta(days=2):
                        return self.result_json
                    dict_item['published_at'] = str_date
                    dict_item['is_added_to_source'] = False
                    self.result_json.append(dict_item)
            page += 1
    #
    # def get_detail_content(self, url):
    #     bs_response = self.extract_content_by_bs(url)
    #     if bs_response.find('section', class_='post_content clearfix'):
    #         second_part_articles = bs_response.find('section', class_='post_content clearfix')
    #     elif bs_test.find('section', class_='post_content podcast-content clearfix'):
    #         second_part_articles = bs_response.find('section', class_='post_content podcast-content clearfix')
    #     else:
    #         return ''
    #     article_part_list = second_part_articles.findAll('p')
    #     summary = ''
    #     for part in article_part_list:
    #         summary += part.getText() + '/n'
    #         if part.find('span'):
    #             summary += part.find('span').getText() + '  '
    #     return summary


#
#
#
# def get_detail_content(url):
#     response = requests.get(url)
#     bs_test = BS(response.text, 'lxml')
#     if bs_test.find('section', class_='post_content clearfix'):
#         second_part_articles = bs_test.find('section', class_='post_content clearfix')
#     elif bs_test.find('section', class_='post_content podcast-content clearfix'):
#         second_part_articles = bs_test.find('section', class_='post_content podcast-content clearfix')
#     else:
#         return ''
#     article_part_list = second_part_articles.findAll('p')
#     summary = ''
#     for part in article_part_list:
#         summary += part.getText() + '/n'
#         if part.find('span'):
#             summary += part.find('span').getText() + '  '
#     return summary
#
#
# def get_source():
#     iter_ = True
#     page = 1
#     result_data_list = []
#     while iter_:
#         url = 'https://www.dealstreetasia.com/countries/china-hk/page/' + str(page) + '/'
#         response = requests.get(url)
#         bs_response = BS(response.text, 'lxml')
#         all_article_list = bs_response.findAll('div', class_='col-lg-12')
#         for item in all_article_list:
#             if item.find('h3'):
#                 dict_item = {}
#                 dict_item['title'] = item.find('h3').find('a').getText()
#                 dict_item['url'] = item.find('h3').find('a')['href']
#                 dict_item['content'] = get_detail_content(item.find('h3').find('a')['href'])
#                 dict_item['language'] = 'en'
#                 dict_item['media'] = 'dealstreetasia'
#                 str_date = item.find('small').find('time')['datetime']
#                 data_format = datetime.strptime(str_date, "%Y-%m-%d")
#                 if datetime.now() - data_format > timedelta(days=2):
#                     return result_data_list
#                 dict_item['published_at'] = data_format
#                 dict_item['is_added_to_source'] = False
#                 result_data_list.append(dict_item)
#         page += 1
#     return result_data_list
#
#
# if __name__ == '__main__':
#     result_data_list = get_source()
#     COLUM_LIST = ["url", "title", "content", "language", "published_at", "is_added_to_source", "media"]
#     if result_data_list:
#         print('successfully  store  data to Red Shift')
#         con_obj = ScrapyConnectToRedshift()
#         con_obj.bulk_insert('media_source_content', COLUM_LIST, result_data_list)
