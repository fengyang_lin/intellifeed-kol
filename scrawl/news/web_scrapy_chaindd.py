#  -*-coding:utf8 -*-
"""
# run the code every day(9:00 am), now we just scrape first page of the website

# URL: https://www.chaindd.com/nictation/1

"""
from scrawl.news.base import BaseScraper
from dateutil.parser import parse


class ScrapeChainDd(BaseScraper):
    def __init__(self, domain_url, media, lang):
        self.base_element = '/html/body/div[1]/div[3]/div[1]/div[1]/ul/li['
        self.page = 1
        self.date_range = 2
        self.media = media
        self.domain_url = domain_url
        self.headers = {
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7',
            'cache-control': 'max-age=0',
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36(KHTML, like Gecko) '
                          'Chrome/71.0.3578.98 Safari/537.36',
            'cookie': 'ci_session=cceb17585076ad88a6bdc1d2d2b90501bd7214d0; user_lang=cn; '
                      'Hm_lvt_a7a57c6655d68aeb04e6846818c79edd=1549964093; '
                      'zg_did=%7B%22did%22%3A%20%22168e10f2a2c7d6-055dfdccae89ce-10336654-1fa400-168e10f2a2d459%22%7D; '
                      '_ga=GA1.2.1426466643.1549964094; _gid=GA1.2.685309623.1549964094; responseTimeline=179; '
                      'Hm_lpvt_a7a57c6655d68aeb04e6846818c79edd=1549965702; '
                      'zg_dc1e574e14aa4c44b51282dca03c46f4=%7B%22sid%22%3A%201549964094%2C%'
                      '22updated%22%3A%201549965702.026%2C%22info%22%3A%201549964094003%7D'
        }
        super(ScrapeChainDd, self).__init__(domain_url, media, lang)

    def extract_content_by_bs(self):
        flg_for_loop = True
        while flg_for_loop:
            bs_response = super(ScrapeChainDd, self).extract_content_by_bs_header(self.domain_url + str(self.page),
                                                                                  self.headers)
            data_amount = bs_response.find('div', class_='word_list').find('div',
                                                                           class_='day_part').findAll('time', data=True)
            if len(data_amount) > 1:
                flg_for_loop = False
            today_datetime = bs_response.find('div', class_='word_list').find('div', class_='day_part').find(
                'time').getText()
            today_articles = bs_response.find('div',
                                              class_='word_list').find('div',
                                                                       class_='day_part').find('ul').findAll('li')
            for article in today_articles:
                if article.find('h2', class_="w_tit"):
                    dict_item = dict()
                    dict_item['url'] = article.find('a')['href']
                    dict_item['title'] = article.find('a').getText()
                    dict_item['content'] = self.get_content_in_detail_page(dict_item['url'])
                    dict_item['published_at'] = parse(today_datetime, fuzzy=True).strftime("%Y-%m-%d %H:%M:%S")
                    dict_item['media'] = self.media
                    dict_item['language'] = self.language
                    self.result_json.append(dict_item)
            self.page += 1
        return self.result_json

    def get_content_in_detail_page(self, url):
        bs_test = super(ScrapeChainDd, self).extract_content_by_bs_header(url, self.headers)
        summary = bs_test.find('article').find('p', class_='inner').getText()
        return summary

