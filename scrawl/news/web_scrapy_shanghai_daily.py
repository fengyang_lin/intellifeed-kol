#  -*-coding:utf8 -*-
"""
# run the code every 1 day(9:00 am), now we just scrape first page of the website,
# call api to get data
# URL: https://api.jinse.com/v4/live/list?limit=200&reading=false&source=web

"""
from datetime import timedelta
import dateutil.parser as parser
from scrawl.news.base import BaseScraper


class ScrapeShanghaiDaily(BaseScraper):
    def __init__(self, domain_url, media, lang):
        self.base_element = '//*[@id="lft-art"]/div['
        self.content_base_element = '//*[@id="Content"]/p'
        super(ScrapeShanghaiDaily, self).__init__(domain_url, media, lang)

    def response_data(self):
        selector = super(ScrapeShanghaiDaily, self).extract_content_by_xpath(self.domain_url)
        for index in range(1, 20):
            dict_result = dict()
            dict_result['title'] = selector.xpath(self.base_element + str(index) + ']/span[2]/h4/a/text()')[0]
            dict_result['url'] = 'https:' + selector.xpath(self.base_element + str(index) + ']/span[2]/h4/a/@href')[0]
            dict_result['content'] = self.get_content_in_detail_page(dict_result['url'])
            check_date_flg = selector.xpath(self.base_element + str(index) + ']/span[2]/b/text()')[0]
            if self.now_time - parser.parse(check_date_flg) < timedelta(days=1):
                dict_result['published_at'] = parser.parse(check_date_flg).strftime("%Y-%m-%d %H:%M:%S")
                dict_result['is_added_to_source'] = False
                dict_result['language'] = self.language
                dict_result['media'] = self.media
                self.result_json.append(dict_result)
            else:
                return self.result_json
        return self.result_json

    def get_content_in_detail_page(self, url_value):
        selector = super(ScrapeShanghaiDaily, self).extract_content_by_xpath(url_value)
        label_p_list_len = len(selector.xpath(self.content_base_element))
        all_content = ''
        for index in range(label_p_list_len):
            p_content = selector.xpath(self.content_base_element + '[' + str(index + 1) + ']/text()')
            p_strong_content = selector.xpath(self.content_base_element + '[' + str(index + 1) + ']/strong/text()')
            if p_content:
                all_content += p_content[0]
            elif p_strong_content:
                all_content += p_strong_content[0] + '\n'
        return all_content


# class scrapy_data:
#     def __init__(self, website_url=''):
#         self.aimed_website = website_url
#         self.result_list = []
#         self.now_time = datetime.now()
#         self.base_element = '//*[@id="lft-art"]/div['
#
#     def get_source_web(self):
#         sourceHtml = requests.get(self.aimed_website)
#         return sourceHtml.text
#
#     def convert_datetime(self, strtime):
#         l = strtime.split(' ')
#         l1 = [l[2].rstrip(','), self.month[l[0]], l[1].rstrip(',')]
#         time = '-'.join(l1)
#         return datetime.strptime(time, "%Y-%m-%d")
#
#     @staticmethod
#     def format_datetime(date_time):
#         dt = parse(date_time, fuzzy=True)
#         return dt
#
#     def response_data(self):
#
#         selector = super(ScrapeCaiXin, self).extract_content_by_xpath(self.domain_url)
#
#         sourceHtml = self.get_source_web()
#         selector = etree.HTML(sourceHtml)
#
#         for index in range(1, 20):
#             dict_result = {}
#             '//*[@id="lft-art"]/div[1]/span[2]/h4/a'
#             dict_result['title'] = selector.xpath(self.base_element + str(index) + ']/span[2]/h4/a/text()')[0]
#             url = selector.xpath(self.base_element + str(index) + ']/span[2]/h4/a/@href')[0]
#             dict_result['url'] = url
#             dict_result['content'] = self.get_content_in_detail_page(url)
#             check_date_flg = selector.xpath(self.base_element + str(index) + ']/span[2]/b/text()')[
#                     0]
#             if self.now_time - parser.parse(check_date_flg) < timedelta(days=1):
#                 dict_result['published_at'] = parser.parse(check_date_flg)
#                 dict_result['is_added_to_source'] = False
#                 dict_result['language'] = 'cn'
#                 dict_result['media'] = 'shanghai-daily-money'
#                 self.result_list.append(dict_result)
#             else:
#                 return self.result_list
#         return self.result_list
#
#     @staticmethod
#     def get_content_in_detail_page(url):
#         sourceHtml = requests.get('https:' + url).text
#         selector = etree.HTML(sourceHtml)
#         label_p_list_len = len(selector.xpath('//*[@id="Content"]/p'))
#         all_content = ''
#         for index in range(label_p_list_len):
#             p_content = selector.xpath('//*[@id="Content"]/p[' + str(index + 1) + ']/text()')
#             p_strong_content = selector.xpath('//*[@id="Content"]/p[' + str(index + 1) + ']/strong/text()')
#             if p_content:
#                 all_content += p_content[0]
#             elif p_strong_content:
#                 all_content += p_strong_content[0] + '\n'
#         return all_content
#
#
# if __name__ == '__main__':
#     url = 'http://www.chinadaily.com.cn/business/money'
#     scrapy_data_v1 = scrapy_data(website_url=url)
#     result_data_list = scrapy_data_v1.response_data()
#     print('*' *20 + "begin to connect to red shift " + '*' * 20)
#     COLUM_LIST = ["url", "title", "content", "language", "published_at", "is_added_to_source", "media"]
#     if result_data_list:
#        con_obj = ScrapyConnectToRedshift()
#        con_obj.bulk_insert('media_source_content', COLUM_LIST, result_data_list)
#        print('*' *20 + "successful to store data to red shift " + '*' * 20)
