#  -*-coding:utf8 -*-
"""
# run the code every 2 day(9:00 am), now we just scrape first page of the website

# URL_1 : https://www.caixinglobal.com/economy/
# URL_2 : https://www.caixinglobal.com/finance/
# URL_3 : https://www.caixinglobal.com/tech/

"""
from datetime import datetime, timedelta
from dateutil.parser import parse

from scrawl.news.base import BaseScraper


class ScrapeCaiXin(BaseScraper):

    def __init__(self, domain_url, media, lang):
        self.base_element = '/html/body/div[1]/div[3]/div[1]/div[1]/ul/li['
        self.page_size = 20
        self.date_range = 2
        self.media = media
        super(ScrapeCaiXin, self).__init__(domain_url, media, lang)

    def extract_content_by_xpath(self):
        selector = super(ScrapeCaiXin, self).extract_content_by_xpath(self.domain_url)
        for index in range(self.page_size):
            dict_result = dict()
            dict_result['title'] = selector.xpath(self.base_element + str(index + 1) + ']/div/dl/dt/a/text()')[0]
            is_dd_summary_empty = selector.xpath(self.base_element + str(index + 1) + ']/div/dl/dd/text()')
            is_dd_2_summary_empty = selector.xpath(self.base_element + str(index + 1) + ']/div/dl/dd[2]/text()')
            if is_dd_summary_empty:
                dict_result['content'] = is_dd_summary_empty[0]
            elif is_dd_2_summary_empty:
                dict_result['content'] = is_dd_2_summary_empty[0]
            else:
                dict_result['content'] = ''
            dt = selector.xpath(self.base_element + str(index + 1) + ']/div/div/div/text()')[0]  # Jan 25, 2019 08:29 AM
            if dt:
                format_date = parse(dt, fuzzy=True)
                if self.now_time - format_date >= timedelta(days=self.date_range):
                    return self.result_json
                dict_result['published_at'] = format_date.strftime("%Y-%m-%d %H:%M:%S")
            else:
                dict_result['published_at'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            dict_result['title'] = selector.xpath(self.base_element + str(index + 1) + ']/div/dl/dt/a/text()')[0]
            dict_result['url'] = selector.xpath(self.base_element + str(index + 1) + ']/div/dl/dt/a/@href')[0]
            dict_result['is_added_to_source'] = False
            dict_result['language'] = 'en'
            dict_result['media'] = self.media
            self.result_json.append(dict_result)
        return self.result_json
