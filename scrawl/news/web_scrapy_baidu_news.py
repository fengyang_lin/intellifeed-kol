#  -*-coding:utf8 -*-
from scrawl.news.base import BaseScraper
import time
from selenium import webdriver
import settings
from selenium_framework import web_element
from selenium_framework.web_element import identifier
from selenium_framework.utils import BasePage, BaseAction
from selenium_framework.utils import BaseAction, start_browser, close_browser
from newspaper import Article, ArticleException
from common import api
import datetime
from urllib.parse import urlparse, quote
import urllib.request
from translate import Translator


class SearchPage(BasePage):
    btn_next_page = web_element.WebButton(identifier.xpath, '//a[contains(text(), "下一页>")]', 'Next Button')


# this is to  translate website title from zh to en
def translated_by_google(text_to_translate=None):
    translator = Translator(from_lang="zh_CN", to_lang="en")
    try:
        result = translator.translate(text_to_translate)
    except Exception as err:
        print(str(err))
        return ''
    if result:
        return result
    else:
        return ''


class LandingHome(BaseAction):
    now_date = datetime.date.today()

    # this dict is from luke
    refer_website_dict = {'证券市场红周刊': 'Weekly on Stocks', '365淘房': 'House 365', '北青网': 'Beijing Youth Daily',
                          '同花顺': 'Tonghuashun', '瞄股网': 'miaogu.com', '凤凰网': 'iFeng', '中证网': 'China Stock',
                          '艾瑞网': 'iResearch', '财新网': 'Finance News Net', '德州新闻网': 'Dezhou News',
                          '博燃网': 'Gas Show Net',
                          '同花顺金融网': 'Tonghuashun > Finance', '无时尚中文网': 'NOFASHION', '千家网': 'qianjia.com',
                          '联商网': 'Link Shop Net', '新华网海南站': 'Xinhua Net Hainan',
                          '中国电力新闻网': 'China Power News', '263财富网': '263 Fortune',
                          '中国石化新闻网': 'Sinopec News', '环球人物网': 'Universal People Net',
                          '中国网络电视台': 'China Network TV', 'FX168财经网': 'FX168 Financials',
                          '中国银河证券': 'China Galaxy Stock', '界面新闻': 'jiemian.com', '电缆网': 'CableABC',
                          '楚秀网': 'aihami.com', '东方财富网港股频道': 'Oriental Fortune Hong Kong Stock Channel',
                          '金融界': 'China Finance Online', 'Jinse Finance': 'Golden Finance',
                          '中国新闻网': 'China News Net', '亿欧网': 'iyiou.com', '环球网': 'Global News',
                          '好买基金网': 'od Buy Fund', '和讯': 'hexun.com', '汇通网': 'huitong.com',
                          'T客帮': 'Techbang', '雨果网': 'Cross-boader News', '集微网': 'Integrated Circuit Net',
                          '新芽网': 'New Seed Net', '证券时报网': 'Stock Times', '中国电池网': 'China Battery',
                          '红商网': 'Red Business', '益盟操盘手': 'Yimeng Trader', '澎湃新闻': 'ThePaper',
                          '站长之家': 'China Webmaster', '证券之星': 'Stock Star',
                          '同花顺金融服务网': 'Tonghuashun > Financial Service', '腾讯证券': 'Tencent Stock',
                          '融360': 'Financing 360', '投资界': 'PE Daily', 'JinseCaijing': 'Golden Finance',
                          '华尔街见闻': 'Wall Street Knowledge', '经济网': 'Economics Net'
                          }

    black_list = ['凤凰之家', '汽车之家']

    @staticmethod
    def next_page_home(driver):
        homepage_obj = SearchPage()
        pass_creteria = homepage_obj.btn_next_page.if_exist(driver)
        if pass_creteria:
            homepage_obj.btn_next_page.click(driver)
            time.sleep(5)
            return pass_creteria
            #
        else:
            close_browser(driver)
            return False

    def get_data_from_list_page(self, driver):
        per_page_content_list = []
        list_value = driver.find_elements_by_xpath("//div[contains(@class, 'result')]")
        title_value = driver.find_elements_by_xpath("//h3[@class = 'c-title']/a")
        author_value_list = driver.find_elements_by_xpath("//p[@class = 'c-author']")

         # Here is some bug if the len is not equal
        if len(title_value) == len(list_value) and len(author_value_list) == len(list_value):
            # ensure the page is right
            total_page_in_per_page = len(list_value)
            for item in range(total_page_in_per_page):
                dict_per_item = dict()
                href_value = title_value[item].get_attribute("href")
                media, pub_time = [item for item in author_value_list[item].text.split("  ")]

                content, title = self.get_detail_content_by_newspaper3k(href_value)
                if title in self.refer_website_dict.keys():
                    title = self.refer_website_dict[title]
                elif title in self.black_list:
                    continue
                else:
                    title = translated_by_google(title)  # change title_cn  to title en
                if content and title:
                    dict_per_item['title'] = title
                    dict_per_item['content'] = content
                    dict_per_item['url'] = href_value
                    dict_per_item['published_at'] = self.convert_date_from_str(pub_time)
                    dict_per_item['language'] = 'cn'
                    dict_per_item['media'] = media
                    per_page_content_list.append(dict_per_item)
                else:
                    print('newspaper 36 seem not work for URL : ' + str(href_value))
            return per_page_content_list
        else:
            return False  # this show the

    def convert_date_from_str(self, str_date):
        print(str_date)
        if str_date.find("小时前")> 0:
            publish_time = int(str_date.split('小时前')[0].strip(" "))
            pub_time = self.now_date - datetime.timedelta(hours=publish_time)
            pub_time = pub_time.strftime("%Y-%m-%d")
            return pub_time
        elif str_date.find("分钟前")> 0:
            minute_time = int(str_date.split('分钟前')[0].strip(" "))
            pub_time = self.now_date - datetime.timedelta(minutes=minute_time)
            pub_time_str = pub_time.strftime("%Y-%m-%d")
            return pub_time_str

        array = time.strptime(str_date, u" %Y年%m月%d日 %H:%M")  # 2019年02月26日 16:23
        try:
            publish_time = time.strftime("%Y-%m-%d", array)
            return publish_time
        except Exception as e:
            return ''

    def get_all_data(self, driver):
        flg_status = True
        api_for_intell = api.IntellifeedAPICaller()
        while flg_status:
            per_page_data = self.get_data_from_list_page(driver)
            if per_page_data:
                # send data to mvp backend
                response = api_for_intell.send_latest_news_data_to_mvp(per_page_data, 'news')
                print(response.status_code)
                flg_status = self.next_page_home(driver)
                if not flg_status:
                    return False
            else:
                flg_status = self.next_page_home(driver)
                if not flg_status:
                    return False
                else:
                    continue

    def get_detail_content_by_newspaper3k(self, href, lang='zh'):
        article = Article(href, language=lang)
        try:
            article.download()
            article.parse()
        except ArticleException:
            return '', ''
        except Exception as err:
            return '', ''
        return article.text, article.title








# class ScrapeBaiduNews(BaseScraper):
#
#     url = 'http://news.baidu.com/ns?rn=20&ie=utf-8&cl=2&ct=1&bs=%E9%BA%A6%E6%A0%BC%E7%90%86pn%3D0&rsv_bp=1&sr=0&f=8&prevct=no&tn=news&word=%E9%BA%A6%E6%A0%BC%E7%90%86&rsv_sug2=0&inputT=5'
#
#     # def get_list_data_from_page(self, driver):
#     #     self.browser.get(self.url)
#     #     list_value = self.browser.find_elements_by_xpath("//div[contains(@class, 'result')]")
#     #     print(len(list_value))
#     #     for item in list_value:
#     #         print(item.text)
#
#     def get_data_from_baidu(self, driver):
#         list_value = self.browser.find_elements_by_xpath("//div[contains(@class, 'result')]")
#
#
#         # print(len(list_value))
#         # for item in list_value:
#         #     print(item.text)
#         # LandingHome()
#         # LandingHome.next_page_home(self.browser)
#         # close_browser(self.browser)
#
#     def click_next_page(self, driver):
#         LandingHome()
#         LandingHome.next_page_home(driver)
#


