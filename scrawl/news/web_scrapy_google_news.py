#  -*-coding:utf8 -*-
from scrawl.news.base import BaseScraper
import time
from selenium import webdriver
import settings
from selenium_framework import web_element
from selenium_framework.web_element import identifier
from selenium_framework.utils import BasePage, BaseAction
from selenium_framework.utils import BaseAction, start_browser, close_browser
from newspaper import Article, ArticleException
from common import api
import datetime


class SearchPage(BasePage):
    btn_next_page = web_element.WebButton(identifier.xpath, '//a[contains(text(), "下一页>")]', 'Next Button')


class LandingHome(BaseAction):
    now_date = datetime.date.today()

    @staticmethod
    def next_page_home(driver):
        homepage_obj = SearchPage()
        pass_creteria = homepage_obj.btn_next_page.if_exist(driver)
        if pass_creteria:
            homepage_obj.btn_next_page.click(driver)
            time.sleep(5)
            return pass_creteria
            #
        else:
            close_browser(driver)
            return False

    def get_data_from_list_page(self, driver):
        per_page_content_list = []
        list_value = driver.find_elements_by_xpath("//div[contains(@class, 'result')]")
        title_value = driver.find_elements_by_xpath("//h3[@class = 'c-title']/a")
        author_value_list = driver.find_elements_by_xpath("//p[@class = 'c-author']")

        if len(author_value_list) == len(list_value) and len(author_value_list) == len(author_value_list):
            # ensure the page is right
            total_page_in_per_page = len(author_value_list)
            for item in range(total_page_in_per_page):
                dict_per_item = dict()
                href_value = title_value[item].get_attribute("href")
                media, pub_time = [item for item in author_value_list[item].text.split("  ")]
                content, title = self.get_detail_content_by_newspaper3k(href_value)
                if content and title:
                    dict_per_item['title'] = title
                    dict_per_item['content'] = content
                    dict_per_item['url'] = href_value
                    dict_per_item['published_at'] = self.convert_date_from_str(pub_time)
                    dict_per_item['language'] = 'cn'
                    dict_per_item['media'] = media
                    per_page_content_list.append(dict_per_item)
                else:
                    print('newspaper 36 seem not work ' + str(href_value))
            return per_page_content_list
        else:
            print('check the web page ! ')

    def convert_date_from_str(self, str_date):
        print(str_date)
        if str_date.find("小时前")> 0:
            publish_time = int(str_date.split('小时前')[0].strip(" "))
            pub_time = self.now_date - datetime.timedelta(hours=publish_time)
            pub_time = pub_time.strftime("%Y-%m-%d")
            return pub_time
        elif str_date.find("分钟前")> 0:
            minute_time = int(str_date.split('分钟前')[0].strip(" "))
            pub_time = self.now_date - datetime.timedelta(minutes=minute_time)
            pub_time_str = pub_time.strftime("%Y-%m-%d")
            return pub_time_str

        array = time.strptime(str_date, u" %Y年%m月%d日 %H:%M")  # 2019年02月26日 16:23
        try:
            publish_time = time.strftime("%Y-%m-%d", array)
            return publish_time
        except Exception as e:
            return ''

    def get_all_data(self, driver):
        flg_status = True
        api_for_intell = api.IntellifeedAPICaller()
        while flg_status:
            per_page_data = self.get_data_from_list_page(driver)
            # send data to mvp backend
            resp = api_for_intell.send_latest_news_data_to_mvp(per_page_data, 'news')
            flg_status = self.next_page_home(driver)

    def get_detail_content_by_newspaper3k(self, href, lang='zh'):
        article = Article(href, language=lang)
        try:
            article.download()
            article.parse()
        except ArticleException:
            return '', ''
        except Exception as err:
            return '', ''
        return article.text, article.title








# class ScrapeBaiduNews(BaseScraper):
#
#     url = 'http://news.baidu.com/ns?rn=20&ie=utf-8&cl=2&ct=1&bs=%E9%BA%A6%E6%A0%BC%E7%90%86pn%3D0&rsv_bp=1&sr=0&f=8&prevct=no&tn=news&word=%E9%BA%A6%E6%A0%BC%E7%90%86&rsv_sug2=0&inputT=5'
#
#     # def get_list_data_from_page(self, driver):
#     #     self.browser.get(self.url)
#     #     list_value = self.browser.find_elements_by_xpath("//div[contains(@class, 'result')]")
#     #     print(len(list_value))
#     #     for item in list_value:
#     #         print(item.text)
#
#     def get_data_from_baidu(self, driver):
#         list_value = self.browser.find_elements_by_xpath("//div[contains(@class, 'result')]")
#
#
#         # print(len(list_value))
#         # for item in list_value:
#         #     print(item.text)
#         # LandingHome()
#         # LandingHome.next_page_home(self.browser)
#         # close_browser(self.browser)
#
#     def click_next_page(self, driver):
#         LandingHome()
#         LandingHome.next_page_home(driver)
#


