#  -*-coding:utf8 -*-
"""
# run the code every 3 day(9:00 am), now we just scrape first page of the website

# URL: http://www.chinadaily.com.cn/business/money/page_1.html

"""
import dateutil.parser as parser
from datetime import datetime, timedelta
from scrawl.news.base import BaseScraper


class ScrapeChinaDaily(BaseScraper):

    def __init__(self, domain_url, media, lang):
        self.web_url = domain_url
        self.base_element = '//*[@id="lft-art"]/div['
        super(ScrapeChinaDaily, self).__init__(domain_url, media, lang)

    def extract_content_by_xpath_data(self):
        selector = self.extract_content_by_xpath(self.web_url)
        for index in range(20):
            dict_result = dict()
            dict_result['published_at'] = selector.xpath(self.base_element + str(index + 1) + ']/span[2]/b/text()')[0]
            check_date = parser.parse(dict_result['published_at'])
            if self.now_time - check_date <= timedelta(days=3):
                dict_result['url'] = selector.xpath(self.base_element + str(index + 1) + ']/span[2]/h4/a/@href')[0]
                dict_result['title'] = selector.xpath(self.base_element + str(index + 1) + ']/span[2]/h4/a/text()')[0]
                dict_result['is_added_to_source'] = False
                dict_result['language'] = self.language
                dict_result['media'] = self.media
                if dict_result['url']:
                    content = self.extract_content_by_newspaper3k(
                        'http://' + dict_result['url'][2:], 'en')
                    if content:
                        dict_result['content'] = content
                    else:
                        dict_result['content'] = ''
                self.result_json.append(dict_result)
            else:
                return self.result_json
        # self.check_scrawl_process_status(self.result_json)
        return self.result_json
