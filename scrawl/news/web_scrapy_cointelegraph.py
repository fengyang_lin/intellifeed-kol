#  -*-coding:utf8 -*-

"""
# run the code every 5 day(9:00 am), now we just scrape first page of the website

# URL: https://cointelegraph.com/tags/china
 # the detail page is used beautifulsoup package , it need to be replaced by newspaper3k later
"""

import json
from datetime import datetime, timedelta

from scrawl.news.base import BaseScraper


class ScrapeChinaDaily(BaseScraper):
    def __init__(self, domain_url, media, lang):
        self.web_url = domain_url
        super(ScrapeChinaDaily, self).__init__(domain_url, media, lang)

    def extract_content_by_xpath_data(self):
        bs_test = self.extract_content_by_bs(self.web_url)
        second_part_articles = bs_test.findAll('article', class_='post-preview-item-inline__article')
        for article in second_part_articles:
            dict_item = dict()
            dict_item['url'] = article.find('a', class_="post-preview-item-inline__figure-link")['href']
            dict_item['title'] = article.find('span', class_="post-preview-item-inline__title").getText()
            dict_item['content'] = self.extract_content_by_newspaper3k(dict_item['url'], 'en')
            dict_item['published_at'] = article.find('time', class_="post-preview-item-inline__date")['datetime']
            dict_item['published_at'] = datetime.strptime(dict_item['published_at'], "%Y-%m-%d")
            if datetime.now() - dict_item['published_at'] > timedelta(days=5):
                return self.result_json
            dict_item['is_added_to_source'] = False
            dict_item['media'] = self.media
            dict_item['language'] = self.language
            self.result_json.append(dict_item)
        return self.result_json

    # def get_detail_content(self, url):
    #     bs_test = self.extract_content_by_bs(url)
    #     second_part_articles = bs_test.find('div', attrs={"itemprop": "articleBody"})
    #     article_part_list = second_part_articles.findAll('p')
    #     summary = ''
    #     for part in article_part_list:
    #         summary += part.getText() + ' /n'
    #         if part.find('a'):
    #             summary += part.find('a').getText() + '  '
    #     return summary
