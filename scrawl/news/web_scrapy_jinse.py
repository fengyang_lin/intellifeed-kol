#  -*-coding:utf8 -*-

"""
# run the code every 1 day(9:00 am), now we just scrape first page of the website,
# call api to get data
# URL: https://api.jinse.com/v4/live/list?limit=200&reading=false&source=web

"""

import re
import time
import datetime
from scrawl.news.base import BaseScraper


class ScrapeJinSe(BaseScraper):
    def __init__(self,  domain_url, media, lang):
        self.web_url = domain_url
        self.base_url = 'https://www.jinse.com/lives/'
        super(ScrapeJinSe, self).__init__(domain_url, media, lang)

    def extract_content(self):
        json_obj = super(ScrapeJinSe, self).response_data_by_urlopen()  # some question about super
        for date in json_obj['list']:
            for item in date['lives']:
                dict_item = dict()
                dict_item['title'] = '[' + item['content'].split('】')[0][1:] + ']'
                dict_item['url'] = self.base_url + str(item['id']) + '.htm'
                dict_item['content'] = item['content'].split('】')[1]
                dict_item['language'] = self.language
                date_time = datetime.datetime.strptime(
                    time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(item['created_at'])), "%Y-%m-%d %H:%M:%S")
                dict_item['published_at'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(item['created_at']))
                dict_item['is_added_to_source'] = False
                dict_item['media'] = self.media
                if datetime.datetime.now() - date_time > datetime.timedelta(days=1):
                    return self.result_json
                self.result_json.append(dict_item)
        return self.result_json


# class scrapy_data:
#     def __init__(self, website=None):
#         self.aimed_website = website
#         self.result_json = []
#
#     def reponse_data(self):
#         self.response_data_by_urlopen
#         response = urlopen(self.aimed_website)
#         str_response = response.read().decode('utf-8')
#         json_obj = json.loads(str_response)
#
#         for date in json_obj['list']:
#             for item in date['lives']:
#                 dict_item = dict()
#                 dict_item['title'] = '[' + item['content'].split('】')[0][1:] + ']'
#                 dict_item['url'] = 'https://www.jinse.com/lives/' + str(item['id']) + '.htm'
#                 dict_item['content'] = item['content'].split('】')[1]
#                 dict_item['language'] = 'cn'
#                 dict_item['published_at'] = datetime.datetime.strptime(
#                     time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(item['created_at'])), "%Y-%m-%d %H:%M:%S")
#
#                 dict_item['is_added_to_source'] = False
#                 dict_item['media'] = 'JinseCaijing'
#
#                 if datetime.datetime.now() - dict_item['published_at'] > datetime.timedelta(days=1):
#                     return self.result_json
#                 self.result_json.append(dict_item)
#         return self.result_json
#
# if __name__ == '__main__':
#     scrapy_data_v1 = scrapy_data(website='https://api.jinse.com/v4/live/list?limit=200&reading=false&source=web')
#     result_data_list = scrapy_data_v1.reponse_data()
#     print('*' * 20 + "begin to connect to red shift " + '*' * 20)
#     # print(result_data_list[0], result_data_list[-1])
#     # con_obj = ScrapyConnectToRedshift()
#     # con_obj.bulk_insert('media_source_content', COLUM_LIST, result_data_list)
#     # print(result_data_list)
#     COLUM_LIST = ["title", "url", "content", "language", 'published_at', "is_added_to_source", "media"]
#     if result_data_list:
#        con_obj = ScrapyConnectToRedshift()
#        con_obj.bulk_insert('media_source_content', COLUM_LIST, result_data_list)
