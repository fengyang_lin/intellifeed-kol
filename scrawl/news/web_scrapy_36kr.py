#  -*-coding:utf8 -*-
from scrawl.news.base import BaseScraper


class Scrape36kr(BaseScraper):
    # def __init__(self):
    #     super(Scrape36kr, self).__init__()

    @staticmethod
    def write_id_to_text(rp_id):
        with open('rp_id_36kr.txt', 'w') as the_file:
            the_file.write(rp_id)

    @staticmethod
    def read_id_to_text():
        with open('rp_id_36kr.txt', 'r') as the_file:
            return the_file.read()

    def response_data(self):
        json_obj = super(Scrape36kr, self).response_data_by_urlopen()
        rp_id = self.read_id_to_text()
        for item in json_obj['data']['items']:
            dict_item = {}
            if item['id'] >= int(rp_id):
                dict_item['title'] = item['title']
                dict_item['content'] = item['description']
                dict_item['published_at'] = item['published_at']
                dict_item['url'] = item['news_url']
                dict_item['language'] = 'cn'
                dict_item['is_added_to_source'] = False
                dict_item['media'] = '36kr'
                self.result_json.append(dict_item)
            else:
                max_rp_id = json_obj['data']['items'][0]['id']
                self.write_id_to_text(str(max_rp_id))
                return self.result_json
        return self.result_json
