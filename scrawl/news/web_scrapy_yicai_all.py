#  -*-coding:utf8 -*-
"""
# run the code everyday(9:00 am), now we just scrape first page of the website
# URLS: ['https://www.yicaiglobal.com/finance', 'https://www.yicaiglobal.com/tech',
                      'https://www.yicaiglobal.com/startup', 'https://www.yicaiglobal.com/business']

"""

from datetime import timedelta
from dateutil.parser import parse
from scrawl.news.base import BaseScraper


class ScrapeYiCai(BaseScraper):
    yicai_url_list = ['https://www.yicaiglobal.com/finance', 'https://www.yicaiglobal.com/tech',
                      'https://www.yicaiglobal.com/startup', 'https://www.yicaiglobal.com/business']

    def __init__(self, domain_url, media, lang):
        self.result_list = []
        self.base_element = '//*[@id="__next"]/div/div[2]/div/div/div[1]/div/div/div/div/div['
        self.media = media
        super(ScrapeYiCai, self).__init__(domain_url, media, lang)

    def response_data(self):
        for url in self.yicai_url_list:
            selector = self.extract_content_by_xpath(url)
            for index in range(10):
                dict_result = dict()
                dict_result['title'] = selector.xpath(
                    self.base_element + str(index + 1) + ']/div[1]/div[2]/a/text()')[0]
                part_url = selector.xpath(self.base_element + str(index + 1) + ']/div[1]/div[2]/a/@href')[0]
                dict_result['url'] = 'https://www.yicaiglobal.com' + part_url
                if dict_result['url']:
                    summary = self.extract_content_by_newspaper3k(dict_result['url'], 'en')
                    dict_result['content'] = summary
                data_xpath = selector.xpath(self.base_element + str(index + 1) + ']/div[1]/div[2]/div/time/text()')
                if not data_xpath:
                    data = None
                else:
                    data = data_xpath[0]
                if data is None or 'ago' in data:
                    datetime_parse = self.now_time
                    dict_result['published_at'] = self.now_time.strftime("%Y-%m-%d %H:%M:%S")
                else:
                    datetime_parse = parse(data, fuzzy=True)
                    dict_result['published_at'] = datetime_parse.strftime("%Y-%m-%d %H:%M:%S")
                if self.now_time - datetime_parse > timedelta(days=5):
                    break
                dict_result['is_added_to_source'] = False
                dict_result['language'] = self.media
                dict_result['media'] = 'YiCai' + '-' + url.split('/')[-1]
                self.result_list.append(dict_result)
            # _ = self.check_scrawl_process_status(self.result_list)
        return self.result_list
