# import codecs
import sys

sys.path.append('../../../intellifeed-kol')
import requests
import re
import time
import json
import datetime
from lxml import etree
from retrying import retry
from urllib import request
from translate import Translator
from html.parser import HTMLParser
from common.api import IntellifeedAPICaller
from common.common import get_avatar_and_upload_to_s3, get_article_img_and_upload_to_s3

profile_obj = IntellifeedAPICaller()


def get_weibo_list(url):
    headers = {
        'User-Agent': 'User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
        'Cookie': '_T_WM=cbb7676fc5c7b125f63be1a46536941f; SUB=_2A25xVvsXDeRhGeBG4lcS9y7JzzqIHXVSuIVfrDV6PUJbktAKLRPQkW1NQfbKnJkLYM7S2QcOAr9L5rOBoWU3CZl0; SUBP=0033WrSXqPxfM725Ws9jqgMF55529P9D9WhccHHQWwXf-8q8W05q01yF5NHD95Qc1h.fe0M7SKBcWs4DqcjSdNWXIgSydh241he7eBtt; SUHB=0QS5AtpkL-mY46; SSOLoginState=1548913479; MLOGIN=1; XSRF-TOKEN=d0f70a; M_WEIBOCN_PARAMS=luicode%3D20000174'}
    try:
        req = request.Request(url, headers=headers)
        reponse = request.urlopen(req)
    except request.HTTPError as err:
        print('HTTPError sleeping ~~~~~~1200second', str(err))
        time.sleep(1200)
        req = request.Request(url, headers=headers)
        reponse = request.urlopen(req)
    page_source = reponse.read()
    selector = etree.HTML(page_source)
    return selector


def get_pagecount(selector):
    page_count = selector.xpath('//*[@id="pagelist"]/form/div/input[1]/@value')
    page_count = int(page_count[0])
    return page_count


def change_Element_to_Html(content):
    from lxml import html
    content_user_1 = html.tostring(content)
    content_user_2 = HTMLParser().unescape(content_user_1.decode())
    html = content_user_2.replace('<span class="ctt">', '<span>')
    repl = re.findall(r'<a.*?href="/n.*?@.{1,15}?</a>', html)
    alink = re.findall(r'(@[\d\D]{1,15})</a>', html)
    for i in range(len(repl)):
        html = html.replace(repl[i], alink[i])
    html = html.replace('[', '')
    return html


def get_user_info(url):
    lst_new = {"nick_name": "", "real_name": "", "gender": "", "email": "", "avatar": "", "living_location": "",
               "industry": "", "career": "", "education": "", "self_intro": "", "original_url": "",
               "followers": {"weibo": {"follower": "", "fans": ""}}, "achievement": ""}
    selector_for_userinfo = get_weibo_list(url)
    userimage = selector_for_userinfo.xpath('/html/body/div[3]/img/@src')
    username = selector_for_userinfo.xpath('/html/body/div[6]/text()')
    if userimage == []:
        userimage = selector_for_userinfo.xpath('/html/body/div[4]/img/@src')
    if username == ['基本信息']:
        username = selector_for_userinfo.xpath('/html/body/div[7]/text()')
    for i in username:
        if i[:2] == '昵称':
            lst_new["nick_name"] = i[3:]
        if i[:2] == '性别':
            if i[3:] == '男':
                lst_new["gender"] = 'male'
            elif i[3:] == '女':
                lst_new["gender"] = 'female'
        if i[:2] == '地区':
            lst_new["living_location"] = i[3:]
        if i[:2] == '简介':
            lst_new["self_intro"] = i[3:]
    lst_new["original_url"] = url
    uid = re.findall(r'\d+', url)
    # r = requests.get(userimage[0])
    # with open('image/' + lst_new['nick_name'] + '.png', 'wb') as f:
    #     f.write(r.content)
    path = get_avatar_and_upload_to_s3(userimage[0], 'weibo_' + uid[0])
    lst_new["avatar"] = path
    translator = Translator(from_lang="zh_CN", to_lang="en")
    result = translator.translate(lst_new["nick_name"])
    lst_new["nick_name"] = result
    return lst_new


def get_user_follower():
    selector_for_follower = get_weibo_list(url)
    follows = selector_for_follower.xpath('//*[@class="tip2"]/a[1]/text()')
    fans = selector_for_follower.xpath('//*[@class="tip2"]/a[2]/text()')
    follows_num = re.findall(r'-?[1-9]\d*', follows[0])
    fans_num = re.findall(r'-?[1-9]\d*', fans[0])
    return follows_num, fans_num


def get_user_href(url):
    selector_for_userhref = get_weibo_list(url)
    userinfo_href = selector_for_userhref.xpath('//*[@class="ut"]/a[2]/@href')
    lst_new = get_user_info('https://weibo.cn/' + userinfo_href[0])
    return lst_new


deleted = ['<span>抱歉，此微博已被作者删除。查看帮助：<a href=',
           '<span>抱歉，由于作者设置，你暂时没有这条微博的查看权限哦。查看帮助：<a href=',
           '<span>该微博因被多人投诉，根据《微博社区公约》，已被删除。查看帮助：<a href=']


def full_text(fulltext, item, image_label):
    number = 0
    for text in fulltext:
        number += 1
        if text == '全文':
            fulltext_href = item.xpath('./div/span[@class="ctt"]/a[' + str(number) + ']/@href')
            fulltext_href = 'https://weibo.cn' + fulltext_href[0]
            time.sleep(1)
            selector_2 = get_weibo_list(fulltext_href)
            List_2 = selector_2.xpath('//*[@class="c"]')
            for item in List_2:
                content_user_2 = item.xpath('./div/span[@class="ctt"]')
                if content_user_2:
                    content = change_Element_to_Html(content_user_2[0]) + image_label
                    return content


def main(url, uid):
    selector_for_page = get_weibo_list(url)
    page_count = get_pagecount(selector_for_page)
    for i in range(1, page_count):
        # lst_weibo = []
        time.sleep(1)
        url_1 = url + '&page=' + str(i)
        print(url_1)
        selector_1 = get_weibo_list(url_1)
        List_1 = selector_1.xpath('//*[@class="c"]')
        weibo_number_in_page = 1
        for item in List_1:
            user_name = item.xpath('./div/span[@class="cmt"]/a[1]/text()')
            content_user_1 = item.xpath('./div/span[@class="ctt"]')
            weibo_text = item.xpath('./div/span[@class="ctt"]/text()')
            likes = item.xpath('./div[2]/a[contains(text(),"赞")]/text()')
            forward = item.xpath('./div[2]/a[contains(text(),"转发")]/text()')
            comment = item.xpath('./div[2]/a[contains(text(),"评论")]/text()')
            source_url = item.xpath('./div[2]/a[contains(text(),"评论")]/@href')
            if likes == []:
                likes = item.xpath('./div[3]/a[contains(text(),"赞")]/text()')
                forward = item.xpath('./div[3]/a[contains(text(),"转发")]/text()')
                comment = item.xpath('./div[3]/a[contains(text(),"评论")]/text()')
                source_url = item.xpath('./div[3]/a[contains(text(),"评论")]/@href')
            if likes == []:
                likes = item.xpath('./div[1]/a[contains(text(),"赞")]/text()')
                forward = item.xpath('./div[1]/a[contains(text(),"转发")]/text()')
                comment = item.xpath('./div[1]/a[contains(text(),"评论")]/text()')
                source_url = item.xpath('./div[1]/a[contains(text(),"评论")]/@href')
            try:
                likes_count = re.findall(r'-?[1-9]\d*', str(likes))[0]
                forward_count = re.findall(r'-?[1-9]\d*', str(forward))[0]
                comment_count = re.findall(r'-?[1-9]\d*', str(comment))[0]
                source_full_url = source_url[0]
            except:
                continue
            print(likes_count)
            print(forward_count)
            print(comment_count)
            print(source_full_url)
            if weibo_text == []:
                weibo_text = item.xpath('./div/text()')
            # href = item.xpath('./div/span[@class="ctt"]/a/@href')
            sendtime = item.xpath('./div/span[@class="ct"]/text()')
            image = item.xpath('./div[2]/a[2]/text()')
            image_label = ""
            if image == ['原图']:
                imghref = item.xpath('./div[2]/a[2]/@href')
                image_uid = re.findall(r'https://weibo.cn/mblog/oripic\?id=(.*)=(.*)', imghref[0])
                image_href = 'http://ww1.sinaimg.cn/large/' + image_uid[0][1] + '.jpg'
                path = get_article_img_and_upload_to_s3(image_href, 'weibo_' + image_uid[0][1])
                image_label = '<img src=' + "\"" + path + "\""'>'
            content = {
                "content_info": {
                    "source_website_name": "Weibo",
                    "source_domain_url": "https://weibo.cn/",
                    # "source_full_destination_url": url_1 + '#' + str(weibo_number_in_page),
                    "source_full_destination_url": source_full_url,
                    "publish_time": "",
                    "content_cn": "",
                    "title_cn": " ",
                    "kol_id": uid,
                    "types": "social_media"
                }
            }
            # if the weibo is transmitted
            weibo_content = ""
            fulltext = item.xpath('./div/span[@class="ctt"]/a/text()')
            if user_name:
                if user_name[0] != profile_data["nick_name"]:
                    if '全文' not in fulltext:
                        weibo_content = change_Element_to_Html(content_user_1[0]) + image_label
                    else:
                        weibo_content = full_text(fulltext, item, image_label)
                    transmitted_reason = item.xpath('./div[3]')
                    if transmitted_reason == []:
                        transmitted_reason = item.xpath('./div[2]')
                    if transmitted_reason:
                        transmitted_reason = change_Element_to_Html(transmitted_reason[0]).replace(
                            '<span class="cmt">转发理由:</span>', '')
                        others_1 = re.findall(r'<a href="https://weibo.cn/attitude/.*?赞.*?<span class="ct">.*?</span>',
                                              transmitted_reason, re.S)
                        transmitted_reason = transmitted_reason.replace(others_1[0], '')
                        emotion_img = re.findall(r'<img.*?src="(//h5.sinaimg.cn/m/emoticon/.*?)".*?>',
                                                 transmitted_reason, re.S)
                        if emotion_img:
                            emotion_src = 'https:' + emotion_img[0]
                            emotion_uid = re.findall(r'https://h5.sinaimg.cn/m/emoticon.*?/(.*?)\..*?', emotion_src)
                            emotion_uid = emotion_uid[0].replace('/', '')
                            emotion_path = get_article_img_and_upload_to_s3(emotion_src, 'weibo_' + emotion_uid)
                            transmitted_reason = transmitted_reason.replace(emotion_img[0], emotion_path)
                        weibo_content = transmitted_reason + '<!-- ===forward=== -->' + weibo_content
            # if the weibo is original
            else:
                if content_user_1:
                    original_content = change_Element_to_Html(content_user_1[0]) + image_label
                    hreflst = re.findall(r'<span>[\d\D]*<a href=', original_content)
                    hreflst.append('deleted')
                    if hreflst[0] not in deleted:
                        if '全文' not in fulltext:
                            weibo_content = original_content
                        else:
                            weibo_content = full_text(fulltext, item, image_label)
            if weibo_content:
                send_time = sendtime[0].split('\xa0')
                send_time = send_time[0].replace('月', '-').replace('日', '')
                if send_time[:2] == '今天':
                    send_time = datetime.datetime.now().strftime("%Y-%m-%d") + send_time[2:]
                if send_time[len(send_time) - 1] == '前':
                    send_time = (datetime.datetime.now() - datetime.timedelta(minutes=int(send_time[:2]))).strftime(
                        "%Y-%m-%d %H:%M")
                if len(send_time) < 16:
                    send_time = '2019-' + send_time
                try:
                    timeStamp = time.mktime(time.strptime(send_time + ':00', '%Y-%m-%d %H:%M:%S'))
                except:
                    timeStamp = time.mktime(time.strptime(send_time, '%Y-%m-%d %H:%M:%S'))
                # content['content_info']["source_full_destination_url"] = url + '#' + str(timeStamp)
                content['content_info']["content_cn"] = weibo_content
                content['content_info']["publish_time"] = send_time
                with open('user_allweibo_lst.txt', 'r') as f_1:
                    all_weibo_lst = f_1.readlines()
                try:
                    check_weibo = weibo_text[0] + '\n'
                except Exception as err:
                    continue
                if check_weibo in all_weibo_lst:
                    print('do not repeat!!!!!!!!!!')
                else:
                    content_response = profile_obj.new_content(content)
                    if type(content_response) == str:
                        continue
                    if content_response.status_code == 201:
                        new_followers = {}
                        json_data = {"user_id": uid}
                        query_response = profile_obj.query_kol_profile(json_data)
                        if type(query_response) == str:
                            continue
                        try:
                            old_followers = query_response.json()['followers']
                        except Exception as err:
                            print('query_response Error', err)
                            time.sleep(1200)
                            old_followers = query_response.json()['followers']
                        if 'likes' in old_followers['weibo']:
                            old_followers['weibo']['likes'] = str(
                                int(old_followers['weibo']['likes']) + int(likes_count))
                        else:
                            old_followers['weibo']['likes'] = '0'
                        new_followers['user_id'] = uid
                        new_followers['followers'] = old_followers
                        update_response = profile_obj.update_kol_profile(new_followers)
                        if type(update_response) == str:
                            continue
                        try:
                            print('update_response', update_response.json())
                        except Exception as err:
                            print('update_response Error', err)
                            time.sleep(600)
                    print('content_response', content_response.status_code)
                    with open('user_allweibo_lst.txt', 'a', encoding='utf-8') as f_2:
                        f_2.write(weibo_text[0] + '\n')
                    print(weibo_content)
                    print(content)
                    if content_response.status_code == 200 or content_response.status_code == 201:
                        print('content_response', content_response.json())
            weibo_number_in_page += 1


if __name__ == '__main__':
    # profile_obj.send_slack_notification('[Start weibo Scrawl]')
    # try:
    f = open("url.txt")
    lines = f.readlines()
    for line in lines:
        uuid_dict_w = {}
        url = line.strip('\n')
        user_uuid_r = open('user_uuid.json', 'r', encoding='utf-8')
        uuid_dict_r = json.load(user_uuid_r)
        if url in uuid_dict_r:
            uid = uuid_dict_r[url]
            query_response = profile_obj.query_kol_profile({"user_id": uid})
            profile_data = query_response.json()
            print('not create:', uid)
        else:
            profile_data = get_user_href(url)
            time.sleep(1)
            follows_num, fans_num = get_user_follower()
            time.sleep(1)
            profile_data["followers"]["weibo"]["follower"] = fans_num[0]
            profile_data["followers"]["weibo"]["fans"] = follows_num[0]
            response_user = profile_obj.new_kol_profile(profile_data)
            uid = response_user.json()['id']
            uuid_dict_r[url] = uid
            user_uuid_w = open('user_uuid.json', 'w', encoding='utf-8')
            user_uuid_w.write(json.dumps(uuid_dict_r))
            print('response_user', response_user.status_code)
            print('response_user', response_user.json())
            user_uuid_w.close()
        print(url, uid)
        main(url, uid)
        user_uuid_r.close()
        # with open('user_allweibo_lst.t    xt', 'r+') as f_3:
        #     f_3.truncate()
        time.sleep(1)
    f.close()
#     profile_obj.send_slack_notification('[ End weibo Scrawl]')
# except Exception as err:
#     profile_obj.send_slack_notification('[ Fail to weibo Scrawl], the error msg is %s' % str(err))
