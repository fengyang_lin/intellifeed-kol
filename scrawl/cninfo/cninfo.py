import json
import datetime

from datetime import date
import time
import requests
from bs4 import BeautifulSoup as BS
import sys
import os
import settings
from common import api
import pdftotext


class CninfoScrape(object):
    job_name = 'Cninfo Spider'
    sipder = 'Spider'
    domain_static_url = settings.CNINFO_STATIC_URL_BASE
    domain_base_url = settings.CNINFO_COMPANY_INFO_URL_BASE
    domain_url_in_s3 = settings.CNINFO_URL_IN_S3

    now_date = datetime.date.today()
    outside_api_caller = api.IntellifeedAPICaller()
    main_path = os.getcwd()
    base_url_cninfo = 'http://www.cninfo.com.cn/new/disclosure?column=szse_latest&pageNum='
    headers = {'Authorization': 'Token ' + settings.MVP_ACCOUNT_TOKEN, 'Content-Type': 'application/json'}

    def check_scrawl_process_status(self, result, flg_status):
        if flg_status:
            to_check_content_list = [item['content_info'] for item in result]
            flg_for_process_success = 0
            for it in to_check_content_list:
                if it.strip() == '':
                    flg_for_process_success += 1
            record_dict = dict()
            if flg_for_process_success >= 3:
                record_dict["name"] = self.Job_Name
                record_dict["over_all_status"] = False
                record_dict["trigger_by"] = self.EC2
                record_dict["details"] = result_list
                self.api_mvp.intellifeed_regular_job_record(record_dict)
                return False
            else:
                record_dict["name"] = self.Job_Name
                record_dict["over_all_status"] = True
                record_dict["trigger_by"] = self.EC2
                record_dict["details"] = result_list
                self.api_mvp.intellifeed_regular_job_record(record_dict)
                return True
        else: # if failed, send status to backend
            record_dict["name"] = self.Job_Name
            record_dict["over_all_status"] = False
            record_dict["trigger_by"] = self.sipder
            record_dict["details"] = result
            self.api_mvp.intellifeed_regular_job_record(record_dict)
            return False

    def get_company_data(self, company_id, stock_market='szcn'):
        print('Find The COmpany Data')
        url_sn_company = self.domain_base_url + stock_market + company_id + '.html'
        # check the differences between response.content and response.text
        response = requests.get(url_sn_company, timeout=100)
        bs_response = BS(response.content, 'lxml')
        company_info_in_table = bs_response.find('div', class_="clear").find('table').findAll('td', class_='zx_data2')
        brief_company_info = bs_response.find('div', class_="zx_info").find('form', id="cninfoform")
        brief_company_info = brief_company_info.find('table', class_='table').find('tr').find('td').getText()
        if len(company_info_in_table) < 20:  # some table can't be compiled by lxml correctly
            bs_response = BS(response.content, 'html5lib')
            company_info_in_table = bs_response.find('div', class_="clear").find('table').findAll('td',
                                                                                                  class_='zx_data2')
        company_data_dict = dict()
        company_data_dict['full_name'] = company_info_in_table[0].getText().replace('\r\n', '')
        company_data_dict['en_name'] = company_info_in_table[1].getText().replace('\r\n', '')
        company_data_dict['cn_name'] = company_info_in_table[0].getText().replace('\r\n', '')  # ?
        company_data_dict['nation'] = 'China'
        company_data_dict['address'] = company_info_in_table[2].getText().replace('\r\n', '')

        company_data_dict['established_time'] = ''

        company_data_dict['industry'] = company_info_in_table[7].getText().replace('\r\n', '')
        company_data_dict['parent_company'] = company_info_in_table[7].getText().replace('\r\n', '')
        company_data_dict['subsidiaries'] = ''
        company_data_dict['offical_website'] = company_info_in_table[11].getText().replace('\r\n', '')

        company_data_dict['phone'] = company_info_in_table[9].getText().replace('\r\n', '')
        company_data_dict['fax'] = company_info_in_table[10].getText().replace('\r\n', '')
        company_data_dict['Twitter'] = ''
        company_data_dict['president'] = company_info_in_table[4].getText().replace('\r\n', '')
        company_data_dict['secretary'] = company_info_in_table[5].getText().replace('\r\n', '')

        company_data_dict['reg_capital'] = company_info_in_table[6].getText().replace('\r\n', '')

        company_data_dict['ipo_time'] = company_info_in_table[12].getText().replace('\r\n', '')
        company_data_dict['prospectus_time'] = company_info_in_table[13].getText().replace('\r\n', '')
        company_data_dict['ipo_vol'] = company_info_in_table[14].getText().replace('\r\n', '')
        company_data_dict['ipo_price'] = company_info_in_table[15].getText().replace('\r\n', '')
        company_data_dict['ipo_pe_ratio'] = company_info_in_table[16].getText().replace('\r\n', '')
        company_data_dict['shareholders'] = ''   # ?
        company_data_dict['stock_code'] = brief_company_info.split('股票简称：')[0].split('股票代码：')[1]
        company_data_dict['short_name'] = company_info_in_table[3].getText().replace('\r\n', '')
        return company_data_dict

    # @staticmethod
    # def get_company_data(company_id, stock_market='szcn'):
    #     url_sn_company = 'http://www.cninfo.com.cn/information/brief/' + stock_market + company_id + '.html'
    #     print(url_sn_company)
    #     response = requests.get(url_sn_company)
    #     bs_response = BS(response.text, 'lxml')
    #     summary_1 = bs_response.find('div', class_="clear").find('table').find('td', class_='zx_data2').getText()
    #     return summary_1

    def extract_data_from_pdf(self, obj_path_in_locality):
        time.sleep(5)
        with open(obj_path_in_locality, "rb") as f:
            pdf_content_list = pdftotext.PDF(f)  # pylint: disable=c-extension-no-member
            all_content_pdf_content = ''
            for page in pdf_content_list:
                all_content_pdf_content += page + '\f'
            return all_content_pdf_content

    def upload_pdf_to_s3(self, pdf_path, pdf_name):
        pdf_path = pdf_path + '/' + pdf_name + '.pdf'
        pdf_name_str = str(pdf_name) + '.pdf'
        pdf_path_in_s3 = 'intellifeeds/cninfo/pdf/%s' % pdf_name_str
        result_upload = self.outside_api_caller.upload_file_to_s3(pdf_path, pdf_path_in_s3,
                                                                  content_type='application/pdf')
        return result_upload

    def download_pdf_locality(self, pdf_url, pdf_path, rp_id):
        os.chdir(pdf_path)
        try:
            response = requests.get(pdf_url, timeout=30)
        except Exception as err:
            try:
                print('[download 2 ...]')
                response = requests.get(pdf_url, timeout=30)
            except Exception as erre:
                print('the err is time out ')
                return 'fail'

        with open(str(rp_id) + '.pdf', 'wb') as f:
            f.write(response.content)
        time.sleep(3)
        changed_now_path = os.getcwd()
        return changed_now_path

    def delete_today_pdf_folder_locality(self, date_path):
        tmp_path = self.main_path + '/tmp/'
        os.chdir(tmp_path)
        result_path = os.path.exists(str())
        if result_path:
            shutil.rmtree(str(date_path))
            os.chdir(self.main_path)

    @staticmethod
    def created_folder_by_date_in_tmp(date_str_path):
        tmp_path = 'tmp'
        result_path = os.path.exists(tmp_path)
        if not result_path:
            os.makedirs(tmp_path)
        os.chdir(tmp_path + '/')
        sub_path_flg = os.path.exists(date_str_path)
        if not sub_path_flg:
            os.makedirs(date_str_path)
        return os.getcwd()

    def get_content(self):
        # start_date , end_date format example is 2019-10-01
        iter_ = True
        page = 1
        result_list = []
        while iter_:
            url = self.base_url_cninfo + str(page) + '&pageSize=20'
            response = requests.post(url, timeout=30)
            print(response)
            json_data = json.loads(response.text)
            pdf_result_dict = {}
            json_list = json_data['classifiedAnnouncements']
            if len(json_list) <= 0:
                iter_ = False
                break
            for item in json_list:
                secCode_flg = item[0]['secCode']
                if secCode_flg.startswith('300'):
                    company_info = self.get_company_data(secCode_flg, 'szcn')
                elif secCode_flg.startswith('002'):
                    company_info = self.get_company_data(secCode_flg, 'szsme')
                else:  # 000
                    company_info = self.get_company_data(secCode_flg, 'szmb')
                for sub_item in item:
                    dict_value = dict()
                    dt = date.fromtimestamp(sub_item['announcementTime'] / 1000)
                    if self.now_date - dt >= datetime.timedelta(days=2):
                        self.delete_today_pdf_folder_locality(self.now_date)
                        return result_list
                    dict_value["source_website_name"] = 'cninfo'
                    dict_value["publish_time"] = dt.strftime("%Y-%m-%d %H:%M:%S")  # dt.strftime('YYYY-MM-DD HH:MM')
                    dict_value["title_cn"] = sub_item['announcementTitle']
                    dict_value["source_full_route_url"] = self.domain_static_url + sub_item['adjunctUrl']  # s3
                    dict_value["source_full_destination_url"] = \
                        self.domain_url_in_s3 + sub_item['announcementId'] + '.pdf'
                    print(dict_value["source_full_destination_url"])
                    dict_value["announcement_type_name"] = sub_item['announcementTypeName']
                    dict_value["source_identifier"] = sub_item['announcementId']
                    dict_value["announcement_type"] = sub_item['announcementType']
                    dict_value['types'] = 'cn_info'
                    dict_value['source_domain_url'] = settings.CNINFO_STATIC_URL_BASE
                    pdf_result_dict["company_info"] = company_info
                    # create new folder and download pdf
                    try:
                        os.chdir(self.main_path)
                        tmp_path = self.created_folder_by_date_in_tmp(str(self.now_date))
                        today_tmp_path = tmp_path + '/' + str(self.now_date) + '/'
                        pdf_tmp_path = self.download_pdf_locality(
                            self.domain_static_url + sub_item['adjunctUrl'], today_tmp_path, sub_item['announcementId'])
                    except Exception as err:
                        print('OOOP some error happened when download PDF in locality, the error message is %s ' % err)
                        iter_ = False
                        continue

                    if settings.AWS_S3_CONNECTION:
                        print('[start to send pdf to s3]')
                        try:
                            s3_upload_file = self.upload_pdf_to_s3(pdf_tmp_path, sub_item['announcementId'])
                            print('result S3 upload !' + str(s3_upload_file))
                            time.sleep(2)
                        except Exception as err:
                            try:
                                print('[Upload PDF 2 ....]')
                                s3_upload_file = self.upload_pdf_to_s3(pdf_tmp_path, sub_item['announcementId'])
                                print('result S3 upload 2!' + str(s3_upload_file))
                                time.sleep(3)
                            except Exception as e:
                                try:
                                    print('[Upload PDF 3 ....]')
                                    s3_upload_file = self.upload_pdf_to_s3(pdf_tmp_path, sub_item['announcementId'])
                                    print('result S3 upload 3 !' + str(s3_upload_file))
                                    time.sleep(3)
                                except Exception as erro:
                                    s3_upload_file = False
                                    iter_ = False
                                    print('[OOp! Some Error to S3], '
                                          'the id is %s and the error is %s' % (sub_item['announcementId'], erro))
                    else:
                        s3_upload_file = False
                        print('[No Data to S3], the id is %s' % sub_item['announcementId'])
                # send data to mvp backend
                    if settings.MVP_BACKEND_CONNECTION and s3_upload_file:
                        try:
                            # extract data from pdf
                            dict_value["content_cn"] = self.extract_data_from_pdf(
                                pdf_tmp_path + '/' + sub_item['announcementId'] + '.pdf')
                            if not dict_value["content_cn"].strip() > "":
                                continue
                            pdf_result_dict["content_info"] = dict_value
                            url = settings.MVP_BACKEND_URL_BASE + settings.MVP_BACKEND_URL_INTELLIFEEDS
                            # url = settings.LOCAL_MVP_BACKEND_URL_BASE + settings.MVP_BACKEND_URL_INTELLIFEEDS
                            content_json = json.dumps(pdf_result_dict)
                            response = requests.post(url, data=content_json, headers=self.headers)
                            print(response.status_code)
                            # response = self.outside_api_caller.new_content(pdf_result_dict)
                            if isinstance(response, str):
                                print('some error to mvp backend 2,the err is %s' % str)
                                response = self.outside_api_caller.new_content(pdf_result_dict)
                                if isinstance(response, str):
                                    print('some error to mvp backend 2,the err is %s' % str)
                                    response = self.outside_api_caller.new_content(pdf_result_dict)
                            if isinstance(response, str):
                                print('some error to mvp backend 2,the err is %s' % str)
                                detail_message = 'This pdf failed to scrawl' + dict_value["source_full_destination_url"]
                                # self.check_scrawl_process_status(detail_message, False)
                                continue  # give up  the cninfo item
                            response_status = response.status_code
                            if response_status in [200, 201]:
                                result_list.append(pdf_result_dict)
                                # self.check_scrawl_process_status(result_list, True)
                                print('[Mvp Data Process Status]: status code is: ' + str(response.status_code))
                            else:
                                print(response_status)
                        except Exception as err:
                            iter_ = False
                            print('[OOp! Some Error to Mvp Backend ]:the error message is %s ' % err)
                        finally:
                            print('*' * 20 + 'Ending ! ' + '*' * 20)
                    else:
                        print('[No Data to Mvp]')
                page += 1
        return result_list


# if __name__ == '__main__':
#
#     print(os.getcwd())
#     Test = CninfoScrape()
#     result = Test.get_content()

    #
    # now_path = os.getcwd()
    # last_path = os.path.dirname(os.getcwd())
    # return_top_path = os.path.dirname(last_path)
    # sys.path.append(return_top_path)
