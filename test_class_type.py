"""
'static method ' / 'class method ' / ' instance method' difference

"""

class Function(object):
    cls_variable = 'class variable'

    def __init__(self):
        self.__instance_varibale = "instance variable"

    def instance_method(self):
        print(self.cls_variable)
        print(self.__instance_varibale)
        print('This is instance method')

    @staicmethod
    def static_method():
        print(self.cls_variable)
        print('This is a static method')

    @classmethod
    def class_method(cls):
        print(cls.cls_variable)
        # print(cls.__instance_varibale)  # class function can't use instance param
        print(cls.__instance_varibale)

    @classmethod
    def set_class_variable(cls):

        cls.cls_variable = 'this is a new class variable '

    def set_instance_variable(self):
        self.__instance_varibale = 'this is a new '
