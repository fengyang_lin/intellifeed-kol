# -*- coding: utf-8 -*-
"""
    pdf2txt.py
    ~~~~~~~~~

    Created by lhuizhong@gmail.com at 2016-02-21

"""
from io import BytesIO
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from pdfminer.converter import TextConverter
import settings


def getText(data, pagenumber=None, codec='utf-8'):
    rsrcmgr = PDFResourceManager()
    retstr = BytesIO()
    in_memory_pdf = BytesIO(bytes(data))
    in_memory_pdf.seek(0)
    laparams = LAParams()
    device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
    fp = in_memory_pdf
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    password = ""
    maxpages = settings.PDF_PAGES_TO_TEXT
    caching = True
    pagenos = set()
    if pagenumber:
        pagenos = (pagenumber,)
    for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages,
                                  password=password, caching=caching, check_extractable=True):
        interpreter.process_page(page)
    fp.close()
    device.close()
    str_result = retstr.getvalue()
    retstr.close()
    return str_result
