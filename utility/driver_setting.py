import settings
import time
import uuid
import shutil
import os
from platform import platform
from selenium import webdriver


def start_browser(URL, browserType='chrome', brower_driver_location=settings.DRIVER_PATH_CHROME_MAC):
    if browserType.upper() == 'FIREFOX':
        driver = webdriver.Firefox()
    elif browserType.upper() == 'CHROME':
        chrome_options = webdriver.ChromeOptions()
        chrome_options.binary_location = settings.DRIVER_PATH_CHROME

        if not os.path.exists(_tmp_folder):
            os.makedirs(_tmp_folder)

        if not os.path.exists(_tmp_folder + '/user-data'):
            os.makedirs(_tmp_folder + '/user-data')

        if not os.path.exists(_tmp_folder + '/data-path'):
            os.makedirs(_tmp_folder + '/data-path')

        if not os.path.exists(_tmp_folder + '/cache-dir'):
            os.makedirs(_tmp_folder + '/cache-dir')

        if 'Darwin' in platform():
            chrome_driver_location = settings.DRIVER_PATH_CHROME_MAC
            driver = webdriver.Chrome(chrome_driver_location)
        elif 'Linux' in platform():
            # Designed for headless like aws lambda
            chrome_options.add_argument('--headless')
            chrome_options.add_argument('--no-sandbox')
            chrome_options.add_argument('--disable-gpu')
            chrome_options.add_argument('--window-size=1280x1696')
            chrome_options.add_argument('--user-data-dir={}'.format(_tmp_folder + '/user-data'))
            chrome_options.add_argument('--hide-scrollbars')
            chrome_options.add_argument('--enable-logging')
            chrome_options.add_argument('--log-level=0')
            chrome_options.add_argument('--v=99')
            chrome_options.add_argument('--single-process')
            chrome_options.add_argument('--data-path={}'.format(_tmp_folder + '/data-path'))
            chrome_options.add_argument('--ignore-certificate-errors')
            chrome_options.add_argument('--homedir={}'.format(_tmp_folder))
            chrome_options.add_argument('--disk-cache-dir={}'.format(_tmp_folder + '/cache-dir'))
            chrome_options.add_argument('user-agent=Mozilla/5.0 (X11; Linux x86_64) '
                                        'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36')
            driver = webdriver.Chrome(chrome_options=chrome_options)
        else:
            raise Exception('which environment is it?')
    else:
        raise RuntimeError('Browser Type is not correct!')
    driver.implicitly_wait(30)
    if URL != "":
        driver.get(URL)
    time.sleep(settings.T_MINI)
    return driver